﻿using labImageOnTable.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageOnTable
{
    public partial class fm : Form
    {
        public fm()
        {
            InitializeComponent();

            var list = new List<PictureBox>();

            list.Add(pictureBox1);
            list.Add(pictureBox2);
            list.Add(pictureBox3);
            list.Add(pictureBox4);
            list.Add(pictureBox5);
            list.Add(pictureBox6);
            list.Add(pictureBox7);
            list.Add(pictureBox8);
            list.Add(pictureBox9);

            int i = 0;
            foreach (var x in list)
            {
                i++;
                x.Tag = i;
                x.Image = imageList1.Images[0];
                x.MouseEnter += PictureBox_mouseEnter;
                x.MouseLeave += PictureBox_mouseLeave;
                x.BackgroundImage = Resources.radial_gradient;
            }




        }

        private void PictureBox_mouseLeave(object sender, EventArgs e)
        {
            if (sender is PictureBox x)
            {
                x.Image = imageList1.Images[0];
            }
        }

        private void PictureBox_mouseEnter(object sender, EventArgs e)
        {
            if (sender is PictureBox x)
            {
               x.Image = imageList1.Images[(int)x.Tag];
            }
        }
    }
}
