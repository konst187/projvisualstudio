﻿namespace labDirToTags
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.edDir = new System.Windows.Forms.TextBox();
            this.buSelect = new System.Windows.Forms.Button();
            this.edTags = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // edDir
            // 
            this.edDir.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edDir.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edDir.Location = new System.Drawing.Point(0, 22);
            this.edDir.Name = "edDir";
            this.edDir.Size = new System.Drawing.Size(784, 26);
            this.edDir.TabIndex = 0;
            // 
            // buSelect
            // 
            this.buSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buSelect.Location = new System.Drawing.Point(790, 22);
            this.buSelect.Name = "buSelect";
            this.buSelect.Size = new System.Drawing.Size(86, 26);
            this.buSelect.TabIndex = 1;
            this.buSelect.Text = "•••";
            this.buSelect.UseVisualStyleBackColor = true;
            // 
            // edTags
            // 
            this.edTags.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edTags.Font = new System.Drawing.Font("Yu Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edTags.Location = new System.Drawing.Point(0, 54);
            this.edTags.Multiline = true;
            this.edTags.Name = "edTags";
            this.edTags.Size = new System.Drawing.Size(876, 552);
            this.edTags.TabIndex = 2;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(877, 607);
            this.Controls.Add(this.edTags);
            this.Controls.Add(this.buSelect);
            this.Controls.Add(this.edDir);
            this.Name = "fm";
            this.Text = "labDirToTags";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox edDir;
        private System.Windows.Forms.Button buSelect;
        private System.Windows.Forms.TextBox edTags;
    }
}

