﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labDirToTags
{
    public partial class fm : Form
    {
        public fm()
        {
            InitializeComponent();

            buSelect.Click += BuSelect_Click;

            edTags.ScrollBars = ScrollBars.Vertical;
            edTags.TextAlign = HorizontalAlignment.Center;
            edTags.ReadOnly = true;

            edTags.BackColor = Color.FromArgb(22, 22, 28);
            edTags.ForeColor = Color.FromArgb(252, 252, 252);

            edDir.ReadOnly = true;
           

            //string s = @"D:\Фото\МосПолитех\2020.01.12 Семинар ООП (Иванов, Сидоров)\23132133137193-002.jpg";
            //var a = s.Split(new char[] { ' ', '\\' }).ToArray();
            //a = a.Select(v => v.TrimStart(new char[] { '(', ')', ',' }).TrimEnd(new char[] { '(', ')', ',' })).OrderBy(v => v).ToArray();
            //edTags.Text = string.Join(Environment.NewLine, a);
        }

        private void BuSelect_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                LoadDir(dialog.SelectedPath);
            }
        }

        private void LoadDir(string newDir)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(newDir);

            edTags.Clear();

            IEnumerable<string> allfiles = Directory.EnumerateFiles(newDir, "*.*", SearchOption.AllDirectories);
            IEnumerable<string> allDirs = Directory.EnumerateDirectories(newDir, "*.*", SearchOption.AllDirectories);


            Dictionary<string, int> lst = new Dictionary<string, int>();
            foreach (string filename in allfiles)
            {
                var a = ClearExtension(System.IO.Path.GetFileNameWithoutExtension(filename)).Split(new char[] { ' ', '\\' }).ToArray();
                a = a.Select(v => v.TrimStart(new char[] { '[', '(', ')', ',' }).TrimEnd(new char[] { ']', '(', ')', ',' })).OrderBy(v => v).ToArray();

                for (int i = 0; i < a.Length; i++)
                {
                    if (lst.ContainsKey(a[i]))
                    {
                        int temp = lst[a[i]];
                        temp++;
                        lst[a[i]] = temp;
                    }
                    else
                    {
                        lst.Add(a[i], 1);
                    }
                }
            }

            foreach (string filename in allDirs)
            {
                var a = ClearExtension(System.IO.Path.GetFileNameWithoutExtension(filename)).Split(new char[] { ' ', '\\' }).ToArray();
                a = a.Select(v => v.TrimStart(new char[] { '(', ')', ',' }).TrimEnd(new char[] { '(', ')', ',' })).OrderBy(v => v).ToArray();
                for (int i = 0; i < a.Length; i++)
                {
                    if (lst.ContainsKey(a[i]))
                    {
                        int temp = lst[a[i]];
                        temp++;
                        lst[a[i]] = temp;
                    }
                    else
                    {
                        lst.Add(a[i], 1);
                    }

                }
            }

            foreach (KeyValuePair<string, int> keyValue in lst)
            {
                edTags.Text += $"{Environment.NewLine} \"{keyValue.Key}\" - {keyValue.Value.ToString()}";
            }

            edDir.Text = newDir;
        }

        public static string ClearExtension(string path)
        {
            if (path == "")
            {
                return path;
            }
            var result = path;
            do
            {
                path = result;
                result = Path.Combine(
                    Path.GetDirectoryName(path),
                    Path.GetFileNameWithoutExtension(path));
            }
            while (result != path);
            return result;
        }
    }
}
