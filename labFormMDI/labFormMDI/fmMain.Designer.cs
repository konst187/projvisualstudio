﻿namespace labFormMDI
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.miCreateNewForm = new System.Windows.Forms.ToolStripMenuItem();
            this.windowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miWindowsCascade = new System.Windows.Forms.ToolStripMenuItem();
            this.miWindowsTileHorizontal = new System.Windows.Forms.ToolStripMenuItem();
            this.miWindowsTileVertical = new System.Windows.Forms.ToolStripMenuItem();
            this.miWindowsArrangeIcons = new System.Windows.Forms.ToolStripMenuItem();
            this.miCloseActiveForm = new System.Windows.Forms.ToolStripMenuItem();
            this.miCloseAllFroms = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miCreateNewForm,
            this.windowsToolStripMenuItem,
            this.miCloseActiveForm,
            this.miCloseAllFroms,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(573, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // miCreateNewForm
            // 
            this.miCreateNewForm.Name = "miCreateNewForm";
            this.miCreateNewForm.Size = new System.Drawing.Size(111, 20);
            this.miCreateNewForm.Text = "Create New Form";
            // 
            // windowsToolStripMenuItem
            // 
            this.windowsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miWindowsCascade,
            this.miWindowsTileHorizontal,
            this.miWindowsTileVertical,
            this.miWindowsArrangeIcons});
            this.windowsToolStripMenuItem.Name = "windowsToolStripMenuItem";
            this.windowsToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.windowsToolStripMenuItem.Text = "Windows";
            // 
            // miWindowsCascade
            // 
            this.miWindowsCascade.Name = "miWindowsCascade";
            this.miWindowsCascade.Size = new System.Drawing.Size(180, 22);
            this.miWindowsCascade.Text = "Cascade";
            // 
            // miWindowsTileHorizontal
            // 
            this.miWindowsTileHorizontal.Name = "miWindowsTileHorizontal";
            this.miWindowsTileHorizontal.Size = new System.Drawing.Size(180, 22);
            this.miWindowsTileHorizontal.Text = "TileHorizontal";
            // 
            // miWindowsTileVertical
            // 
            this.miWindowsTileVertical.Name = "miWindowsTileVertical";
            this.miWindowsTileVertical.Size = new System.Drawing.Size(180, 22);
            this.miWindowsTileVertical.Text = "TileVertical";
            // 
            // miWindowsArrangeIcons
            // 
            this.miWindowsArrangeIcons.Name = "miWindowsArrangeIcons";
            this.miWindowsArrangeIcons.Size = new System.Drawing.Size(180, 22);
            this.miWindowsArrangeIcons.Text = "ArrangeIcons";
            // 
            // closeActiveFormToolStripMenuItem
            // 
            this.miCloseActiveForm.Name = "closeActiveFormToolStripMenuItem";
            this.miCloseActiveForm.Size = new System.Drawing.Size(109, 20);
            this.miCloseActiveForm.Text = "CloseActiveForm";
            // 
            // closeAllFromsToolStripMenuItem
            // 
            this.miCloseAllFroms.Name = "closeAllFromsToolStripMenuItem";
            this.miCloseAllFroms.Size = new System.Drawing.Size(95, 20);
            this.miCloseAllFroms.Text = "CloseAllFroms";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 362);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "labFormMDI";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem miCreateNewForm;
        private System.Windows.Forms.ToolStripMenuItem windowsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miWindowsCascade;
        private System.Windows.Forms.ToolStripMenuItem miWindowsTileHorizontal;
        private System.Windows.Forms.ToolStripMenuItem miWindowsTileVertical;
        private System.Windows.Forms.ToolStripMenuItem miWindowsArrangeIcons;
        private System.Windows.Forms.ToolStripMenuItem miCloseActiveForm;
        private System.Windows.Forms.ToolStripMenuItem miCloseAllFroms;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
}

