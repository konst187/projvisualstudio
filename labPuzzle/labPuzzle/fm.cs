﻿using labPuzzle.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labPuzzle
{
    public partial class fm : Form
    {
        public int cols { get; private set; }
        public int Rows { get; private set; }
        private PictureBox[,] pics;
        private Point StartPoint;
        private Point EndPoint;
        NumericUpDown text1;
        NumericUpDown text2;
        Label label1;
        Label label2;
        Form form2;
        Button buOk;
        public fm()
        {
            InitializeComponent();
            form2SetStyles();

            cols = 5;
            Rows = 5;
            this.ClientSize = new System.Drawing.Size(800, 800);
            this.MinimumSize = new System.Drawing.Size(400, 400);
            CreateCell();
            StartLocationCells();
            ResizeCells();
            RandomCells();
            ChangeSizeBu.Click += ChangeSizeClicked;
            buOk.Click += BuOk_Click;

            this.ResizeEnd += Fm_ResizeEnd;
            this.KeyDown += Fm_KeyDown;
            this.Text += "F1 - Собрать,F2 - Перемешать, F3 - Новый размер";

            // Загрузить другую картинку
            // Картинка во весь размер +
            // Новое деление картинки +
            // Автодоведение картинки +
        }

        private void form2SetStyles()
        {
            form2 = new Form();
            form2.Text = "Изменение размера";
            form2.Size = new Size(300, 300);
            form2.MinimumSize = new Size(300, 250);
            form2.MaximumSize = new Size(300, 250);
            form2.StartPosition = FormStartPosition.CenterScreen;

            buOk = new Button();
            buOk.Text = "Подвердить";
            buOk.Dock = DockStyle.Bottom;
            buOk.Font = new Font("Tahoma", 14, FontStyle.Bold);
            buOk.BackColor = Color.FromArgb(147, 201, 77);
            buOk.Height = 40;
            form2.Controls.Add(buOk);

            text1 = new NumericUpDown();
            text2 = new NumericUpDown();

            text1.Value = Rows;
            text2.Value = cols;
            text1.Maximum = 10;
            text1.Minimum = 1;
            text2.Maximum = 10;
            text2.Minimum = 1;

            text1.Width = form2.Width - 20;
            text2.Width = form2.Width - 20;
            text1.Font = new Font("Tahoma", 14, FontStyle.Bold);
            text1.ReadOnly = true;
            text2.ReadOnly = true;
            text2.Font = new Font("Tahoma", 14, FontStyle.Bold);
            text1.Height = 50;
            text2.Height = 50;
            text1.Location = new Point(0, 40);
            text2.Location = new Point(0, 120);

            label1 = new Label();
            label2 = new Label();
            label1.Width = form2.Width;
            label2.Width = form2.Width;
            label1.Font = new Font("Tahoma", 14, FontStyle.Bold);
            label2.Font = new Font("Tahoma", 14, FontStyle.Bold);
            label1.Height = 40;
            label2.Height = 40;

            label1.Text = "Строки:";
            label2.Text = "Столбцы:";
            label1.Location = new Point(0, 0);
            label2.Location = new Point(0, 80);
            form2.Controls.Add(label1);
            form2.Controls.Add(label2);
            form2.Controls.Add(text1);
            form2.Controls.Add(text2);
        }

        public void RemoveObjects() // Удаляет старые объекты интерфейса
        {

            if (pics != null)
            {
                for (int i = 0; i < Rows; i++)
                {
                    for (int j = 0; j < cols; j++)
                    {
                        this.Controls.Remove(pics[i, j]);
                    }

                }
                pics = null;
            }

        }

        private void ChangeSizeClicked(object sender, EventArgs e)
        {
            form2.ShowDialog();
        }

        private void BuOk_Click(object sender, EventArgs e)
        {
            RemoveObjects();

            Rows = (int)text1.Value;
            cols = (int)text2.Value;
            CreateCell();
            ResizeCells();
            form2.Close();
        }

        private void Fm_ResizeEnd(object sender, EventArgs e)
        {
            ResizeCells();
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    StartLocationCells();
                    break;
                case Keys.F2:
                    RandomCells();
                    break;
                case Keys.F3:
                    StartLocationCells();
                    ResizeCells();
                    break;
            }
        }

        private void RandomCells()
        {
            Random rnd = new Random();
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    pics[i, j].Location = new Point(
                        rnd.Next(ClientSize.Width - pics[i, j].Width),
                       rnd.Next(ClientSize.Height - pics[i, j].Height));

                }
            }
        }

        private void ResizeCells()
        {
            int xCellWidth = ClientSize.Width / cols;
            int xCellHeigth = ClientSize.Height / Rows;

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    int ImageHeight = Resources.sti_wallpaper_7.Height / Rows;
                    int ImageWidth = Resources.sti_wallpaper_7.Width / cols;

                    pics[i, j].Width = xCellWidth;
                    pics[i, j].Height = xCellHeigth;
                    pics[i, j].Image = new Bitmap(xCellWidth, xCellHeigth);
                    var g = Graphics.FromImage(pics[i, j].Image);
                    g.DrawImage(Resources.sti_wallpaper_7, new Rectangle(0, 0, pics[i, j].Width, pics[i, j].Height),
                    new Rectangle(j * ImageWidth, i * ImageHeight, ImageWidth, ImageHeight), GraphicsUnit.Pixel);

                    g.Dispose();

                }
            }
        }

        private void StartLocationCells()
        {
            int xCellWidth = ClientSize.Width / cols;
            int xCellHeigth = ClientSize.Height / Rows;
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    pics[i, j].Location = new Point(j * xCellWidth, i * xCellHeigth + 25);
                }
            }
        }

        private void CreateCell()
        {
            pics = new PictureBox[Rows, cols];
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    pics[i, j] = new PictureBox();
                    pics[i, j].BorderStyle = BorderStyle.FixedSingle;
                    pics[i, j].MouseUp += Fm_MouseUp;
                    pics[i, j].MouseDown += PictureBoxAll_mouseDown;
                    pics[i, j].MouseMove += PictureBoxAll_mouseMove;
                    this.Controls.Add(pics[i, j]);
                }
            }
        }

        private void Fm_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                int clW = ClientSize.Width - EndPoint.X;
                int clH = ClientSize.Height - EndPoint.Y;
                int xCellWidth = ClientSize.Width / cols;
                int xCellHeigth = ClientSize.Height / Rows;

                clW = (clW + xCellWidth / 2) / xCellWidth;
                clH = (clH + xCellHeigth / 2) / xCellHeigth;

                clW = (cols - clW);
                clH = (Rows - clH);

                if (clW > cols - 1)
                    clW = cols - 1;
                if (clW < 0)
                    clW = 0;
                if (clH > Rows - 1)
                    clH = Rows - 1;
                if (clH < 0)
                    clH = 0;

                clW *= xCellWidth;
                clH *= xCellHeigth;
                ((Control)sender).Location = (new Point(clW, clH +25));

            }
        }

        private void PictureBoxAll_mouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point x = new Point(Cursor.Position.X - StartPoint.X, Cursor.Position.Y - StartPoint.Y);
                
                if (sender is Control)
                {
                    
                    ((Control)sender).Location = PointToClient(x);
                    ((Control)sender).BringToFront();
                    EndPoint = ((Control)sender).Location;
                    
                }
            }
        }

        private void PictureBoxAll_mouseDown(object sender, MouseEventArgs e)
        {
            StartPoint = new Point(e.X, e.Y);
        }
    }
}
