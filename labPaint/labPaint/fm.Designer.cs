﻿namespace labPaint
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.paMenu = new System.Windows.Forms.Panel();
            this.paImage = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buSave = new System.Windows.Forms.Button();
            this.buLoad = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.paMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // paMenu
            // 
            this.paMenu.Controls.Add(this.buLoad);
            this.paMenu.Controls.Add(this.buSave);
            this.paMenu.Controls.Add(this.pictureBox1);
            this.paMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.paMenu.Location = new System.Drawing.Point(0, 0);
            this.paMenu.Name = "paMenu";
            this.paMenu.Size = new System.Drawing.Size(200, 500);
            this.paMenu.TabIndex = 0;
            // 
            // paImage
            // 
            this.paImage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.paImage.AutoSize = true;
            this.paImage.BackColor = System.Drawing.Color.White;
            this.paImage.Location = new System.Drawing.Point(217, 12);
            this.paImage.Name = "paImage";
            this.paImage.Size = new System.Drawing.Size(407, 476);
            this.paImage.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Lime;
            this.pictureBox1.Location = new System.Drawing.Point(39, 26);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // buSave
            // 
            this.buSave.Location = new System.Drawing.Point(39, 98);
            this.buSave.Name = "buSave";
            this.buSave.Size = new System.Drawing.Size(75, 23);
            this.buSave.TabIndex = 1;
            this.buSave.Text = "Save";
            this.buSave.UseVisualStyleBackColor = true;
            // 
            // buLoad
            // 
            this.buLoad.Location = new System.Drawing.Point(39, 127);
            this.buLoad.Name = "buLoad";
            this.buLoad.Size = new System.Drawing.Size(75, 23);
            this.buLoad.TabIndex = 2;
            this.buLoad.Text = "Load";
            this.buLoad.UseVisualStyleBackColor = true;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(636, 500);
            this.Controls.Add(this.paImage);
            this.Controls.Add(this.paMenu);
            this.Name = "fm";
            this.Text = "labPaint";
            this.paMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel paMenu;
        private System.Windows.Forms.Panel paImage;
        private System.Windows.Forms.Button buLoad;
        private System.Windows.Forms.Button buSave;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ColorDialog colorDialog1;
    }
}

