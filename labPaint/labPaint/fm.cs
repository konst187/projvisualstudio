﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labPaint
{
    public partial class fm : Form
    {
        
        private Bitmap b;
        Graphics g;
        private bool isPressed;
        private Point StartPoint;
        private Point CurPoint = new Point(0,0);

        int font1 = 5;
        Pen pen;
        int x = 0;
        int y = 0;

        public fm()
        {
            InitializeComponent();
            isPressed = false;

            b = new Bitmap(paImage.Width, paImage.Height);
            pen = new Pen(Color.Black, font1);
            //pen.StartCap = pen.EndCap = System.Drawing.Drawing2D.LineCap.Round;

            paMenu.BorderStyle = BorderStyle.FixedSingle;
            paImage.BorderStyle = BorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.MinimumSize = new System.Drawing.Size(200, 200);


            this.ResizeEnd += Fm_ResizeEnd;
            paImage.MouseDown += PaImage_MouseDown;
            paImage.MouseUp += PaImage_MouseUp;
            paImage.MouseMove += PaImage_MouseMove;
            paImage.Paint += PaImage_Paint;
            paImage.MinimumSize = new Size(1, 1);
        }



        private void Fm_ResizeEnd(object sender, EventArgs e)
        {
            Rectangle cloneRect = new Rectangle(0, 0, b.Width, b.Height);
            System.Drawing.Imaging.PixelFormat format = b.PixelFormat;
            Bitmap cloneBitmap = b.Clone(cloneRect, format);

            b.Dispose();
            b = new Bitmap(paImage.Width, paImage.Height);
            var g = Graphics.FromImage(b);
            g.DrawImage(cloneBitmap, 0, 0);
            g.Dispose();
            cloneBitmap.Dispose();

        }

        private void PaImage_Paint(object sender, PaintEventArgs e)
        {
            //e.Graphics.DrawImage(b, new Point(0, 0));

            e.Graphics.DrawImageUnscaled(b, CurPoint);
        }

        private void DrawFunc(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                g = paImage.CreateGraphics();
                g.DrawLine(new Pen(pen.Color, font1), new Point(x, y), e.Location);
                g.DrawLine(new Pen(pen.Color, font1), new Point(x + 1, y), e.Location);
                //g.DrawLine(new Pen(Color.Red, 5), x - 10, y - 10, x + 10, y + 10);
                //g.DrawLine(new Pen(Color.Red, 5), x - 10, y + 10, x + 10, y - 10);
                x = e.X;
                y = e.Y;
                g.Dispose();
                paImage.CreateGraphics().DrawImage(b, new Point(0, 0));
            }            

        }

        private void PaImage_MouseMove(object sender, MouseEventArgs e)
        {
            if (!isPressed) return;
            DrawFunc(e);

            if (e.Button == MouseButtons.Right)
            {
                CurPoint.X += e.X - StartPoint.X;
                CurPoint.Y += e.Y - StartPoint.Y;
                StartPoint = e.Location;
                paImage.Invalidate();
            }
        }

        private void PaImage_MouseUp(object sender, MouseEventArgs e)
        {
            isPressed = false;
        }

        private void PaImage_MouseDown(object sender, MouseEventArgs e)
        {
            x = e.X;
            y = e.Y;
            isPressed = true;
            StartPoint = new Point(e.X, e.Y);
        }
    }
}
