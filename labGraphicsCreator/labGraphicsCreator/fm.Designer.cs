﻿namespace labGraphicsCreator
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.buDraw = new System.Windows.Forms.Button();
            this.cbFunc = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.minNum = new System.Windows.Forms.NumericUpDown();
            this.maxNum = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.interval = new System.Windows.Forms.TrackBar();
            this.aNum = new System.Windows.Forms.TrackBar();
            this.bNum = new System.Windows.Forms.TrackBar();
            this.cNum = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.interval)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cNum)).BeginInit();
            this.SuspendLayout();
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Location = new System.Drawing.Point(200, 12);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.Color = System.Drawing.Color.Blue;
            series1.IsVisibleInLegend = false;
            series1.MarkerBorderWidth = 5;
            series1.MarkerSize = 10;
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(999, 689);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            title1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            title1.Name = "Title1";
            title1.Text = "График функции";
            this.chart1.Titles.Add(title1);
            // 
            // buDraw
            // 
            this.buDraw.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buDraw.Location = new System.Drawing.Point(3, 658);
            this.buDraw.Name = "buDraw";
            this.buDraw.Size = new System.Drawing.Size(191, 34);
            this.buDraw.TabIndex = 1;
            this.buDraw.Text = "Построить";
            this.buDraw.UseVisualStyleBackColor = true;
            // 
            // cbFunc
            // 
            this.cbFunc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbFunc.FormattingEnabled = true;
            this.cbFunc.Items.AddRange(new object[] {
            "y = ax + b",
            "y = ax^2 + bx + c",
            "y = x^3",
            "y = x^1/2",
            "y = sinx",
            "y = cosx",
            "y = tgx"});
            this.cbFunc.Location = new System.Drawing.Point(3, 31);
            this.cbFunc.Name = "cbFunc";
            this.cbFunc.Size = new System.Drawing.Size(191, 28);
            this.cbFunc.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(3, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "a=";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(3, 135);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "b=";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(4, 212);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "c=";
            // 
            // minNum
            // 
            this.minNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.minNum.Location = new System.Drawing.Point(3, 365);
            this.minNum.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.minNum.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.minNum.Name = "minNum";
            this.minNum.Size = new System.Drawing.Size(191, 26);
            this.minNum.TabIndex = 9;
            this.minNum.Value = new decimal(new int[] {
            20,
            0,
            0,
            -2147483648});
            // 
            // maxNum
            // 
            this.maxNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.maxNum.Location = new System.Drawing.Point(3, 436);
            this.maxNum.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.maxNum.Name = "maxNum";
            this.maxNum.Size = new System.Drawing.Size(191, 26);
            this.maxNum.TabIndex = 10;
            this.maxNum.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(-1, 342);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(133, 20);
            this.label4.TabIndex = 11;
            this.label4.Text = "Нижняя граница";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(-1, 413);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(137, 20);
            this.label5.TabIndex = 12;
            this.label5.Text = "Верхняя граница";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(3, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 20);
            this.label6.TabIndex = 13;
            this.label6.Text = "Уравнение";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(-1, 482);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 20);
            this.label7.TabIndex = 14;
            this.label7.Text = "Шаг=";
            // 
            // interval
            // 
            this.interval.LargeChange = 1;
            this.interval.Location = new System.Drawing.Point(3, 505);
            this.interval.Minimum = 1;
            this.interval.Name = "interval";
            this.interval.Size = new System.Drawing.Size(191, 45);
            this.interval.TabIndex = 15;
            this.interval.Value = 1;
            // 
            // aNum
            // 
            this.aNum.LargeChange = 1;
            this.aNum.Location = new System.Drawing.Point(3, 94);
            this.aNum.Maximum = 100;
            this.aNum.Minimum = -100;
            this.aNum.Name = "aNum";
            this.aNum.Size = new System.Drawing.Size(191, 45);
            this.aNum.TabIndex = 16;
            this.aNum.Value = 1;
            // 
            // bNum
            // 
            this.bNum.LargeChange = 1;
            this.bNum.Location = new System.Drawing.Point(3, 158);
            this.bNum.Minimum = -10;
            this.bNum.Name = "bNum";
            this.bNum.Size = new System.Drawing.Size(191, 45);
            this.bNum.TabIndex = 17;
            this.bNum.Value = 1;
            // 
            // cNum
            // 
            this.cNum.LargeChange = 1;
            this.cNum.Location = new System.Drawing.Point(3, 235);
            this.cNum.Minimum = -10;
            this.cNum.Name = "cNum";
            this.cNum.Size = new System.Drawing.Size(191, 45);
            this.cNum.TabIndex = 18;
            this.cNum.Value = 1;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1211, 704);
            this.Controls.Add(this.cNum);
            this.Controls.Add(this.bNum);
            this.Controls.Add(this.aNum);
            this.Controls.Add(this.interval);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.maxNum);
            this.Controls.Add(this.minNum);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbFunc);
            this.Controls.Add(this.buDraw);
            this.Controls.Add(this.chart1);
            this.Name = "fm";
            this.Text = "labGraphicsCreator";
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.interval)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cNum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button buDraw;
        private System.Windows.Forms.ComboBox cbFunc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown minNum;
        private System.Windows.Forms.NumericUpDown maxNum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TrackBar interval;
        private System.Windows.Forms.TrackBar aNum;
        private System.Windows.Forms.TrackBar bNum;
        private System.Windows.Forms.TrackBar cNum;
    }
}

