﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labGraphicsCreator
{
    public partial class fm : Form
    {
        public fm()
        {
            InitializeComponent();

            buDraw.Click += BuDraw_Click;
            label1.Text = $"a={(double)aNum.Value / 10}";
            label2.Text = $"b={(double)bNum.Value}";
            label3.Text = $"c={(double)cNum.Value}";
            label7.Text = $"Шаг={(double)interval.Value / 10}";
            aNum.ValueChanged += (s, e) => label1.Text = $"a={(double)aNum.Value / 10}";
            bNum.ValueChanged += (s, e) => label2.Text = $"b={bNum.Value}";
            cNum.ValueChanged += (s, e) => label3.Text = $"c={cNum.Value}";
            interval.ValueChanged += (s, e) => label7.Text = $"Шаг={(double)interval.Value / 10}";           
        }

        private void BuDraw_Click(object sender, EventArgs e)
        {
            double min = (double)minNum.Value;
            double max = (double)maxNum.Value;
            double h = (double)interval.Value / 10;
            double x, y;
            double a = (double)aNum.Value / 10;
            double b = (double)bNum.Value;
            double c = (double)cNum.Value;

            switch (cbFunc.SelectedIndex)
            {
                case 0:
                    this.chart1.Series[0].Points.Clear();
                    x = min;
                    Text = $"{cbFunc.SelectedItem.ToString()}";
                    while (x < max)
                    {
                        y = a * x + b;
                        this.chart1.Series[0].Points.AddXY(x, y);
                        x += h;
                    }
                    x += h;
                    break;
                case 1:
                    Text = $"{cbFunc.SelectedItem.ToString()}";
                    this.chart1.Series[0].Points.Clear();
                    x = min;
                    Text = $"{cbFunc.SelectedItem.ToString()}";
                    while (x < max)
                    {
                        y = a * Math.Pow(x, 2) + b * x + c;
                        this.chart1.Series[0].Points.AddXY(x, y);
                        x += h;
                    }
                    x += h;
                    break;
                case 2:
                    Text = $"{cbFunc.SelectedItem.ToString()}";
                    this.chart1.Series[0].Points.Clear();
                    x = min;
                    Text = $"{cbFunc.SelectedItem.ToString()}";
                    while (x < max)
                    {
                        y = Math.Pow(x, 3);
                        this.chart1.Series[0].Points.AddXY(x, y);
                        x += h;
                    }
                    x += h;
                    break;
                case 3:
                    Text = $"{cbFunc.SelectedItem.ToString()}";
                    this.chart1.Series[0].Points.Clear();
                    x = min;
                    Text = $"{cbFunc.SelectedItem.ToString()}";
                    while (x < max)
                    {
                        y = Math.Pow(x, 0.5);
                        this.chart1.Series[0].Points.AddXY(x, y);
                        x += h;
                    }
                    x += h;
                    break;
                case 4:
                    Text = $"{cbFunc.SelectedItem.ToString()}";
                    this.chart1.Series[0].Points.Clear();
                    x = min;
                    Text = $"{cbFunc.SelectedItem.ToString()}";
                    while (x < max)
                    {
                        y = Math.Sin(x);
                        this.chart1.Series[0].Points.AddXY(x, y);
                        x += h;
                    }
                    x += h;
                    break;
                case 5:
                    Text = $"{cbFunc.SelectedItem.ToString()}";
                    this.chart1.Series[0].Points.Clear();
                    x = min;
                    Text = $"{cbFunc.SelectedItem.ToString()}";
                    while (x < max)
                    {
                        y = Math.Cos(x);
                        this.chart1.Series[0].Points.AddXY(x, y);
                        x += h;
                    }
                    x += h;
                    break;
                case 6:
                    Text = $"{cbFunc.SelectedItem.ToString()}";
                    this.chart1.Series[0].Points.Clear();
                    x = min;
                    Text = $"{cbFunc.SelectedItem.ToString()}";
                    while (x < max)
                    {
                        y = Math.Tan(x);
                        this.chart1.Series[0].Points.AddXY(x, y);
                        x += h;
                    }
                    x += h;
                    break;
            }

        }
    }
}
