﻿namespace labSoundPlayer
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buPlay = new System.Windows.Forms.Button();
            this.buStop = new System.Windows.Forms.Button();
            this.buPlayLooping = new System.Windows.Forms.Button();
            this.buBeep = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buPlay
            // 
            this.buPlay.Location = new System.Drawing.Point(11, 12);
            this.buPlay.Name = "buPlay";
            this.buPlay.Size = new System.Drawing.Size(75, 23);
            this.buPlay.TabIndex = 0;
            this.buPlay.Text = "Play";
            this.buPlay.UseVisualStyleBackColor = true;
            // 
            // buStop
            // 
            this.buStop.Location = new System.Drawing.Point(92, 12);
            this.buStop.Name = "buStop";
            this.buStop.Size = new System.Drawing.Size(75, 23);
            this.buStop.TabIndex = 1;
            this.buStop.Text = "Stop";
            this.buStop.UseVisualStyleBackColor = true;
            // 
            // buPlayLooping
            // 
            this.buPlayLooping.Location = new System.Drawing.Point(173, 12);
            this.buPlayLooping.Name = "buPlayLooping";
            this.buPlayLooping.Size = new System.Drawing.Size(75, 23);
            this.buPlayLooping.TabIndex = 2;
            this.buPlayLooping.Text = "PlayLooping";
            this.buPlayLooping.UseVisualStyleBackColor = true;
            // 
            // buBeep
            // 
            this.buBeep.Location = new System.Drawing.Point(92, 41);
            this.buBeep.Name = "buBeep";
            this.buBeep.Size = new System.Drawing.Size(75, 23);
            this.buBeep.TabIndex = 3;
            this.buBeep.Text = "Beep";
            this.buBeep.UseVisualStyleBackColor = true;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(259, 74);
            this.Controls.Add(this.buBeep);
            this.Controls.Add(this.buPlayLooping);
            this.Controls.Add(this.buStop);
            this.Controls.Add(this.buPlay);
            this.Name = "fm";
            this.Text = "labSoundPlayer";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buPlay;
        private System.Windows.Forms.Button buStop;
        private System.Windows.Forms.Button buPlayLooping;
        private System.Windows.Forms.Button buBeep;
    }
}

