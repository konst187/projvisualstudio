﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labButtonOnGrid
{
    public partial class fm : Form
    {

        private const int COL_MAX = 5;
        private const int ROW_MAX = 4;
        private Button[,] bu = new Button[ROW_MAX, COL_MAX];
        public fm()
        {
            InitializeComponent();
            this.Width = 500;
            this.Height = 400;

            CreateButtons();
            ResizeButtins();
            this.Resize += (s, e) => ResizeButtins();


        }

        private void ResizeButtins()
        {
            int xCellWidth = ClientSize.Width / COL_MAX;
            int xCellHeigth = ClientSize.Height / ROW_MAX;
            for (int i = 0; i < ROW_MAX; i++)
            {
                for (int j = 0; j < COL_MAX; j++)
                {
                    bu[i, j].Width = xCellWidth;
                    bu[i, j].Height = xCellHeigth;
                    bu[i, j].Location = new Point(j + (j * xCellWidth), i + (i * xCellHeigth));
                }
            }
        }

        private void CreateButtons()
        {
            int n = 1;
            for (int i = 0; i < ROW_MAX; i++)
            {
                for (int j = 0; j < COL_MAX; j++)
                {
                    bu[i, j] = new Button();
                    bu[i, j].Text = (n).ToString();
                    bu[i, j].Font = new System.Drawing.Font("Times new Roman", 14);
                    bu[i, j].Click += buAll_click;
                    this.Controls.Add(bu[i, j]);
                    n++;
                }
            }
        }

        private void buAll_click(object sender, EventArgs e)
        {
            if (sender is Button x)
                MessageBox.Show(x.Text);

        }
    }
}
