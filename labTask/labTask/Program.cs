﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labTask
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start");

            var t1 = new Task(MyTask1);
            t1.Start();
            t1.Wait();
            //t1.Status == TaskStatus.

            new Task(() => Console.WriteLine("Task2")).Start();
             Task.Run(() => Console.WriteLine("Task3"));


            //new Task.Run(async () => await Task.Delay(100);

            Console.WriteLine("End");
            Console.ReadKey();
        }

        private static void MyTask1()
        {
            Console.WriteLine("Task1");
        }
    }
}
