﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labTransparency
{
    public partial class fm : Form
    {
        public fm()
        {
            InitializeComponent();

            button1.Click += (sender,e) => this.Close();
            this.MouseDown += (s, e) => StartPoint = e.Location;
            this.MouseMove += Fm_MouseMove;

        }

        private void Fm_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Location = new Point(
                    Cursor.Position.X - StartPoint.X,
                    Cursor.Position.Y - StartPoint.Y
                    );
            }
        }

        public Point StartPoint;
    }
}
