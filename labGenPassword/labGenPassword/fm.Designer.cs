﻿namespace labGenPassword
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.edPassword = new System.Windows.Forms.TextBox();
            this.buPassword = new System.Windows.Forms.Button();
            this.ckLower = new System.Windows.Forms.CheckBox();
            this.ckUpper = new System.Windows.Forms.CheckBox();
            this.ckNumber = new System.Windows.Forms.CheckBox();
            this.ckSpec = new System.Windows.Forms.CheckBox();
            this.edLength = new System.Windows.Forms.NumericUpDown();
            this.laLength = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.edLength)).BeginInit();
            this.SuspendLayout();
            // 
            // edPassword
            // 
            this.edPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edPassword.Location = new System.Drawing.Point(12, 12);
            this.edPassword.Name = "edPassword";
            this.edPassword.ReadOnly = true;
            this.edPassword.Size = new System.Drawing.Size(497, 68);
            this.edPassword.TabIndex = 0;
            this.edPassword.Text = "<Password>";
            this.edPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buPassword
            // 
            this.buPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buPassword.BackColor = System.Drawing.Color.GreenYellow;
            this.buPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buPassword.Location = new System.Drawing.Point(12, 86);
            this.buPassword.Name = "buPassword";
            this.buPassword.Size = new System.Drawing.Size(497, 46);
            this.buPassword.TabIndex = 1;
            this.buPassword.Text = "Генерировать";
            this.buPassword.UseVisualStyleBackColor = false;
            // 
            // ckLower
            // 
            this.ckLower.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ckLower.AutoSize = true;
            this.ckLower.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ckLower.Location = new System.Drawing.Point(12, 172);
            this.ckLower.Name = "ckLower";
            this.ckLower.Size = new System.Drawing.Size(246, 24);
            this.ckLower.TabIndex = 2;
            this.ckLower.Text = "Символы в нижнем регистре";
            this.ckLower.UseVisualStyleBackColor = true;
            // 
            // ckUpper
            // 
            this.ckUpper.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ckUpper.AutoSize = true;
            this.ckUpper.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ckUpper.Location = new System.Drawing.Point(12, 195);
            this.ckUpper.Name = "ckUpper";
            this.ckUpper.Size = new System.Drawing.Size(251, 24);
            this.ckUpper.TabIndex = 3;
            this.ckUpper.Text = "Символы в верхнем регистре";
            this.ckUpper.UseVisualStyleBackColor = true;
            // 
            // ckNumber
            // 
            this.ckNumber.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ckNumber.AutoSize = true;
            this.ckNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ckNumber.Location = new System.Drawing.Point(12, 218);
            this.ckNumber.Name = "ckNumber";
            this.ckNumber.Size = new System.Drawing.Size(84, 24);
            this.ckNumber.TabIndex = 4;
            this.ckNumber.Text = "Цифры";
            this.ckNumber.UseVisualStyleBackColor = true;
            // 
            // ckSpec
            // 
            this.ckSpec.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ckSpec.AutoSize = true;
            this.ckSpec.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ckSpec.Location = new System.Drawing.Point(12, 243);
            this.ckSpec.Name = "ckSpec";
            this.ckSpec.Size = new System.Drawing.Size(203, 24);
            this.ckSpec.TabIndex = 5;
            this.ckSpec.Text = "Специальные символы";
            this.ckSpec.UseVisualStyleBackColor = true;
            // 
            // edLength
            // 
            this.edLength.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.edLength.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edLength.Location = new System.Drawing.Point(132, 268);
            this.edLength.Name = "edLength";
            this.edLength.Size = new System.Drawing.Size(120, 26);
            this.edLength.TabIndex = 6;
            this.edLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.edLength.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            // 
            // laLength
            // 
            this.laLength.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.laLength.AutoSize = true;
            this.laLength.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laLength.Location = new System.Drawing.Point(9, 270);
            this.laLength.Name = "laLength";
            this.laLength.Size = new System.Drawing.Size(117, 20);
            this.laLength.TabIndex = 7;
            this.laLength.Text = "Длина пароля";
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(521, 391);
            this.Controls.Add(this.laLength);
            this.Controls.Add(this.edLength);
            this.Controls.Add(this.ckSpec);
            this.Controls.Add(this.ckNumber);
            this.Controls.Add(this.ckUpper);
            this.Controls.Add(this.ckLower);
            this.Controls.Add(this.buPassword);
            this.Controls.Add(this.edPassword);
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "fm";
            this.Text = "labGenPassword";
            ((System.ComponentModel.ISupportInitialize)(this.edLength)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox edPassword;
        private System.Windows.Forms.Button buPassword;
        private System.Windows.Forms.CheckBox ckLower;
        private System.Windows.Forms.CheckBox ckUpper;
        private System.Windows.Forms.CheckBox ckNumber;
        private System.Windows.Forms.CheckBox ckSpec;
        private System.Windows.Forms.NumericUpDown edLength;
        private System.Windows.Forms.Label laLength;
    }
}

