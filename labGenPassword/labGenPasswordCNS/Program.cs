﻿using labGenPassword.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labGenPasswordCNS
{
    class Program
    {
        static void Main(string[] args)
        {

           
            while (true)
            {
                try
                {
                    Console.WriteLine("Введите длину пароля:");
                    int len = int.Parse(Console.ReadLine());
                    Console.WriteLine("Введите количество генерируемых паролей:");
                    int k = int.Parse(Console.ReadLine());
                    ArrayList list = new ArrayList();
                    for (int i = 0; i < k; i++)
                    {
                        list.Add(Utils.RandomStr(len));
                    }
                    //string password = Utils.RandomStr(len);
                    //Console.WriteLine(password);

                    foreach (string i in list)
                    {
                        Console.WriteLine("{0}ый пароль: {1}", k, i);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

        }
    }
}
