﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labFileExplorer
{
    public partial class fm : Form
    {
        private string _curDir;
        private bool flag = false;
        private string oldDir;
        //private string fullPath;
        private Stack<string> stack;
        private int sortColumn = -1;
        
        public string CurDir
        {
            get
            {
                return _curDir;
            }
            private set
            {
                _curDir = value;
                edDir.Text = value;
            }
        }
        public string SeltItem { get; private set; }

        public fm()
        {
            InitializeComponent();

            stack = new Stack<string>();
            buUp.Click += BuUp_Click;
            CurDir = Directory.GetCurrentDirectory();

            edDir.KeyDown += EdDir_KeyDown;
            lv.View = View.Details;
            buRight.Enabled = false;
            buLeft.Enabled = false;

            miViewLargeIcon.Click += (s, e) => lv.View = View.LargeIcon;
            miViewSmallIcon.Click += (s, e) => lv.View = View.SmallIcon;
            miViewList.Click += (s, e) => lv.View = View.List;
            miViewDetails.Click += (s, e) => lv.View = View.Details;
            miViewTile.Click += (s, e) => lv.View = View.Tile;

            buLeft.Click += BuLeft_Click;
            buRight.Click += BuRight_Click;

            lv.ItemSelectionChanged += (s, e) => SeltItem = Path.Combine(CurDir, e.Item.Text);
            lv.DoubleClick += Lv_DoubleClick;

            buDirSelect.Click += BuDirSelect_Click;

            lv.Columns.Add("Имя", 350);
            lv.Columns.Add("Дата изменения", 150);
            lv.Columns.Add("Тип", 100);
            lv.Columns.Add("Размер", 150);

            buHome.Click += BuHome_Click;

            lv.ColumnClick += listView1_ColumnClick;

            LoadDir(CurDir);

            buAll.Click += BuAll_Click;
            buFold.Click += BuFold_Click;
            buFile.Click += BuFile_Click;
        }

        private void BuFile_Click(object sender, EventArgs e)
        {
            LoadFiles(CurDir);
        }

        private void BuFold_Click(object sender, EventArgs e)
        {
            LoadFold(CurDir);
        }

        private void BuAll_Click(object sender, EventArgs e)
        {
            LoadDir(CurDir);
        }

        private void BuHome_Click(object sender, EventArgs e)
        {
            LoadMain();
        }

        private void listView1_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e)
        {
            if (e.Column != sortColumn)
            {
                sortColumn = e.Column;
                lv.Sorting = SortOrder.Ascending;
            }
            else
            {
                if (lv.Sorting == SortOrder.Ascending)
                    lv.Sorting = SortOrder.Descending;
                else
                    lv.Sorting = SortOrder.Ascending;
            }

            lv.Sort();
            this.lv.ListViewItemSorter = new ListViewItemComparer(e.Column, lv.Sorting);
        }

        class ListViewItemComparer : IComparer
        {
            private int col;
            private SortOrder order;
            public ListViewItemComparer()
            {
                col = 0;
                order = SortOrder.Ascending;
            }
            public ListViewItemComparer(int column, SortOrder order)
            {
                col = column;
                this.order = order;
            }
            public int Compare(object x, object y)
            {
                int returnVal;                
                try
                {
                    
                    System.DateTime firstDate =
                            DateTime.Parse(((ListViewItem)x).SubItems[col].Text);
                    System.DateTime secondDate =
                            DateTime.Parse(((ListViewItem)y).SubItems[col].Text);                    
                    returnVal = DateTime.Compare(firstDate, secondDate);
                }
                catch
                {
                    int value;
                    int value2;
                    int.TryParse(string.Join("", ((ListViewItem)x).SubItems[col].Text.Where(c => char.IsDigit(c))), out value);
                    int.TryParse(string.Join("", ((ListViewItem)y).SubItems[col].Text.Where(c => char.IsDigit(c))), out value2);
                    if (value != 0 && value2 != 0)
                    {
                        if (value > value2)
                        {
                            returnVal = 1;
                        }
                        else
                        {
                            returnVal = -1;
                        }
                    }
                    else
                    {
                        returnVal = String.Compare(((ListViewItem)x).SubItems[col].Text,
                            ((ListViewItem)y).SubItems[col].Text);
                    }


                }
                if (order == SortOrder.Descending)
                    returnVal *= -1;
                return returnVal;
            }
        }

       
        private void Lv_DoubleClick(object sender, EventArgs e)
        {
            stack.Push(CurDir);
            LoadDir(SeltItem);
        }

        private void BuRight_Click(object sender, EventArgs e)
        {
            oldDir = stack.Pop();
            LoadDir(oldDir);

            buLeft.Enabled = true;

            if (stack.Count() == 0)
            {
                buRight.Enabled = false;
            }
        }

        private void BuLeft_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < Directory.GetLogicalDrives().Length; i++)
            {
                if (CurDir.ToString() != Directory.GetLogicalDrives()[i] && CurDir != "")
                {
                    flag = true;
                    buUp.Enabled = true;
                    buLeft.Enabled = true;
                }
                else
                {                    
                    buLeft.Enabled = false;
                    buUp.Enabled = false;
                    flag = false;
                    break;
                }
            }

            if (flag == false)
            {
                stack.Push(CurDir);
                LoadMain();
            }
            else
            {
                stack.Push(CurDir);
                LoadDir(Directory.GetParent(CurDir).ToString());
            }
            if (stack.Count() > 0)
            {
                buRight.Enabled = true;
            }
        }

        private void BuUp_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < Directory.GetLogicalDrives().Length; i++)
            {
                if (CurDir.ToString() != Directory.GetLogicalDrives()[i] && CurDir != "")
                {
                    flag = true;
                    buUp.Enabled = true;
                }
                else
                {
                    buUp.Enabled = false;
                    flag = false;
                    break;
                }
            }

            if (flag == false)
            {
                stack.Push(CurDir);
                LoadMain();
            }
            else
            {
                stack.Push(CurDir);
                LoadDir(Directory.GetParent(CurDir).ToString());
            }
            if (stack.Count() > 0)
            {
                buRight.Enabled = true;
            }
        }

        private void BuDirSelect_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                LoadDir(dialog.SelectedPath);
            }
        }

        private void EdDir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (Directory.Exists(edDir.Text))
                {
                    LoadDir(edDir.Text);
                }
                if (edDir.Text == "")
                {
                    LoadMain();
                }
            }
        }

        private void LoadMain()
        {
            lv.BeginUpdate();
            lv.Items.Clear();
            for (int i = 0; i < Directory.GetLogicalDrives().Length; i++)
            {
                if (Directory.GetLogicalDrives()[i].ToString() == "C:\\")
                {
                    lv.Items.Add(new ListViewItem(new string[] { Directory.GetLogicalDrives()[i].ToString(), "", "Диск", "" }, 2));
                }
                else
                {
                    lv.Items.Add(new ListViewItem(new string[] { Directory.GetLogicalDrives()[i].ToString(), "", "Диск", "" }, 3));
                }

            }
            lv.EndUpdate();
            CurDir = "";
        }

        private void LoadDir(string newDir)
        {
            if (newDir == "")
            {
                buUp.Enabled = false;
                LoadMain();
            }
            else
            {
                buUp.Enabled = true;
                DirectoryInfo directoryInfo = new DirectoryInfo(newDir);
                lv.BeginUpdate();
                lv.Items.Clear();

                foreach (var item in directoryInfo.GetDirectories())
                {
                    var f = new FileInfo(item.FullName);
                    lv.Items.Add(new ListViewItem(new string[] { item.Name, f.LastWriteTime.ToString(), "Папка", "" }, 1));

                }

                foreach (var item in directoryInfo.GetFiles())
                {
                    var f = new FileInfo(item.FullName);

                    if (f.Extension == ".txt")
                    {
                        lv.Items.Add(new ListViewItem(new string[] { item.Name, f.LastWriteTime.ToString(), "Файл", (f.Length / 1024 + 1).ToString() + " КБ" }, 4));
                    }
                    else if (f.Extension == ".exe")
                    {
                        lv.Items.Add(new ListViewItem(new string[] { item.Name, f.LastWriteTime.ToString(), "Файл", (f.Length / 1024 + 1).ToString() + " КБ" }, 5));
                    }
                    else if (f.Extension == ".jpg")
                    {
                        lv.Items.Add(new ListViewItem(new string[] { item.Name, f.LastWriteTime.ToString(), "Файл", (f.Length / 1024 + 1).ToString() + " КБ" }, 6));
                    }
                    else if (f.Extension == ".mp3")
                    {
                        lv.Items.Add(new ListViewItem(new string[] { item.Name, f.LastWriteTime.ToString(), "Файл", (f.Length / 1024 + 1).ToString() + " КБ" }, 8));
                    }
                    else if (f.Extension == ".mp4")
                    {
                        lv.Items.Add(new ListViewItem(new string[] { item.Name, f.LastWriteTime.ToString(), "Файл", (f.Length / 1024 + 1).ToString() + " КБ" }, 9));
                    }
                    else if (f.Extension == ".png")
                    {
                        lv.Items.Add(new ListViewItem(new string[] { item.Name, f.LastWriteTime.ToString(), "Файл", (f.Length / 1024 + 1).ToString() + " КБ" }, 7));
                    }
                    else
                    {
                        lv.Items.Add(new ListViewItem(new string[] { item.Name, f.LastWriteTime.ToString(), "Файл", (f.Length / 1024 + 1).ToString() + " КБ" }, 0));
                    }

                    

                }

                lv.EndUpdate();
                CurDir = newDir;

            }
        }

        private void LoadFold(string newDir)
        {
            if (newDir == "")
            {
                buUp.Enabled = false;
                LoadMain();
            }
            else
            {
                buUp.Enabled = true;
                DirectoryInfo directoryInfo = new DirectoryInfo(newDir);
                lv.BeginUpdate();
                lv.Items.Clear();

                foreach (var item in directoryInfo.GetDirectories())
                {
                    var f = new FileInfo(item.FullName);
                    lv.Items.Add(new ListViewItem(new string[] { item.Name, f.LastWriteTime.ToString(), "Папка", "" }, 1));

                }               

                lv.EndUpdate();
                CurDir = newDir;

            }
        }

        private void LoadFiles(string newDir)
        {
            if (newDir == "")
            {
                buUp.Enabled = false;
                LoadMain();
            }
            else
            {
                buUp.Enabled = true;
                DirectoryInfo directoryInfo = new DirectoryInfo(newDir);
                lv.BeginUpdate();
                lv.Items.Clear();

               
                foreach (var item in directoryInfo.GetFiles())
                {
                    var f = new FileInfo(item.FullName);

                    if (f.Extension == ".txt")
                    {
                        lv.Items.Add(new ListViewItem(new string[] { item.Name, f.LastWriteTime.ToString(), "Файл", (f.Length / 1024 + 1).ToString() + " КБ" }, 4));
                    }
                    else if (f.Extension == ".exe")
                    {
                        lv.Items.Add(new ListViewItem(new string[] { item.Name, f.LastWriteTime.ToString(), "Файл", (f.Length / 1024 + 1).ToString() + " КБ" }, 5));
                    }
                    else if (f.Extension == ".jpg")
                    {
                        lv.Items.Add(new ListViewItem(new string[] { item.Name, f.LastWriteTime.ToString(), "Файл", (f.Length / 1024 + 1).ToString() + " КБ" }, 6));
                    }
                    else if (f.Extension == ".mp3")
                    {
                        lv.Items.Add(new ListViewItem(new string[] { item.Name, f.LastWriteTime.ToString(), "Файл", (f.Length / 1024 + 1).ToString() + " КБ" }, 8));
                    }
                    else if (f.Extension == ".mp4")
                    {
                        lv.Items.Add(new ListViewItem(new string[] { item.Name, f.LastWriteTime.ToString(), "Файл", (f.Length / 1024 + 1).ToString() + " КБ" }, 9));
                    }
                    else if (f.Extension == ".png")
                    {
                        lv.Items.Add(new ListViewItem(new string[] { item.Name, f.LastWriteTime.ToString(), "Файл", (f.Length / 1024 + 1).ToString() + " КБ" }, 7));
                    }
                    else
                    {
                        lv.Items.Add(new ListViewItem(new string[] { item.Name, f.LastWriteTime.ToString(), "Файл", (f.Length / 1024 + 1).ToString() + " КБ" }, 0));
                    }



                }

                lv.EndUpdate();
                CurDir = newDir;

            }
        }

    }
}
