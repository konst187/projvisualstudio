﻿namespace labFileExplorer
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.buLeft = new System.Windows.Forms.ToolStripButton();
            this.buRight = new System.Windows.Forms.ToolStripButton();
            this.buUp = new System.Windows.Forms.ToolStripButton();
            this.edDir = new System.Windows.Forms.ToolStripTextBox();
            this.buDirSelect = new System.Windows.Forms.ToolStripButton();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.miViewLargeIcon = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewSmallIcon = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewList = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewDetails = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewTile = new System.Windows.Forms.ToolStripMenuItem();
            this.buHome = new System.Windows.Forms.ToolStripButton();
            this.lv = new System.Windows.Forms.ListView();
            this.ilLargeIcons = new System.Windows.Forms.ImageList(this.components);
            this.ilSmallIcons = new System.Windows.Forms.ImageList(this.components);
            this.buform = new System.Windows.Forms.ToolStripDropDownButton();
            this.buAll = new System.Windows.Forms.ToolStripMenuItem();
            this.buFold = new System.Windows.Forms.ToolStripMenuItem();
            this.buFile = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buLeft,
            this.buRight,
            this.buUp,
            this.edDir,
            this.buDirSelect,
            this.toolStripDropDownButton1,
            this.buHome,
            this.buform});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(999, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // buLeft
            // 
            this.buLeft.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buLeft.Image = ((System.Drawing.Image)(resources.GetObject("buLeft.Image")));
            this.buLeft.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buLeft.Name = "buLeft";
            this.buLeft.Size = new System.Drawing.Size(23, 22);
            this.buLeft.Text = "◀";
            // 
            // buRight
            // 
            this.buRight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buRight.Image = ((System.Drawing.Image)(resources.GetObject("buRight.Image")));
            this.buRight.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buRight.Name = "buRight";
            this.buRight.Size = new System.Drawing.Size(23, 22);
            this.buRight.Text = "▶";
            // 
            // buUp
            // 
            this.buUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buUp.Image = ((System.Drawing.Image)(resources.GetObject("buUp.Image")));
            this.buUp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buUp.Name = "buUp";
            this.buUp.Size = new System.Drawing.Size(23, 22);
            this.buUp.Text = "▲";
            // 
            // edDir
            // 
            this.edDir.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.edDir.Name = "edDir";
            this.edDir.Size = new System.Drawing.Size(450, 25);
            // 
            // buDirSelect
            // 
            this.buDirSelect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buDirSelect.Image = ((System.Drawing.Image)(resources.GetObject("buDirSelect.Image")));
            this.buDirSelect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buDirSelect.Name = "buDirSelect";
            this.buDirSelect.Size = new System.Drawing.Size(23, 22);
            this.buDirSelect.Text = "...";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miViewLargeIcon,
            this.miViewSmallIcon,
            this.miViewList,
            this.miViewDetails,
            this.miViewTile});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(40, 22);
            this.toolStripDropDownButton1.Text = "Вид";
            // 
            // miViewLargeIcon
            // 
            this.miViewLargeIcon.Name = "miViewLargeIcon";
            this.miViewLargeIcon.Size = new System.Drawing.Size(126, 22);
            this.miViewLargeIcon.Text = "LargeIcon";
            // 
            // miViewSmallIcon
            // 
            this.miViewSmallIcon.Name = "miViewSmallIcon";
            this.miViewSmallIcon.Size = new System.Drawing.Size(126, 22);
            this.miViewSmallIcon.Text = "SmallIcon";
            // 
            // miViewList
            // 
            this.miViewList.Name = "miViewList";
            this.miViewList.Size = new System.Drawing.Size(126, 22);
            this.miViewList.Text = "List";
            // 
            // miViewDetails
            // 
            this.miViewDetails.Name = "miViewDetails";
            this.miViewDetails.Size = new System.Drawing.Size(126, 22);
            this.miViewDetails.Text = "Details";
            // 
            // miViewTile
            // 
            this.miViewTile.Name = "miViewTile";
            this.miViewTile.Size = new System.Drawing.Size(126, 22);
            this.miViewTile.Text = "Tile";
            // 
            // buHome
            // 
            this.buHome.BackgroundImage = global::labFileExplorer.Properties.Resources.home_icon_png_home_house_icon_image_202_512;
            this.buHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buHome.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.buHome.Image = ((System.Drawing.Image)(resources.GetObject("buHome.Image")));
            this.buHome.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buHome.Name = "buHome";
            this.buHome.Size = new System.Drawing.Size(23, 22);
            // 
            // lv
            // 
            this.lv.BackColor = System.Drawing.SystemColors.Window;
            this.lv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lv.ForeColor = System.Drawing.SystemColors.MenuText;
            this.lv.HideSelection = false;
            this.lv.LargeImageList = this.ilLargeIcons;
            this.lv.Location = new System.Drawing.Point(0, 25);
            this.lv.Name = "lv";
            this.lv.Size = new System.Drawing.Size(999, 465);
            this.lv.SmallImageList = this.ilSmallIcons;
            this.lv.TabIndex = 1;
            this.lv.UseCompatibleStateImageBehavior = false;
            // 
            // ilLargeIcons
            // 
            this.ilLargeIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilLargeIcons.ImageStream")));
            this.ilLargeIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.ilLargeIcons.Images.SetKeyName(0, "file.png");
            this.ilLargeIcons.Images.SetKeyName(1, "fold.png");
            this.ilLargeIcons.Images.SetKeyName(2, "diskC.png");
            this.ilLargeIcons.Images.SetKeyName(3, "diskD.png");
            this.ilLargeIcons.Images.SetKeyName(4, "txt.png");
            this.ilLargeIcons.Images.SetKeyName(5, "exe.png");
            this.ilLargeIcons.Images.SetKeyName(6, "jpg.png");
            this.ilLargeIcons.Images.SetKeyName(7, "unnamed.png");
            this.ilLargeIcons.Images.SetKeyName(8, "unnamed (1).png");
            this.ilLargeIcons.Images.SetKeyName(9, "mp4.png");
            // 
            // ilSmallIcons
            // 
            this.ilSmallIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilSmallIcons.ImageStream")));
            this.ilSmallIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.ilSmallIcons.Images.SetKeyName(0, "file.png");
            this.ilSmallIcons.Images.SetKeyName(1, "fold.png");
            this.ilSmallIcons.Images.SetKeyName(2, "diskC.png");
            this.ilSmallIcons.Images.SetKeyName(3, "diskD.png");
            this.ilSmallIcons.Images.SetKeyName(4, "txt.png");
            this.ilSmallIcons.Images.SetKeyName(5, "exe.png");
            this.ilSmallIcons.Images.SetKeyName(6, "jpg.png");
            this.ilSmallIcons.Images.SetKeyName(7, "unnamed.png");
            this.ilSmallIcons.Images.SetKeyName(8, "unnamed (1).png");
            this.ilSmallIcons.Images.SetKeyName(9, "mp4.png");
            // 
            // buform
            // 
            this.buform.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buform.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buAll,
            this.buFold,
            this.buFile});
            this.buform.Image = ((System.Drawing.Image)(resources.GetObject("buform.Image")));
            this.buform.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buform.Name = "buform";
            this.buform.Size = new System.Drawing.Size(63, 22);
            this.buform.Text = "Формат";
            // 
            // buAll
            // 
            this.buAll.Name = "buAll";
            this.buAll.Size = new System.Drawing.Size(180, 22);
            this.buAll.Text = "Все";
            // 
            // buFold
            // 
            this.buFold.Name = "buFold";
            this.buFold.Size = new System.Drawing.Size(180, 22);
            this.buFold.Text = "Папки";
            // 
            // buFile
            // 
            this.buFile.Name = "buFile";
            this.buFile.Size = new System.Drawing.Size(180, 22);
            this.buFile.Text = "Файлы";
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(999, 490);
            this.Controls.Add(this.lv);
            this.Controls.Add(this.toolStrip1);
            this.Name = "fm";
            this.Text = "labFileExplorer";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton buLeft;
        private System.Windows.Forms.ToolStripButton buRight;
        private System.Windows.Forms.ToolStripButton buUp;
        private System.Windows.Forms.ToolStripTextBox edDir;
        private System.Windows.Forms.ToolStripButton buDirSelect;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ListView lv;
        private System.Windows.Forms.ToolStripMenuItem miViewLargeIcon;
        private System.Windows.Forms.ToolStripMenuItem miViewSmallIcon;
        private System.Windows.Forms.ToolStripMenuItem miViewList;
        private System.Windows.Forms.ToolStripMenuItem miViewDetails;
        private System.Windows.Forms.ToolStripMenuItem miViewTile;
        private System.Windows.Forms.ImageList ilLargeIcons;
        private System.Windows.Forms.ImageList ilSmallIcons;
        private System.Windows.Forms.ToolStripButton buHome;
        private System.Windows.Forms.ToolStripDropDownButton buform;
        private System.Windows.Forms.ToolStripMenuItem buAll;
        private System.Windows.Forms.ToolStripMenuItem buFold;
        private System.Windows.Forms.ToolStripMenuItem buFile;
    }
}

