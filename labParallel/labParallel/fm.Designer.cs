﻿namespace labParallel
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.edDirTemp = new System.Windows.Forms.TextBox();
            this.buCreate = new System.Windows.Forms.Button();
            this.buDel = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.edDirTemp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edDirTemp.Location = new System.Drawing.Point(12, 12);
            this.edDirTemp.Name = "textBox1";
            this.edDirTemp.Size = new System.Drawing.Size(531, 20);
            this.edDirTemp.TabIndex = 0;
            // 
            // button1
            // 
            this.buCreate.Location = new System.Drawing.Point(13, 39);
            this.buCreate.Name = "button1";
            this.buCreate.Size = new System.Drawing.Size(189, 23);
            this.buCreate.TabIndex = 1;
            this.buCreate.Text = "Создать файлы";
            this.buCreate.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.buDel.Location = new System.Drawing.Point(208, 39);
            this.buDel.Name = "button2";
            this.buDel.Size = new System.Drawing.Size(189, 23);
            this.buDel.TabIndex = 2;
            this.buDel.Text = "Удалить файлы";
            this.buDel.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(403, 38);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(140, 23);
            this.button3.TabIndex = 3;
            this.button3.Text = "Тест";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox2.Location = new System.Drawing.Point(13, 68);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(531, 371);
            this.textBox2.TabIndex = 4;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(555, 451);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.buDel);
            this.Controls.Add(this.buCreate);
            this.Controls.Add(this.edDirTemp);
            this.Name = "fm";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox edDirTemp;
        private System.Windows.Forms.Button buCreate;
        private System.Windows.Forms.Button buDel;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox2;
    }
}

