﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labParallel
{
    public partial class fm : Form
    {
        private int countFiles = 100;

        public fm()
        {
            InitializeComponent();
            edDirTemp.Text = Path.Combine(Path.GetTempPath(), Application.ProductName);
            Directory.CreateDirectory(edDirTemp.Text);

            buCreate.Click += BuCreate_Click;
            buDel.Click += BuDel_Click;
            button3.Click += Button3_Click;
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            Parallel.Invoke(
                () => { },
                () => { }
                );
        }

        private void BuDel_Click(object sender, EventArgs e)
        {
             Parallel.For(0, countFiles, (v) => File.Delete(Path.Combine(edDirTemp.Text, $"файл{v:000}.txt")));
        }

        private void BuCreate_Click(object sender, EventArgs e)
        {
            Parallel.For(0, countFiles, CreateFile);
           
        }

        private void CreateFile(int v)
        {
            File.Create(Path.Combine(edDirTemp.Text, $"файл{v:000}.txt")).Close();
        }
    }
}
