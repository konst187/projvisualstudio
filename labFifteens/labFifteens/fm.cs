﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace labFifteens
{
    public partial class fm : Form
    {
        //Глобальные переменыне интерфейса
        public int rang = 4;
        public int steps = 0;
        Point lastPos = new Point(-1, -1);
        Point StartPoint = new Point(0, 30);
        public Panel panel;
        public Panel panel1;
        public Label label1;
        public Label label2;
        Button[] buttonArr;
        Point[] winArray;
        DateTime date1;
        public Color color = Color.FromArgb(154, 23, 255);

        public fm()
        {
            InitializeComponent();
            initObjects(); // Инициализирует новые объекты
            setStyles(); // Отрисовывает стили для новых объектов
            newGame(rang); // Запуск новой игры

            // Назначение управляющих кнопок
            restart.Click += Restart;
            mixEasy.Click += mix;
            mixMed.Click += mix;
            mixHard.Click += mix;
            mixExtr.Click += mix;
            mode5.Click += gameModeHard;
            mode4.Click += gameModeMed;
            mode3.Click += gameModeEasy;

            timer1.Tick += Timer1_Tick;
            timer1.Interval = 1000;

            buColor.Click += changeColor;
            help.Click += helpMessage;

        }

        private void helpMessage(object sender, EventArgs e)
        {
            MessageBox.Show("В разделе \"Settings\" можно выбрать размер игрового поля, а также задать цвет.\n" +
                "Можно двигать фишки при помощи стрелок на клавиатуре.\nЗажав SHIFT и стрелку, вы передвинете " +
                "сразу целый ряд.\nНажмите F5 для сброса игры.");
        }

        private void changeColor(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            color = colorDialog1.Color;
            for (int i = 0; i < buttonArr.Length - 1; i++)
            {
                buttonArr[i].BackColor = color;
            }
        }

        public void setStyles()
        {
            //Задание стилей
            this.BackColor = Color.FromArgb(255, 255, 255);
            panel.BackColor = Color.FromArgb(255, 255, 255);
            panel1.BackColor = Color.FromArgb(0, 0, 0); // Для бордера
            menu.BackColor = Color.FromArgb(255, 255, 255);
            label1.Font = new Font("Tahoma", 20, FontStyle.Bold);
            label1.ForeColor = Color.FromArgb(0, 0, 0);
            label2.Font = new Font("Tahoma", 20, FontStyle.Bold);
            label2.ForeColor = Color.FromArgb(0, 0, 0);

            //Задание размеров
            panel.Size = new System.Drawing.Size(rang * 100, rang * 100);
            panel1.Size = new System.Drawing.Size(rang * 100, rang * 100 + 2);
            this.ClientSize = new System.Drawing.Size(rang * 100 + 17, rang * 100 + StartPoint.Y * 4);
            this.MinimumSize = new System.Drawing.Size(rang * 100 + 17, rang * 100 + StartPoint.Y * 4);
            this.MaximumSize = new System.Drawing.Size(rang * 100 + 17, rang * 100 + StartPoint.Y * 4);

            //Выравнивание по краям        
            panel.Location = StartPoint;
            panel1.Location = StartPoint;
            label1.Width = ClientSize.Width / 2;
            label1.Height = ClientSize.Height / 2;
            label1.Location = new Point(5, panel1.Height + 40);

            label2.Width = ClientSize.Width / 3;
            label2.Height = ClientSize.Height / 3;
            label2.Location = new Point(panel1.Width - 90, panel1.Height + 40);

            buttonArr = new Button[rang * rang]; // инициализация массива кнопок
            for (int i = 0; i < buttonArr.Length; i++)
            {
                buttonArr[i] = new Button();
                this.Controls.Add(this.buttonArr[i]);

                buttonArr[i].Size = new System.Drawing.Size(100, 100);
                buttonArr[i].Text = ((int)i + 1).ToString();
                buttonArr[i].Visible = true;
                buttonArr[i].BackColor = color;
                buttonArr[i].FlatStyle = FlatStyle.Flat;
                buttonArr[i].Font = new Font("Tahoma", 35, FontStyle.Bold);
                buttonArr[i].TextAlign = ContentAlignment.MiddleCenter;
                buttonArr[i].ForeColor = Color.FromArgb(255, 255, 255);

            }
            // Последняя кнопка - пустая клетка
            buttonArr[buttonArr.Length - 1].Size = new System.Drawing.Size(100, 100);
            buttonArr[buttonArr.Length - 1].Text = "";
            buttonArr[buttonArr.Length - 1].Visible = true;
            buttonArr[buttonArr.Length - 1].Enabled = false;
            buttonArr[buttonArr.Length - 1].BackColor = Color.FromArgb(255, 255, 255);
            buttonArr[buttonArr.Length - 1].FlatStyle = FlatStyle.Flat;
            buttonArr[buttonArr.Length - 1].Font = new Font("Tahoma", 30, FontStyle.Bold);
            buttonArr[buttonArr.Length - 1].ForeColor = Color.FromArgb(255, 255, 255);

            for (int i = 0; i < buttonArr.Length; i++)
            {
                buttonArr[i].GotFocus += Fm_GotFocus; // Перевод фокуса с кнопок, чтобы можно было привязаться к нажатиям клавиш
                buttonArr[i].Click += button_click; // привязка всех кнопок к событию Click
            }


            label2.Text = "00:00";
            label1.Text = string.Format("Шагов: {0}", steps.ToString()); // текст в счетчике 

            // Добавляет объекты в массив объектов формы
            this.Controls.Add(panel);
            this.Controls.Add(panel1);
            this.Controls.Add(label1);
            this.Controls.Add(label2);
            label1.Focus();
        }

        private void Timer1_Tick(object sender, EventArgs e) // функция для работы таймера
        {
            date1 = date1.AddSeconds(1);
            label2.Text = date1.ToString("mm:ss");
        }

        private void Fm_GotFocus(object sender, EventArgs e) //функиця для перевода фокуса на label, чтобы корректно считывались клавиши
        {
            if (label1 != null)
            {
                label1.Focus();
            }

        }
        private void Label1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e) //обработка нажатий клавиш
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    if (e.Shift)
                    {
                        for (int k = 0; k < rang; k++)
                            canMoveKey("Left");
                    }
                    else
                    {
                        canMoveKey("Left");
                    }
                    break;
                case Keys.Up:
                    if (e.Shift)
                    {
                        for (int k = 0; k < rang; k++)
                            canMoveKey("Up");
                    }
                    else
                    {
                        canMoveKey("Up");
                    }
                    break;
                case Keys.Down:
                    if (e.Shift)
                    {
                        for (int k = 0; k < rang; k++)
                            canMoveKey("Down");
                    }
                    else
                    {
                        canMoveKey("Down");
                    }
                    break;
                case Keys.Right:
                    if (e.Shift)
                    {
                        for (int k = 0; k < rang; k++)
                            canMoveKey("Right");
                    }
                    else
                    {
                        canMoveKey("Right");
                    }
                    break;
                case Keys.F5:
                    newGame(rang);
                    clearStat();
                    break;
            }
        }


        public void RemoveObjects() // Удаляет старые объекты интерфейса
        {
            this.Controls.Remove(panel);
            this.Controls.Remove(panel1);
            this.Controls.Remove(label1);
            this.Controls.Remove(label2);

            timer1.Stop();
            panel = null;
            panel1 = null;
            label1 = null;
            label2 = null;

            if (buttonArr != null)
            {
                for (int i = 0; i < buttonArr.Length; i++)
                {
                    this.Controls.Remove(buttonArr[i]);
                }
                buttonArr = null;
            }

        }

        public void initObjects() // Инициализирует объекты интерфейса
        {
            panel1 = new Panel();
            label1 = new Label();
            panel = new Panel();
            label2 = new Label();
            date1 = new DateTime(0, 0);
            label1.PreviewKeyDown += Label1_PreviewKeyDown;
        }

        public void Reload()
        {
            RemoveObjects(); // Удаляет старые объекты
            initObjects(); // Инициализирует новые объекты
            setStyles(); // Отрисовывает стили для новых объектов
            clearStat(); // Очищает статистику
            newGame(rang); // Запуск новой игры
        }

        // Инициализация игры с различными уровнями сложности
        private void gameModeHard(object sender, EventArgs e) // Сложный уровень
        {
            rang = 5; // Задание размера сетки
            Reload();
        }
        public void gameModeMed(object sender, EventArgs e) // Средний уровень сложности
        {
            rang = 4;
            Reload();
        }

        private void gameModeEasy(object sender, EventArgs e) // Легкий уровень сложности
        {
            rang = 3;
            Reload();
        }

        private void mix(object sender, EventArgs e) // Функция для перемешивания
        {
            clearStat(); // Очищает статистику
            newGame(rang); // Запуск новой игры

            int value = 0;

            if (((ToolStripMenuItem)sender).Name == "mixEasy")
            {
                value = 10;
            }
            else if (((ToolStripMenuItem)sender).Name == "mixMed")
            {
                value = 25;
            }
            else if (((ToolStripMenuItem)sender).Name == "mixHard")
            {
                value = 50;
            }
            else if (((ToolStripMenuItem)sender).Name == "mixExtr")
            {
                value = 100;
            }

            for (int p = 0; p < value; p++)
            {
                canMoveMix();
            }
        }

        public void checkWin() // Проверка выигрыша
        {
            bool flag = true;
            for (int i = 0; i < buttonArr.Length; i++)
            {
                if (buttonArr[i].Location != winArray[i])
                {
                    flag = false;
                }
            }

            if (flag)
            {
                timer1.Stop();
                MessageBox.Show(string.Format("Поздравляем, вы завершили игру за {0} ходов за {1}!", steps, label2.Text));
                Reload();

            }
        }
        public void newGame(int rank) // Новая игра
        {
            if (timer1.Enabled == true)
            {
                timer1.Enabled = false;
                date1 = new DateTime(0, 0);
                timer1.Enabled = true;
            }
            else
                timer1.Enabled = true;

            winArray = new Point[buttonArr.Length];
            int k = 0;
            int m = 0;
            for (int i = 0; i < buttonArr.Length; i++)
            {
                if (k == rank)
                {
                    k = 0;
                    m++;
                }

                buttonArr[i].Location = new Point(k * 100 + StartPoint.X, m * 100 + StartPoint.Y);
                winArray[i] = buttonArr[i].Location;
                k++;
            }
        }
        private void Restart(object sender, EventArgs e) // Рестарт
        {
            Reload();
        }

        public void canMoveKey(string key) // проверка двжиения по нажатию на клавиши
        {

            Point gamePoint = buttonArr[buttonArr.Length - 1].Location;
            Point right = new Point(gamePoint.X + 100, gamePoint.Y);
            Point left = new Point(gamePoint.X - 100, gamePoint.Y);
            Point up = new Point(gamePoint.X, gamePoint.Y - 100);
            Point down = new Point(gamePoint.X, gamePoint.Y + 100);

            switch (key)
            {
                case "Left":
                    for (int i = 0; i < buttonArr.Length; i++)
                    {
                        if (left == buttonArr[i].Location)
                        {
                            (buttonArr[buttonArr.Length - 1].Location, buttonArr[i].Location) = (buttonArr[i].Location, (buttonArr[buttonArr.Length - 1].Location));
                            updateStat();
                            checkWin();
                            break;
                        }

                    }
                    break;

                case "Right":
                    for (int i = 0; i < buttonArr.Length; i++)
                    {
                        if (right == buttonArr[i].Location)
                        {
                            (buttonArr[buttonArr.Length - 1].Location, buttonArr[i].Location) = (buttonArr[i].Location, (buttonArr[buttonArr.Length - 1].Location));
                            updateStat();
                            checkWin();
                            break;
                        }
                    }
                    break;

                case "Up":
                    for (int i = 0; i < buttonArr.Length; i++)
                    {
                        if (up == buttonArr[i].Location)
                        {
                            (buttonArr[buttonArr.Length - 1].Location, buttonArr[i].Location) = (buttonArr[i].Location, (buttonArr[buttonArr.Length - 1].Location));
                            updateStat();
                            checkWin();
                            break;
                        }
                    }
                    break;

                case "Down":
                    for (int i = 0; i < buttonArr.Length; i++)
                    {
                        if (down == buttonArr[i].Location)
                        {
                            (buttonArr[buttonArr.Length - 1].Location, buttonArr[i].Location) = (buttonArr[i].Location, (buttonArr[buttonArr.Length - 1].Location));
                            updateStat();
                            checkWin();
                            break;
                        }
                    }
                    break;
            }

        }

        public void updateStat() // Обновление статистики
        {
            steps++;
            label1.Text = string.Format("Шагов: {0}", steps.ToString());
        }

        public void canMoveMix() // Перемещение пустой области на одну клетку без повторений предыдущего хода
        {
            Random rnd = new Random();
            Point gamePoint = buttonArr[buttonArr.Length - 1].Location;

            Point rigth = new Point(gamePoint.X + 100, gamePoint.Y);
            Point left = new Point(gamePoint.X - 100, gamePoint.Y);
            Point up = new Point(gamePoint.X, gamePoint.Y - 100);
            Point down = new Point(gamePoint.X, gamePoint.Y + 100);

            List<RandomMove> listCh = new List<RandomMove>()
            {

            };

            for (int i = 0; i < buttonArr.Length; i++)
            {
                if (rigth == buttonArr[i].Location)
                {
                    listCh.Add(new RandomMove() { move = 0, number = i, neighborButt = buttonArr[i].Location });
                }
                if (left == buttonArr[i].Location)
                {
                    listCh.Add(new RandomMove() { move = 1, number = i, neighborButt = buttonArr[i].Location });
                }
                if (up == buttonArr[i].Location)
                {
                    listCh.Add(new RandomMove() { move = 2, number = i, neighborButt = buttonArr[i].Location });
                }
                if (down == buttonArr[i].Location)
                {
                    listCh.Add(new RandomMove() { move = 3, number = i, neighborButt = buttonArr[i].Location });
                }
            }

            while (true)
            {
                int index = new Random().Next(0, listCh.Count);
                if (listCh[index].neighborButt != lastPos)
                {
                    lastPos = gamePoint;
                    gamePoint = listCh[index].neighborButt;
                    buttonArr[listCh[index].number].Location = buttonArr[buttonArr.Length - 1].Location;
                    buttonArr[buttonArr.Length - 1].Location = gamePoint;
                    break;
                }
            }

        }

        class RandomMove // Класс для хранения соседних кнопок и последующего выбора при перемешивании
        {
            public int move { get; set; }
            public int number { get; set; }
            public Point neighborButt { get; set; }

        }

        public void clearStat() // Очистка статистики
        {
            steps = 0;
            label2.Text = "00:00";
            label1.Text = string.Format("Шагов: {0}", steps.ToString());
        }

        public void canMove(Control butt) // Проверка на возможность ходить
        {
            bool flag = false;
            Point gamePoint = butt.Location;

            Point rigth = new Point(gamePoint.X + 100, gamePoint.Y);
            Point left = new Point(gamePoint.X - 100, gamePoint.Y);
            Point up = new Point(gamePoint.X, gamePoint.Y - 100);
            Point down = new Point(gamePoint.X, gamePoint.Y + 100);

            for (int i = 0; i < buttonArr.Length; i++)
            {
                if (rigth == buttonArr[buttonArr.Length - 1].Location)
                {
                    flag = true;
                }
                if (left == buttonArr[buttonArr.Length - 1].Location)
                {
                    flag = true;
                }
                if (up == buttonArr[buttonArr.Length - 1].Location)
                {
                    flag = true;
                }
                if (down == buttonArr[buttonArr.Length - 1].Location)
                {
                    flag = true;
                }
            }

            if (flag)
            {
                updateStat();
                gamePoint = butt.Location;
                butt.Location = buttonArr[buttonArr.Length - 1].Location;
                buttonArr[buttonArr.Length - 1].Location = gamePoint;
                checkWin();
            }

        }

        private void button_click(object sender, EventArgs e) // Функция для всех кнопок
        {

            canMove(((Control)sender));

        }

        private void fm_Load(object sender, EventArgs e)
        {

        }
    }
}
