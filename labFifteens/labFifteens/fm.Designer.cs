﻿namespace labFifteens
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.restart = new System.Windows.Forms.ToolStripMenuItem();
            this.рестартToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mixEasy = new System.Windows.Forms.ToolStripMenuItem();
            this.mixMed = new System.Windows.Forms.ToolStripMenuItem();
            this.mixHard = new System.Windows.Forms.ToolStripMenuItem();
            this.mixExtr = new System.Windows.Forms.ToolStripMenuItem();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.параметрыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buColor = new System.Windows.Forms.ToolStripMenuItem();
            this.режимToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.mode3 = new System.Windows.Forms.ToolStripMenuItem();
            this.mode4 = new System.Windows.Forms.ToolStripMenuItem();
            this.mode5 = new System.Windows.Forms.ToolStripMenuItem();
            this.help = new System.Windows.Forms.ToolStripMenuItem();
            this.menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // menu
            // 
            this.menu.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.restart,
            this.рестартToolStripMenuItem,
            this.параметрыToolStripMenuItem,
            this.help});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(484, 29);
            this.menu.TabIndex = 0;
            this.menu.Text = "menuStrip1";
            // 
            // restart
            // 
            this.restart.Name = "restart";
            this.restart.Size = new System.Drawing.Size(97, 25);
            this.restart.Text = "New game";
            // 
            // рестартToolStripMenuItem
            // 
            this.рестартToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mixEasy,
            this.mixMed,
            this.mixHard,
            this.mixExtr});
            this.рестартToolStripMenuItem.Name = "рестартToolStripMenuItem";
            this.рестартToolStripMenuItem.Size = new System.Drawing.Size(47, 25);
            this.рестартToolStripMenuItem.Text = "Mix";
            // 
            // mixEasy
            // 
            this.mixEasy.Name = "mixEasy";
            this.mixEasy.Size = new System.Drawing.Size(180, 26);
            this.mixEasy.Text = "Easy";
            // 
            // mixMed
            // 
            this.mixMed.Name = "mixMed";
            this.mixMed.Size = new System.Drawing.Size(180, 26);
            this.mixMed.Text = "Medium";
            // 
            // mixHard
            // 
            this.mixHard.Name = "mixHard";
            this.mixHard.Size = new System.Drawing.Size(180, 26);
            this.mixHard.Text = "Hard";
            // 
            // mixExtr
            // 
            this.mixExtr.Name = "mixExtr";
            this.mixExtr.Size = new System.Drawing.Size(180, 26);
            this.mixExtr.Text = "Extreme";
            // 
            // параметрыToolStripMenuItem
            // 
            this.параметрыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buColor,
            this.режимToolStripMenuItem1});
            this.параметрыToolStripMenuItem.Name = "параметрыToolStripMenuItem";
            this.параметрыToolStripMenuItem.Size = new System.Drawing.Size(78, 25);
            this.параметрыToolStripMenuItem.Text = "Settings";
            // 
            // buColor
            // 
            this.buColor.Name = "buColor";
            this.buColor.Size = new System.Drawing.Size(180, 26);
            this.buColor.Text = "Color";
            // 
            // режимToolStripMenuItem1
            // 
            this.режимToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mode3,
            this.mode4,
            this.mode5});
            this.режимToolStripMenuItem1.Name = "режимToolStripMenuItem1";
            this.режимToolStripMenuItem1.Size = new System.Drawing.Size(180, 26);
            this.режимToolStripMenuItem1.Text = "Mode";
            // 
            // mode3
            // 
            this.mode3.Name = "mode3";
            this.mode3.Size = new System.Drawing.Size(180, 26);
            this.mode3.Text = "3х3";
            // 
            // mode4
            // 
            this.mode4.Name = "mode4";
            this.mode4.Size = new System.Drawing.Size(180, 26);
            this.mode4.Text = "4х4";
            // 
            // mode5
            // 
            this.mode5.Name = "mode5";
            this.mode5.Size = new System.Drawing.Size(180, 26);
            this.mode5.Text = "5х5";
            // 
            // help
            // 
            this.help.Name = "help";
            this.help.Size = new System.Drawing.Size(54, 25);
            this.help.Text = "Help";
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 461);
            this.Controls.Add(this.menu);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.251F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MainMenuStrip = this.menu;
            this.Name = "fm";
            this.ShowIcon = false;
            this.Text = "labFifteens";
            this.Load += new System.EventHandler(this.fm_Load);
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem restart;
        private System.Windows.Forms.ToolStripMenuItem рестартToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mixEasy;
        private System.Windows.Forms.ToolStripMenuItem mixMed;
        private System.Windows.Forms.ToolStripMenuItem mixHard;
        private System.Windows.Forms.ToolStripMenuItem mixExtr;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.ToolStripMenuItem параметрыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buColor;
        private System.Windows.Forms.ToolStripMenuItem режимToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem mode3;
        private System.Windows.Forms.ToolStripMenuItem mode4;
        private System.Windows.Forms.ToolStripMenuItem mode5;
        private System.Windows.Forms.ToolStripMenuItem help;
    }
}

