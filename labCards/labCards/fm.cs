﻿using labCards.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labCards
{
    public partial class fm : Form
    {
        Graphics g;
        ImageBox ImageBox;
        private CardPack cardPack;
        private int cardCount = 10;
        private Image b;

        private Point StartPoint;
        private Point CurPoint;

        public fm()
        {
            InitializeComponent();
           
            pictureBox1.BackgroundImage = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            pictureBox1.BackgroundImageLayout = ImageLayout.None;
            b = new Bitmap(pictureBox1.BackgroundImage);
            g = Graphics.FromImage(b);

            //ImageBox = new ImageBox(Resources.Karty_dlya_Fotoshopa_variant_3, 4, 13, 13 * 4);
            ImageBox = new ImageBox(Resources.Karty_dlya_Fotoshopa_variant_3, 4, 13);
            
            cardPack = new CardPack(ImageBox.Count);
            RandomCards();

            button1.Click += (s, e) => RandomCards();
            button2.Click += (s, e) => Rand();

            pictureBox1.MouseDown += PictureBox1_MouseDown;
            pictureBox1.MouseMove += PictureBox1_MouseMove;
            pictureBox1.Paint += PictureBox1_Paint;

            pictureBox1.BackColor = Color.Green;

        }

        private void PictureBox1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(b, CurPoint);
        }

        private void PictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                CurPoint.X += e.X - StartPoint.X;
                CurPoint.Y += e.Y - StartPoint.Y;
                StartPoint = e.Location;
                this.Text = $"{CurPoint}";
                pictureBox1.Invalidate();
            }
        }

        private void PictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            StartPoint = new Point(e.X, e.Y);
        }

        private void Rand()
        {
            Random rnd = new Random();
            g = Graphics.FromImage(b);

            //ImageBox = new ImageBox(Resources.Karty_dlya_Fotoshopa_variant_3, 4, 13, 13 * 4);
            ImageBox = new ImageBox(Resources.Karty_dlya_Fotoshopa_variant_3, 4, 13);
            cardPack = new CardPack(ImageBox.Count);

            g.Clear(Color.Green);

            for (int i = 0; i < cardCount; i++)
            {
                g.DrawImage(ImageBox[cardPack[i]],
                   rnd.Next(this.Width - ImageBox[cardPack[i]].Width),
                   rnd.Next(this.Height - ImageBox[cardPack[i]].Height));
            }

            pictureBox1.Invalidate();

        }

        

        private void RandomCards() //test
        {
            g = Graphics.FromImage(b);

            //ImageBox = new ImageBox(Resources.Karty_dlya_Fotoshopa_variant_3, 4, 13, 13 * 4);
            ImageBox = new ImageBox(Resources.Karty_dlya_Fotoshopa_variant_3, 4, 13);
            cardPack = new CardPack(ImageBox.Count);

            g.Clear(Color.Green);
            g.TranslateTransform(ClientSize.Width / 4, ClientSize.Height / 2);
            g.RotateTransform(-20);
            for (int i = 0; i < cardCount; i++)
            {
                g.RotateTransform(3);
                g.DrawImage(ImageBox[cardPack[i]], ImageBox[cardPack[i]].Width / 4 * i, 100 - i * 6);
            }
            pictureBox1.Invalidate();
        }
    }
}
