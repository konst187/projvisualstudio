﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labTuple
{
    class Program
    {
        static void Main(string[] args)
        {

            //var x0 = 5;
            //int x00 = 5;

            var x1 = (2, 4);
            //(int, int) x11 = (2, 4);

            Console.WriteLine(x1.Item1);
            Console.WriteLine(x1.Item2);
            Console.WriteLine();

            //(2) название полей
            (int min, int max) x2 = (2, 4);
            Console.WriteLine(x2.max);
            Console.WriteLine(x2.min);
            Console.WriteLine();

            //(3) название полей кортежа через инициализацию

            var x3 = (min: 2, max: 4);
            Console.WriteLine(x3.max);
            Console.WriteLine(x3.min);
            Console.WriteLine();

            //(4) Отдельные переменныые для каждого поля
            var (min, max) = (2, 4);
            Console.WriteLine(max);
            Console.WriteLine(min);
            Console.WriteLine();

            //(5) Получение кортежа из какой то функции

            var x5 = GetX5();
            Console.WriteLine(x5.Item1);
            Console.WriteLine(x5.Item2);
            Console.WriteLine();

            //(6) Получение кортежа из функции с именами

            var x6 = GetX6();
            Console.WriteLine(x6.min);
            Console.WriteLine(x6.max);
            Console.WriteLine();

            //(7) Кортеж в качестве параметра функции

            var x7 = GetX7((4, 5), 6);
            Console.WriteLine(x7.Item1);
            Console.WriteLine(x7.Item2);
            Console.WriteLine();
        }

        private static (int, int) GetX7((int, int) p, int v) => (p.Item1 + p.Item2, v);

        //private static (int, int) GetX7((int, int) p, int v)
        //{
        //    return (p.Item1 + p.Item2, v);
        //}

        private static (int min, int max) GetX6()
        {
            return (3, 6);
        }

        private static (int,int) GetX5()
        {
            return (3, 6);
        }
    }
}
