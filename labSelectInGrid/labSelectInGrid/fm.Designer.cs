﻿namespace labSelectInGrid
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.pxImage = new System.Windows.Forms.PictureBox();
            this.trackRow = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.trackCol = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackCol)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.trackCol);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.trackRow);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 445);
            this.panel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pxImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pxImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pxImage.Location = new System.Drawing.Point(200, 0);
            this.pxImage.Name = "pictureBox1";
            this.pxImage.Size = new System.Drawing.Size(310, 445);
            this.pxImage.TabIndex = 1;
            this.pxImage.TabStop = false;
            // 
            // trackRow
            // 
            this.trackRow.Location = new System.Drawing.Point(6, 25);
            this.trackRow.Name = "trackRow";
            this.trackRow.Size = new System.Drawing.Size(188, 45);
            this.trackRow.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Rows";
            // 
            // trackCol
            // 
            this.trackCol.Location = new System.Drawing.Point(6, 86);
            this.trackCol.Name = "trackCol";
            this.trackCol.Size = new System.Drawing.Size(188, 45);
            this.trackCol.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Cols";
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 445);
            this.Controls.Add(this.pxImage);
            this.Controls.Add(this.panel1);
            this.Name = "fm";
            this.Text = "labSelectInGrid";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackCol)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TrackBar trackCol;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar trackRow;
        private System.Windows.Forms.PictureBox pxImage;
    }
}

