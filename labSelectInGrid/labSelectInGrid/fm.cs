﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labSelectInGrid
{
    public partial class fm : Form
    {
        private Bitmap b;

        public int Rows { get; private set; } = 3;
        public int Cols { get; private set; } = 4;
        public int CellWidth { get; private set; }
        public int CellHeight { get; private set; }
        public int CurRow { get; private set; }
        public int CurCol { get; private set; }

        public fm()
        {
            InitializeComponent();

            ResizeCells();
            DrawCells();

            pxImage.MouseMove += PxImage_MouseMove;
            pxImage.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);
            pxImage.Resize += (s, e) => { ResizeCells(); pxImage.Invalidate(); };

            trackRow.Minimum = 1;
            trackRow.Maximum = 10;
            trackCol.Maximum = 10;
            trackCol.Minimum = 1;
            trackRow.Value = Rows;
            trackCol.Value = Cols;
            trackRow.ValueChanged += (s, e) => { Rows = trackRow.Value; ResizeCells(); pxImage.Invalidate(); };
            trackCol.ValueChanged += (s, e) => { Cols = trackCol.Value; ResizeCells(); pxImage.Invalidate(); };
        }

        private void PxImage_MouseMove(object sender, MouseEventArgs e)
        {
            CurCol = e.X / CellWidth;
            CurRow = e.Y / CellHeight;
            this.Text = $"{CurRow}, {CurCol}";
            ResizeCells();
            pxImage.Invalidate();
        }

     
        private void DrawCells()
        {
           
            using (var g = Graphics.FromImage(b))
            {
                g.Clear(DefaultBackColor);
                for (int i = 0; i < Rows; i++)
                {
                    g.DrawLine(new Pen(Color.Green, 1), 0, i * CellHeight, Cols * CellWidth, i* CellHeight);

                }

                for (int j = 0; j < Cols; j++)
                {
                      g.DrawLine(new Pen(Color.Green, 1), j * CellWidth,0, j * CellWidth, Rows* CellHeight);
                }
                g.DrawString($"{CurRow}, {CurCol}", new Font("",40), new SolidBrush(Color.Black), CurCol * CellWidth, CurRow * CellHeight);
                g.DrawRectangle(new Pen(Color.Red, 3), CurCol * CellWidth, CurRow * CellHeight, CellWidth, CellHeight);
            }

        }

        private void ResizeCells()
        {
            b = new Bitmap(Width, Height);
            CellWidth = pxImage.Width / Cols;
            CellHeight = pxImage.Height / Rows;
            DrawCells();


        }
    }
}
