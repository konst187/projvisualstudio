﻿namespace labFormDialogs
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.TextDialog = new System.Windows.Forms.TextBox();
            this.buOpenFileDialog = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // openDialog
            // 
            this.TextDialog.Location = new System.Drawing.Point(12, 33);
            this.TextDialog.Name = "openDialog";
            this.TextDialog.Size = new System.Drawing.Size(407, 20);
            this.TextDialog.TabIndex = 0;
            // 
            // buOpenFileDialog
            // 
            this.buOpenFileDialog.Location = new System.Drawing.Point(443, 30);
            this.buOpenFileDialog.Name = "buOpenFileDialog";
            this.buOpenFileDialog.Size = new System.Drawing.Size(75, 23);
            this.buOpenFileDialog.TabIndex = 1;
            this.buOpenFileDialog.Text = "button1";
            this.buOpenFileDialog.UseVisualStyleBackColor = true;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(545, 458);
            this.Controls.Add(this.buOpenFileDialog);
            this.Controls.Add(this.TextDialog);
            this.Name = "fm";
            this.Text = "labFormDialogs";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.TextBox TextDialog;
        private System.Windows.Forms.Button buOpenFileDialog;
    }
}

