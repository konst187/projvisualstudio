﻿namespace labFileExplorerOnWB
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buDirSelect = new System.Windows.Forms.Button();
            this.edDir = new System.Windows.Forms.TextBox();
            this.buUp = new System.Windows.Forms.Button();
            this.buForward = new System.Windows.Forms.Button();
            this.buBack = new System.Windows.Forms.Button();
            this.wb = new System.Windows.Forms.WebBrowser();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.buDirSelect);
            this.panel1.Controls.Add(this.edDir);
            this.panel1.Controls.Add(this.buUp);
            this.panel1.Controls.Add(this.buForward);
            this.panel1.Controls.Add(this.buBack);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(742, 75);
            this.panel1.TabIndex = 0;
            // 
            // buDirSelect
            // 
            this.buDirSelect.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buDirSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buDirSelect.Location = new System.Drawing.Point(667, 28);
            this.buDirSelect.Name = "buDirSelect";
            this.buDirSelect.Size = new System.Drawing.Size(52, 21);
            this.buDirSelect.TabIndex = 4;
            this.buDirSelect.Text = "...";
            this.buDirSelect.UseVisualStyleBackColor = true;
            // 
            // edDir
            // 
            this.edDir.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edDir.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edDir.Location = new System.Drawing.Point(177, 28);
            this.edDir.Name = "edDir";
            this.edDir.Size = new System.Drawing.Size(484, 26);
            this.edDir.TabIndex = 3;
            // 
            // buUp
            // 
            this.buUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buUp.Location = new System.Drawing.Point(119, 28);
            this.buUp.Name = "buUp";
            this.buUp.Size = new System.Drawing.Size(52, 23);
            this.buUp.TabIndex = 2;
            this.buUp.Text = "▲";
            this.buUp.UseVisualStyleBackColor = true;
            // 
            // buForward
            // 
            this.buForward.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buForward.Location = new System.Drawing.Point(61, 28);
            this.buForward.Name = "buForward";
            this.buForward.Size = new System.Drawing.Size(52, 23);
            this.buForward.TabIndex = 1;
            this.buForward.Text = "▶";
            this.buForward.UseVisualStyleBackColor = true;
            // 
            // buBack
            // 
            this.buBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buBack.Location = new System.Drawing.Point(3, 28);
            this.buBack.Name = "buBack";
            this.buBack.Size = new System.Drawing.Size(52, 23);
            this.buBack.TabIndex = 0;
            this.buBack.Text = "◀";
            this.buBack.UseVisualStyleBackColor = true;
            // 
            // wb
            // 
            this.wb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wb.Location = new System.Drawing.Point(0, 75);
            this.wb.MinimumSize = new System.Drawing.Size(20, 20);
            this.wb.Name = "wb";
            this.wb.Size = new System.Drawing.Size(742, 477);
            this.wb.TabIndex = 1;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 552);
            this.Controls.Add(this.wb);
            this.Controls.Add(this.panel1);
            this.Name = "fm";
            this.Text = "labFileExplorerOnWB";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buDirSelect;
        private System.Windows.Forms.TextBox edDir;
        private System.Windows.Forms.Button buUp;
        private System.Windows.Forms.Button buForward;
        private System.Windows.Forms.Button buBack;
        private System.Windows.Forms.WebBrowser wb;
    }
}

