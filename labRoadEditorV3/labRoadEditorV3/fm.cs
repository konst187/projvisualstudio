﻿using labRoadEditorV3.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labRoadEditorV3
{
    public partial class fm : Form
    {
        //Элементы графической отрисовки
        private PictureBox[,] pics;
        private Bitmap[,] BitPics;
        private Bitmap bitRoadMap;
        private Bitmap BitCurrntRoadPicture;

        // Точки для ориентирования по карте
        private Point StartPointRM;
        private Point CurPointRM;
        public int Cols { get; private set; } = 4;
        public int Rows { get; private set; } = 3;
        public int CurRowRM { get; private set; }
        public int CurColRM { get; private set; }

        // Номер текущей выбранной ячейки из BitPics
        private int testRow;
        private int testCol;
        // Размеры карты
        private int MAP_HEIGHT = 0;
        private int MAP_WIDTH = 0;

        // Количество ячеек
        private int CELLS_X = 20;
        private int CELLS_Y = 20;

        private int RoadCellWidthMini = 0;
        private int RoadCellHeighMini = 0;

        // Размеры клетки на карте
        private int HeightCellImage = 150;
        private int WidthCellImage = 150;

        // Словарь для соотношения кодировки файла с ячейкой карты
        private Dictionary<string, Bitmap> dictMap =
    new Dictionary<string, Bitmap>();

        // Массив, который хранит информацию о всех ячейках, в закодированном виде
        // нужен для сохранения
        private string[,] MapCells;

        private int DelataZoom = 10;

        Label rowCount;
        Label colCount;
        Form form2;
        Button buOk;
        NumericUpDown text1;
        NumericUpDown text2;

        public fm()
        {
            InitializeComponent();

            InitVars();

            CurrPicture.Height = HeightCellImage;
            CurrPicture.Width = WidthCellImage;
            BitCurrntRoadPicture = new Bitmap(CurrPicture.Width, CurrPicture.Height);

            RoadMap.MouseWheel += RoadMap_MouseWheel;
            buClear.Click += BuClear_Click;
            buLoad.Click += BuLoad_Click;
            buSave.Click += BuSave_Click;
            RoadMap.MouseDown += RoadMap_MouseDown;
            RoadMap.MouseMove += RoadMap_MouseMove;
            RoadMap.Paint += RoadMap_Paint;
            this.KeyDown += Fm_KeyDown;
            CurrPicture.Paint += CurrPicture_Paint;

            buSize.Click += BuSettings_Click;

            SetStyles();
            CreateCell();
            ResizeCells();
            StartLocationCells();
            DrawCells();
            ResizeOneCell(pics[1, 1]);

            buOk.Click += BuOk_Click;
            buFill.Click += BuFill_Click;


        }

        private void BuFill_Click(object sender, EventArgs e)
        {
            var g = Graphics.FromImage(bitRoadMap);

            g.Clear(Color.FromArgb(22, 22, 28));

            for (int i = 0; i < CELLS_X; i++)
            {
                for (int j = 0; j < CELLS_Y; j++)
                {
                    foreach (KeyValuePair<string, Bitmap> kvp in dictMap)
                    {
                        if (kvp.Value == BitPics[testRow, testCol])
                        {
                            MapCells[i, j] = kvp.Key;
                            g.DrawImage(kvp.Value, new Rectangle(i * HeightCellImage, j * WidthCellImage, WidthCellImage, HeightCellImage),
                            new Rectangle(0, 0, kvp.Value.Width, kvp.Value.Height), GraphicsUnit.Pixel);
                            break;
                        }
                    }
                }
            }

            g.DrawRectangle(new Pen(Color.Gray, 3), 0, 0, MAP_WIDTH, MAP_HEIGHT);
            g.Dispose();
            RoadMap.Refresh();
        }

        private void BuOk_Click(object sender, EventArgs e)
        {
            CELLS_Y = (int)text1.Value;
            CELLS_X = (int)text2.Value;
            InitVars();
            ClearMap();
            form2.Close();

        }

        private void BuSettings_Click(object sender, EventArgs e)
        {

            form2.ShowDialog();

        }

        private void RoadMap_MouseWheel(object sender, MouseEventArgs e)
        {
            int x = CurRowRM * 10;
            int y = CurColRM * 10;

            if (e.Delta > 0)
            {
                if (WidthCellImage < 150)
                {
                    WidthCellImage += DelataZoom;
                    HeightCellImage += DelataZoom;
                    CurPointRM = new Point(CurPointRM.X - y, CurPointRM.Y - x);
                }
            }
            else
            {
                if (WidthCellImage > 30)
                {
                    WidthCellImage -= DelataZoom;
                    HeightCellImage -= DelataZoom;
                    CurPointRM = new Point(CurPointRM.X + y, CurPointRM.Y + x);
                }
            }

            MAP_WIDTH = CELLS_X * WidthCellImage;
            MAP_HEIGHT = CELLS_Y * WidthCellImage;
            bitRoadMap = new Bitmap(MAP_WIDTH, MAP_HEIGHT);
            DrawCells();

        }

        private void BuClear_Click(object sender, EventArgs e)
        {
            ClearMap();
        }

        private void BuSave_Click(object sender, EventArgs e)
        {
            var path = System.IO.Path.GetFullPath(@"config");
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            if (!dirInfo.Exists)
            {
                Directory.CreateDirectory(path);
            }

            if (!File.Exists(@"config\config.txt"))
            {
                StreamWriter sw = File.CreateText(@"config\config.txt");
                sw.Close();
            }

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";

            openFileDialog.InitialDirectory = path;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                SaveCells(openFileDialog.FileName);
            }

        }

        private void BuLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";

            var path = System.IO.Path.GetFullPath(@"config");
            openFileDialog.InitialDirectory = path;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                LoadCell(openFileDialog.FileName);
            }

        }

        private void CurrPicture_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(BitCurrntRoadPicture, new Point(0, 0));
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:

                    break;
                case Keys.F2:

                    break;
            }
        }

        private void InitVars()
        {
            MAP_WIDTH = CELLS_X * WidthCellImage;
            MAP_HEIGHT = CELLS_Y * HeightCellImage;
            RoadCellWidthMini = RoadAll.Width / Cols;
            RoadCellHeighMini = RoadAll.Height / Rows;
            MapCells = new string[CELLS_X, CELLS_Y];
            bitRoadMap = new Bitmap(MAP_WIDTH, MAP_HEIGHT);

            // Заполнение всего массива пустыми клетками
            for (int i = 0; i < CELLS_X; i++)
            {
                for (int j = 0; j < CELLS_Y; j++)
                {
                    MapCells[i, j] = "z";
                }
            }
            // Перемещение в центр карты при запуске приложения
            CurPointRM = new Point((RoadMap.Width - bitRoadMap.Width) / 2, (RoadMap.Height - bitRoadMap.Height) / 2);
        }

        private void SetStyles() // Стили
        {
            RoadMap.BorderStyle = BorderStyle.FixedSingle;
            RoadAll.BorderStyle = BorderStyle.FixedSingle;
            CurrPicture.BorderStyle = BorderStyle.FixedSingle;

            this.BackColor = Color.FromArgb(37, 6, 79);
            CurrPicture.BackColor = Color.FromArgb(22, 22, 28);
            RoadMap.BackColor = Color.FromArgb(22, 22, 28);
            RoadAll.BackColor = Color.FromArgb(22, 22, 28);
            RoadAll.ForeColor = Color.FromArgb(22, 22, 28);
            menuStrip1.BackColor = Color.FromArgb(22, 22, 28);
            menuStrip1.ForeColor = Color.FromArgb(252, 252, 252);
            buSize.BackColor = Color.FromArgb(22, 22, 28);
            buSize.ForeColor = Color.FromArgb(252, 252, 252);

            CurrPicture.Location = new Point(20, CurrPicture.Location.Y);

            form2 = new Form();
            form2.BackColor = Color.FromArgb(22, 22, 28);
            form2.ForeColor = Color.FromArgb(252, 252, 252);
            form2.ShowIcon = false;
            form2.Text = "Изменение размера";
            form2.Size = new Size(300, 300);
            form2.MinimumSize = new Size(300, 250);
            form2.MaximumSize = new Size(300, 250);
            form2.StartPosition = FormStartPosition.CenterScreen;

            buOk = new Button();
            buOk.Text = "Подвердить";
            buOk.Dock = DockStyle.Bottom;
            buOk.Font = new Font("Tahoma", 14, FontStyle.Bold);
            buOk.BackColor = Color.FromArgb(37, 6, 79);
            buOk.ForeColor = Color.FromArgb(252, 252, 252);
            buOk.Height = 40;
            form2.Controls.Add(buOk);


            text1 = new NumericUpDown();
            text2 = new NumericUpDown();

            text1.BackColor = Color.FromArgb(22, 22, 28);
            text2.BackColor = Color.FromArgb(22, 22, 28);
            text1.ForeColor = ForeColor = Color.FromArgb(252, 252, 252);
            text2.ForeColor = ForeColor = Color.FromArgb(252, 252, 252);
            text1.Value = CELLS_X;
            text2.Value = CELLS_Y;
            text1.Maximum = 40;
            text1.Minimum = 1;
            text2.Maximum = 40;
            text2.Minimum = 1;

            text1.Width = form2.Width - 20;
            text2.Width = form2.Width - 20;
            text1.Font = new Font("Tahoma", 14, FontStyle.Bold);
            text1.ReadOnly = true;
            text2.ReadOnly = true;
            text2.Font = new Font("Tahoma", 14, FontStyle.Bold);
            text1.Height = 50;
            text2.Height = 50;
            text1.Location = new Point(0, 40);
            text2.Location = new Point(0, 120);

            rowCount = new Label();
            colCount = new Label();
            rowCount.Width = form2.Width;
            colCount.Width = form2.Width;
            rowCount.Font = new Font("Tahoma", 14, FontStyle.Bold);
            colCount.Font = new Font("Tahoma", 14, FontStyle.Bold);
            rowCount.Height = 40;
            colCount.Height = 40;

            rowCount.Text = $"Строки:";
            colCount.Text = $"Столбцы:";
            rowCount.Location = new Point(0, 0);
            colCount.Location = new Point(0, 80);
            form2.Controls.Add(rowCount);
            form2.Controls.Add(colCount);
            form2.Controls.Add(text1);
            form2.Controls.Add(text2);

            buFill.BackColor = Color.FromArgb(22, 22, 28);
            buFill.ForeColor = Color.FromArgb(252, 252, 252);


        }


        private bool CheckField()
        {
            if (CurColRM < 0 || CurRowRM < 0 || CurColRM >= CELLS_X || CurRowRM >= CELLS_Y)
                return false;

            return true;
        }

        private void RoadMap_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImageUnscaled(bitRoadMap, CurPointRM);
        }

        private void RoadMap_MouseMove(object sender, MouseEventArgs e)
        {
            CurRowRM = (e.Y - CurPointRM.Y) / WidthCellImage;
            CurColRM = (e.X - CurPointRM.X) / HeightCellImage;
            label2.Text = $"Номер ячейки: {CurRowRM}, {CurColRM}";
            RoadMap.Refresh();

            if (e.Button == MouseButtons.Right)
            {
                CurPointRM.X += e.X - StartPointRM.X;
                CurPointRM.Y += e.Y - StartPointRM.Y;
                StartPointRM = e.Location;
                RoadMap.Invalidate();
            }
            if ((e.Button == MouseButtons.Left) && (CheckField()))
            {
                foreach (KeyValuePair<string, Bitmap> kvp in dictMap)
                {
                    if (kvp.Value == BitPics[testRow, testCol])
                    {
                        MapCells[CurColRM, CurRowRM] = kvp.Key;
                    }
                }

                var g = Graphics.FromImage(bitRoadMap); // Орисовка выбранного объекта в текущей ячейке на карте
                g.DrawImage(BitPics[testRow, testCol], new Rectangle(CurColRM * WidthCellImage, CurRowRM * HeightCellImage, WidthCellImage, HeightCellImage),
                new Rectangle(0, 0, BitPics[testRow, testCol].Width, BitPics[testRow, testCol].Height), GraphicsUnit.Pixel);
                g.Dispose();
            }
        }
        private void RoadMap_MouseDown(object sender, MouseEventArgs e)
        {
            StartPointRM = new Point(e.X, e.Y);
            if ((e.Button == MouseButtons.Left) && (CheckField()))
            {
                foreach (KeyValuePair<string, Bitmap> kvp in dictMap)
                {
                    if (kvp.Value == BitPics[testRow, testCol])
                    {
                        MapCells[CurColRM, CurRowRM] = kvp.Key;
                    }
                }
                var g = Graphics.FromImage(bitRoadMap); // Орисовка выбранного объекта в текущей ячейке на карте
                g.DrawImage(BitPics[testRow, testCol], new Rectangle(CurColRM * WidthCellImage, CurRowRM * HeightCellImage, WidthCellImage, HeightCellImage),
                new Rectangle(0, 0, BitPics[testRow, testCol].Width, BitPics[testRow, testCol].Height), GraphicsUnit.Pixel);
                g.Dispose();
            }

        }

        private void ResizeCells()
        {
            char a = 'a';
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    int ImageHeight = Resources.Road.Height / Rows;
                    int ImageWidth = Resources.Road.Width / Cols;
                    BitPics[i, j] = new Bitmap(Resources.Road.Clone(new Rectangle(j * ImageWidth, i * ImageHeight, ImageWidth, ImageHeight), System.Drawing.Imaging.PixelFormat.Format32bppArgb));
                    dictMap.Add(a.ToString(), BitPics[i, j]);
                    pics[i, j].Width = RoadCellWidthMini;
                    pics[i, j].Height = RoadCellHeighMini;
                    pics[i, j].Image = new Bitmap(RoadCellWidthMini, RoadCellHeighMini);
                    var g = Graphics.FromImage(pics[i, j].Image);

                    g.DrawImage(BitPics[i, j], new Rectangle(0, 0, pics[i, j].Width, pics[i, j].Height),
                        new Rectangle(0, 0, BitPics[i, j].Width, BitPics[i, j].Height), GraphicsUnit.Pixel);

                    g.Dispose();
                    a++;
                }
            }

        }

        private void StartLocationCells()
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    pics[i, j].Location = new Point(j * RoadCellWidthMini, i * RoadCellHeighMini);
                }
            }
        }

        private void CreateCell()
        {
            BitPics = new Bitmap[Rows, Cols];
            pics = new PictureBox[Rows, Cols];
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    pics[i, j] = new PictureBox();
                    pics[i, j].BorderStyle = BorderStyle.FixedSingle;
                    pics[i, j].MouseUp += Fm_MouseUp;
                    pics[i, j].MouseDown += PictureBoxAll_mouseDown;
                    pics[i, j].MouseMove += PictureBoxAll_mouseMove;
                    RoadAll.Controls.Add(pics[i, j]);
                }
            }
        }
        ///
        private void PictureBoxAll_mouseMove(object sender, MouseEventArgs e)
        {

        }


        private void ResizeOneCell(PictureBox pic)
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    if (pics[i, j].Image == pic.Image)
                    {
                        int ImageHeight = Resources.Road.Height / Rows;
                        int ImageWidth = Resources.Road.Width / Cols;
                        testRow = i;
                        testCol = j;
                        BitCurrntRoadPicture = new Bitmap(CurrPicture.Width, CurrPicture.Height);
                        var g = Graphics.FromImage(BitCurrntRoadPicture);
                        g.DrawImage(BitPics[i, j], new Rectangle(0, 0, CurrPicture.Width, CurrPicture.Height),
                            new Rectangle(0, 0, BitPics[i, j].Width, BitPics[i, j].Height), GraphicsUnit.Pixel);
                        g.Dispose();
                        CurrPicture.Refresh();
                    }
                }
            }
        }

        private void PictureBoxAll_mouseDown(object sender, MouseEventArgs e)
        {
            ResizeOneCell((PictureBox)sender);
        }


        private void LoadCell(string path)
        {
            //string path = @"D:\CFG.txt";

            using (StreamReader streamReader = new StreamReader(path))
            {
                string x = streamReader.ReadLine();
                string y = streamReader.ReadLine();
                int IX = Convert.ToInt32(x);
                int IY = Convert.ToInt32(y);
                CELLS_X = IX;
                CELLS_Y = IY;
                text1.Value = CELLS_X;
                text2.Value = CELLS_Y;

                InitVars();
                DrawCells();
                var g = Graphics.FromImage(bitRoadMap);
                g.Clear(Color.FromArgb(22, 22, 28));
                for (int i = 0; i < CELLS_X; i++)
                {
                    for (int j = 0; j < CELLS_Y; j++)
                    {
                        MapCells[i, j] = Convert.ToChar(streamReader.Read()).ToString();
                        if (MapCells[i, j] == "z")
                            g.DrawRectangle(new Pen(Color.Gray, 1), i * HeightCellImage, j * WidthCellImage, WidthCellImage, HeightCellImage);
                        else
                        {
                            foreach (KeyValuePair<string, Bitmap> kvp in dictMap)
                            {
                                if (kvp.Key == MapCells[i, j])
                                {
                                    g.DrawImage(kvp.Value, new Rectangle(i * HeightCellImage, j * WidthCellImage, WidthCellImage, HeightCellImage),
                                    new Rectangle(0, 0, kvp.Value.Width, kvp.Value.Height), GraphicsUnit.Pixel);
                                    break;
                                }
                            }
                        }
                    }
                }

                g.DrawRectangle(new Pen(Color.Gray, 3), 0, 0, MAP_WIDTH, MAP_HEIGHT);
                g.Dispose();
            }

            CurPointRM = new Point((RoadMap.Width - bitRoadMap.Width) / 2, (RoadMap.Height - bitRoadMap.Height) / 2);
            RoadMap.Invalidate();
        }


        private void SaveCells(string path)
        {
            using (StreamWriter streamWriter = new StreamWriter(path))
            {
                streamWriter.WriteLine(CELLS_X);
                streamWriter.WriteLine(CELLS_Y);
                for (int i = 0; i < CELLS_X; i++)
                {
                    for (int j = 0; j < CELLS_Y; j++)
                    {
                        streamWriter.Write(MapCells[i, j]);
                    }
                }
            }
        }

        private void DrawCells()
        {
            var g = Graphics.FromImage(bitRoadMap);

            g.Clear(Color.FromArgb(22, 22, 28));

            for (int i = 0; i < CELLS_X; i++)
            {
                for (int j = 0; j < CELLS_Y; j++)
                {
                    foreach (KeyValuePair<string, Bitmap> kvp in dictMap)
                    {
                        g.DrawRectangle(new Pen(Color.Gray, 1), i * HeightCellImage, j * WidthCellImage, WidthCellImage, HeightCellImage);
                        if (kvp.Key == MapCells[i, j])
                        {
                            g.DrawImage(kvp.Value, new Rectangle(i * HeightCellImage, j * WidthCellImage, WidthCellImage, HeightCellImage),
                            new Rectangle(0, 0, kvp.Value.Width, kvp.Value.Height), GraphicsUnit.Pixel);
                            break;
                        }
                    }
                }
            }
            g.DrawRectangle(new Pen(Color.Gray, 3), 0, 0, MAP_WIDTH, MAP_HEIGHT);
            g.Dispose();
            RoadMap.Refresh();
        }

        private void ClearMap()
        {
            var g = Graphics.FromImage(bitRoadMap);

            g.Clear(Color.FromArgb(22, 22, 28));

            for (int i = 0; i < CELLS_X; i++)
            {
                for (int j = 0; j < CELLS_Y; j++)
                {
                    MapCells[i, j] = "z";
                    g.DrawRectangle(new Pen(Color.Gray, 1), i * HeightCellImage, j * WidthCellImage, WidthCellImage, HeightCellImage);
                }
            }

            g.DrawRectangle(new Pen(Color.Gray, 3), 0, 0, MAP_WIDTH, MAP_HEIGHT);
            g.Dispose();
            RoadMap.Refresh();

        }


        private void Fm_MouseUp(object sender, MouseEventArgs e)
        {

        }
    }
}
