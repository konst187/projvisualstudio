﻿namespace labRoadEditorV3
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.RoadAll = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.buClear = new System.Windows.Forms.ToolStripMenuItem();
            this.buLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.buSave = new System.Windows.Forms.ToolStripMenuItem();
            this.BuSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.buSize = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CurrPicture = new System.Windows.Forms.PictureBox();
            this.RoadMap = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.buFill = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CurrPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RoadMap)).BeginInit();
            this.SuspendLayout();
            // 
            // RoadAll
            // 
            this.RoadAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.RoadAll.Location = new System.Drawing.Point(12, 438);
            this.RoadAll.Name = "RoadAll";
            this.RoadAll.Size = new System.Drawing.Size(288, 304);
            this.RoadAll.TabIndex = 2;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buClear,
            this.buLoad,
            this.buSave,
            this.BuSettings});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1184, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // buClear
            // 
            this.buClear.Name = "buClear";
            this.buClear.Size = new System.Drawing.Size(71, 20);
            this.buClear.Text = "Очистить";
            // 
            // buLoad
            // 
            this.buLoad.Name = "buLoad";
            this.buLoad.Size = new System.Drawing.Size(73, 20);
            this.buLoad.Text = "Загрузить";
            // 
            // buSave
            // 
            this.buSave.Name = "buSave";
            this.buSave.Size = new System.Drawing.Size(78, 20);
            this.buSave.Text = "Сохранить";
            // 
            // BuSettings
            // 
            this.BuSettings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buSize});
            this.BuSettings.Name = "BuSettings";
            this.BuSettings.Size = new System.Drawing.Size(83, 20);
            this.BuSettings.Text = "Параметры";
            // 
            // buSize
            // 
            this.buSize.Name = "buSize";
            this.buSize.Size = new System.Drawing.Size(159, 22);
            this.buSize.Text = "Размеры карты";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // CurrPicture
            // 
            this.CurrPicture.Location = new System.Drawing.Point(11, 102);
            this.CurrPicture.Name = "CurrPicture";
            this.CurrPicture.Size = new System.Drawing.Size(134, 137);
            this.CurrPicture.TabIndex = 3;
            this.CurrPicture.TabStop = false;
            // 
            // RoadMap
            // 
            this.RoadMap.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RoadMap.BackColor = System.Drawing.SystemColors.Control;
            this.RoadMap.Location = new System.Drawing.Point(318, 37);
            this.RoadMap.Name = "RoadMap";
            this.RoadMap.Size = new System.Drawing.Size(846, 705);
            this.RoadMap.TabIndex = 0;
            this.RoadMap.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Impact", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(12, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 23);
            this.label1.TabIndex = 6;
            this.label1.Text = "Текущий элемент:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Impact", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(12, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 23);
            this.label2.TabIndex = 7;
            this.label2.Text = "Номер ячейки:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Impact", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(8, 412);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(197, 23);
            this.label3.TabIndex = 8;
            this.label3.Text = "Доступные элементы:";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // buFill
            // 
            this.buFill.Font = new System.Drawing.Font("Impact", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buFill.Location = new System.Drawing.Point(16, 258);
            this.buFill.Name = "buFill";
            this.buFill.Size = new System.Drawing.Size(134, 33);
            this.buFill.TabIndex = 9;
            this.buFill.Text = "Заливка";
            this.buFill.UseVisualStyleBackColor = true;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1184, 761);
            this.Controls.Add(this.buFill);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CurrPicture);
            this.Controls.Add(this.RoadAll);
            this.Controls.Add(this.RoadMap);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(800, 700);
            this.Name = "fm";
            this.ShowIcon = false;
            this.Text = "labRoadEditor";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CurrPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RoadMap)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox RoadMap;
        private System.Windows.Forms.Panel RoadAll;
        private System.Windows.Forms.PictureBox CurrPicture;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem buClear;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem buLoad;
        private System.Windows.Forms.ToolStripMenuItem buSave;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem BuSettings;
        private System.Windows.Forms.ToolStripMenuItem buSize;
        private System.Windows.Forms.Button buFill;
    }
}

