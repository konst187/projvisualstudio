﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labCreateControlOnMouse
{
    public partial class fm : Form
    {
        public fm()
        {
            InitializeComponent();

            this.MouseDown += Fm_MouseDown;


        }

        private void Fm_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                var x = new Label();
                x.Location = e.Location;
                x.Text = String.Format("{0}, {1}", e.X, e.Y);
                x.AutoSize = true;
                x.Font = new System.Drawing.Font("Microsoft Sans Serif", 14);


                this.Controls.Add(x);

            }

            if (e.Button == MouseButtons.Right)
            {
                var rnd = new Random();
               
                for (int i = 0; i < 10; i++)
                {
                    var x = new Label();
                    x.Text = String.Format("{0}, {1}", e.X, e.Y);
                    x.Location = new Point(rnd.Next(this.Width - x.Size.Width), rnd.Next(this.Height - x.Size.Height));
                    x.AutoSize = true;
                    x.Font = new System.Drawing.Font("Microsoft Sans Serif", 14);
                    x.ForeColor = Color.Green;
                    this.Controls.Add(x);

                }             

               
            }
        }
    }
}
