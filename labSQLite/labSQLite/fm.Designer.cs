﻿namespace labSQLite
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvLogs = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.edSQL = new System.Windows.Forms.TextBox();
            this.buUsersSh = new System.Windows.Forms.Button();
            this.buNotesSH = new System.Windows.Forms.Button();
            this.buRunOne = new System.Windows.Forms.Button();
            this.buDel = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.edNotes = new System.Windows.Forms.TextBox();
            this.edNotesPr = new System.Windows.Forms.NumericUpDown();
            this.buNotesAdd = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edNotesPr)).BeginInit();
            this.SuspendLayout();
            // 
            // lvLogs
            // 
            this.lvLogs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lvLogs.HideSelection = false;
            this.lvLogs.Location = new System.Drawing.Point(13, 53);
            this.lvLogs.Name = "lvLogs";
            this.lvLogs.Size = new System.Drawing.Size(249, 397);
            this.lvLogs.TabIndex = 0;
            this.lvLogs.UseCompatibleStateImageBehavior = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(205, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Лог запусков приложения";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(279, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "SQL";
            // 
            // edSQL
            // 
            this.edSQL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edSQL.Location = new System.Drawing.Point(283, 53);
            this.edSQL.Multiline = true;
            this.edSQL.Name = "edSQL";
            this.edSQL.Size = new System.Drawing.Size(364, 85);
            this.edSQL.TabIndex = 3;
            this.edSQL.Text = "SELECT * FROM Notes;";
            // 
            // buUsersSh
            // 
            this.buUsersSh.Location = new System.Drawing.Point(283, 145);
            this.buUsersSh.Name = "buUsersSh";
            this.buUsersSh.Size = new System.Drawing.Size(96, 29);
            this.buUsersSh.TabIndex = 4;
            this.buUsersSh.Text = "Пользователи";
            this.buUsersSh.UseVisualStyleBackColor = true;
            // 
            // buNotesSH
            // 
            this.buNotesSH.Location = new System.Drawing.Point(375, 145);
            this.buNotesSH.Name = "buNotesSH";
            this.buNotesSH.Size = new System.Drawing.Size(92, 29);
            this.buNotesSH.TabIndex = 5;
            this.buNotesSH.Text = "Заметки";
            this.buNotesSH.UseVisualStyleBackColor = true;
            // 
            // buRunOne
            // 
            this.buRunOne.Location = new System.Drawing.Point(473, 145);
            this.buRunOne.Name = "buRunOne";
            this.buRunOne.Size = new System.Drawing.Size(83, 29);
            this.buRunOne.TabIndex = 6;
            this.buRunOne.Text = "Выполнить 1";
            this.buRunOne.UseVisualStyleBackColor = true;
            // 
            // buDel
            // 
            this.buDel.Location = new System.Drawing.Point(562, 144);
            this.buDel.Name = "buDel";
            this.buDel.Size = new System.Drawing.Size(85, 29);
            this.buDel.TabIndex = 7;
            this.buDel.Text = "delete";
            this.buDel.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(283, 180);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(364, 229);
            this.dataGridView1.TabIndex = 8;
            // 
            // edNotes
            // 
            this.edNotes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edNotes.Location = new System.Drawing.Point(283, 424);
            this.edNotes.Name = "edNotes";
            this.edNotes.Size = new System.Drawing.Size(173, 26);
            this.edNotes.TabIndex = 9;
            // 
            // edNotesPr
            // 
            this.edNotesPr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.edNotesPr.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edNotesPr.Location = new System.Drawing.Point(462, 424);
            this.edNotesPr.Name = "edNotesPr";
            this.edNotesPr.Size = new System.Drawing.Size(93, 26);
            this.edNotesPr.TabIndex = 10;
            // 
            // buNotesAdd
            // 
            this.buNotesAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buNotesAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buNotesAdd.Location = new System.Drawing.Point(561, 424);
            this.buNotesAdd.Name = "buNotesAdd";
            this.buNotesAdd.Size = new System.Drawing.Size(86, 26);
            this.buNotesAdd.TabIndex = 11;
            this.buNotesAdd.Text = "add";
            this.buNotesAdd.UseVisualStyleBackColor = true;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 457);
            this.Controls.Add(this.buNotesAdd);
            this.Controls.Add(this.edNotesPr);
            this.Controls.Add(this.edNotes);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.buDel);
            this.Controls.Add(this.buRunOne);
            this.Controls.Add(this.buNotesSH);
            this.Controls.Add(this.buUsersSh);
            this.Controls.Add(this.edSQL);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lvLogs);
            this.Name = "fm";
            this.Text = "labSQLite";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edNotesPr)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvLogs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox edSQL;
        private System.Windows.Forms.Button buUsersSh;
        private System.Windows.Forms.Button buNotesSH;
        private System.Windows.Forms.Button buRunOne;
        private System.Windows.Forms.Button buDel;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox edNotes;
        private System.Windows.Forms.NumericUpDown edNotesPr;
        private System.Windows.Forms.Button buNotesAdd;
    }
}

