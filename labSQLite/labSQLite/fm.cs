﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labSQLite
{
    public partial class fm : Form
    {
        private SQLiteConnection db;

        public fm()
        {
            InitializeComponent();

            buDel.Enabled = false;

            db = new SQLiteConnection("myDB.db");
            db.CreateTable<Logs>();
            db.CreateTable<Users>();
            db.CreateTable<Notes>();

            db.Insert(new Logs() { DT = DateTime.Now });
            lvLogs.Columns.Add("DateTime", 220);
            lvLogs.View = View.Details;
            foreach (var item in db.Table<Logs>())
            {
                lvLogs.Items.Add(item.DT.ToString());
            }

            buUsersSh.Click += (s, e) => dataGridView1.DataSource = db.Table<Users>().ToList();
            buNotesSH.Click += (s, e) => dataGridView1.DataSource = db.Table<Notes>().ToList();

            buDel.Click += BuDel_Click;


            buNotesAdd.Click += BuNotesAdd_Click;

            buRunOne.Click += (s, e) => MessageBox.Show(db.ExecuteScalar<int>(edSQL.Text).ToString());

            dataGridView1.SelectionChanged += DataGridView1_SelectionChanged;

            dataGridView1.DataError += DataGridView1_DataError;

            /* 
             Добавление редактирование удаление
             фильтры
             обновление после добавления             
             */
        }

        private void DataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            dataGridView1.DataSource = db.Table<Notes>().ToList();
        }

        private void BuDel_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int k = 0;
                foreach (var item in db.Table<Notes>())
                {
                    //lvLogs.Items.Add(item.DT.ToString());
                    if (k == dataGridView1.SelectedRows[0].Index)
                    {
                        db.Delete<Notes>(item.ID.ToString());
                    }
                    k++;
                }

                Text = dataGridView1.SelectedRows[0].Index.ToString();
                dataGridView1.DataSource = db.Table<Notes>().ToList();

            }
        }

        private void DataGridView1_SelectionChanged(object sender, EventArgs e)
        {

            if (dataGridView1.SelectedRows.Count > 0)
            {
                buDel.Enabled = true;
            }
            else
            {
                buDel.Enabled = false;
            }
        }



        private void BuNotesAdd_Click(object sender, EventArgs e)
        {
            //(1)
            var x = new Notes();
            x.Caption = edNotes.Text;
            x.Priority = (byte)edNotesPr.Value;
            db.Insert(x);
            dataGridView1.DataSource = db.Table<Notes>().ToList();
            //(2)
            //db.Insert(new Notes() { Caption = edNotes.Text, Priority = (byte)edNotesPr.Value });

        }

        private class Logs
        {
            public DateTime DT { get; set; }
        }

        private class Users
        {
            [PrimaryKey, AutoIncrement]
            public int ID { get; set; }
            public string FIO { get; set; }
            public string Email { get; set; }
            public byte Age { get; set; }
        }

        private class Notes
        {
            [PrimaryKey, AutoIncrement]
            public int ID { get; set; }
            public string Caption { get; set; }
            public byte Priority { get; set; }

        }
    }
}
