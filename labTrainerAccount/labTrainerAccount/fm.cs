﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labTrainerAccount
{
    public partial class fm : Form
    {

        private Games g;
        public fm()
        {
            InitializeComponent();

            // стили
            this.BackColor = Color.FromArgb(38, 0, 56);
            this.buNo.BackColor = Color.FromArgb(229, 57, 57);
            this.buYes.BackColor = Color.FromArgb(87, 194, 48);
            this.laWrong.BackColor = Color.FromArgb(229, 57, 57);
            this.laCorrect.BackColor = Color.FromArgb(87, 194, 48);
            this.menuDown.BackColor = Color.FromArgb(247, 206, 0);
            

            g = new Games();
            string mode = menuDown.Text.ToString();
            g.Change += Event_Change;

            g.DoReset(mode);
            buYes.Click += (sender, e) => g.DoAnswer(true);
            buNo.Click += (sender, e) => g.DoAnswer(false);


        }

        private void Event_Change(object sender, EventArgs e)
        {
            laCorrect.Text = String.Format("Верно = {0}", g.CountCorrect.ToString());
            laWrong.Text = String.Format("Неверно = {0}", g.CountWrong.ToString());
            laCode.Text = g.CodeText;
            laMode.Text = String.Format("Режим: {0}", g.mode.ToString());

        }

        private void menuDown_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            this.UpdateStatus(e.ClickedItem);
        }

        private void UpdateStatus(ToolStripItem item)
        {
            if (item != null && item.Text != g.mode)
            {
                string mode = item.Text.ToString();
                this.menu.Items[0].Text = mode;
                g.DoReset(mode);

            }
        }
    }
}
