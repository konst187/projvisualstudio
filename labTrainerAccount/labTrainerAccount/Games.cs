﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labTrainerAccount
{
    class Games
    {
        public int CountCorrect { get; protected set; } // счет верных ответов
        public int CountWrong { get; protected set; } // счет ложных ответов
        public bool AnswerCorrect { get; protected set; }
        public string CodeText { get; protected set; } // текст выражения в окне
        public string mode { get; protected set; } // Переменная, в которой хранится режим игры

        public event EventHandler Change;

        public void DoReset(string choice)
        {
            mode = choice;
            CountCorrect = 0;
            CountWrong = 0;
            DoContinue(mode);
        }

        // функции с действиями
        private void sum()
        {
            Random rnd = new Random();
            int xValue1 = 0;
            int xValue2 = 0;
            int xResult = 0;
            int xResultNew; // новый результат, если он будет изменен
            int xSign; // увеличится или уменьшится неверный ответ

            xValue1 = rnd.Next(1, 50);
            xValue2 = rnd.Next(1, 50);
            xResult = xValue1 + xValue2;

            if (rnd.Next(2) == 1)
            {
                xResultNew = xResult;
            }
            else
            {

                if (rnd.Next(2) == 1)
                {
                    xSign = 1;
                }
                else
                {
                    xSign = -1;
                }

                xResultNew = xResult + (rnd.Next(7) * xSign);

            }

            AnswerCorrect = (xResult == xResultNew);
            CodeText = string.Format("{0} + {1} = {2}", xValue1, xValue2, xResultNew);

            if (Change != null)
                Change(this, EventArgs.Empty);
        }

        private void minus()
        {
            Random rnd = new Random();
            int xValue1 = 0;
            int xValue2 = 0;
            int xResult = 0;
            int xResultNew; // новый результат, если он будет изменен
            int xSign; // увеличится или уменьшится неверный ответ

            xValue1 = rnd.Next(2, 50);
            xValue2 = rnd.Next(1, xValue1);
            xResult = xValue1 - xValue2;

            if (rnd.Next(2) == 1)
            {
                xResultNew = xResult;
            }
            else
            {

                if (rnd.Next(2) == 1)
                {
                    xSign = 1;
                }
                else
                {
                    xSign = -1;
                }

                xResultNew = xResult + (rnd.Next(7) * xSign);

            }

            AnswerCorrect = (xResult == xResultNew);
            CodeText = string.Format("{0} - {1} = {2}", xValue1, xValue2, xResultNew);

            if (Change != null)
                Change(this, EventArgs.Empty);
        }

        private void mult()
        {
            Random rnd = new Random();
            int xValue1 = 0;
            int xValue2 = 0;
            int xResult = 0;
            int xResultNew; // новый результат, если он будет изменен
            int xSign; // увеличится или уменьшится неверный ответ

            xValue1 = rnd.Next(2, 10);
            xValue2 = rnd.Next(2, 10);
            xResult = xValue1 * xValue2;

            if (rnd.Next(2) == 1)
            {
                xResultNew = xResult;
            }
            else
            {

                if (rnd.Next(2) == 1)
                {
                    xSign = 1;
                }
                else
                {
                    xSign = -1;
                }

                xResultNew = xValue1 * (xValue2 + xSign);

            }

            AnswerCorrect = (xResult == xResultNew);
            CodeText = string.Format("{0} * {1} = {2}", xValue1, xValue2, xResultNew);

            if (Change != null)
                Change(this, EventArgs.Empty);
        }

        private void divide()
        {
            Random rnd = new Random();
            int xValue1 = 0;
            int xValue2 = 0;
            int xResult = 0;
            int xResultNew; // новый результат, если он будет изменен
            int xSign; // увеличится или уменьшится неверный ответ

            xValue1 = rnd.Next(2, 10);
            xValue2 = rnd.Next(2, 10);
            xResult = xValue1 * xValue2;
            xValue1 = xResult;
            xResult = xValue1 / xValue2;

            if (rnd.Next(2) == 1)
            {
                xResultNew = xResult;
            }
            else
            {

                if (rnd.Next(2) == 1)
                {
                    xSign = 1;
                }
                else
                {
                    xSign = -1;
                }

                xResultNew = (xValue1 + xSign) / xValue2;

            }

            AnswerCorrect = (xResult == xResultNew);
            CodeText = string.Format("{0} / {1} = {2}", xValue1, xValue2, xResultNew);

            if (Change != null)
                Change(this, EventArgs.Empty);
        }

        private void combo()
        {
            Random rnd = new Random();
            int choice = rnd.Next(4);
            if (choice == 0)
            {
                sum();
            }
            else if (choice == 1)
            {
                minus();
            }
            else if (choice == 2)
            {
                divide();
            }
            else if (choice == 3)
            {
                mult();
            }

        }

        // Выбор функции для следующей итерации
        public void DoContinue(string choice)
        {

            switch (choice)
            {
                case "Сложение":

                    sum();

                    break;

                case "Вычитание":

                    minus();

                    break;

                case "Деление":

                    divide();

                    break;

                case "Умножение":

                    mult();

                    break;

                case "Случайные":

                    combo();

                    break;

            }

        }

        public void DoAnswer(bool answer)
        {
            if (answer == AnswerCorrect)
                CountCorrect++;
            else
                CountWrong++;
            DoContinue(mode);

        }


    }
}
