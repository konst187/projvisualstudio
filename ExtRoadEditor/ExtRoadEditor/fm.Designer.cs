﻿namespace ExtRoadEditor
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fm));
            this.RoadMap = new System.Windows.Forms.PictureBox();
            this.miniMap = new System.Windows.Forms.PictureBox();
            this.CurrPicture = new System.Windows.Forms.PictureBox();
            this.lv = new System.Windows.Forms.ListView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.laText = new System.Windows.Forms.Label();
            this.buFillArea = new System.Windows.Forms.Button();
            this.buFill = new System.Windows.Forms.Button();
            this.BuPlay = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.buClear = new System.Windows.Forms.ToolStripMenuItem();
            this.buLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.buSave = new System.Windows.Forms.ToolStripMenuItem();
            this.BuSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.buSize = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.RoadMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.miniMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrPicture)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // RoadMap
            // 
            this.RoadMap.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RoadMap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RoadMap.Location = new System.Drawing.Point(298, 27);
            this.RoadMap.Name = "RoadMap";
            this.RoadMap.Size = new System.Drawing.Size(1076, 661);
            this.RoadMap.TabIndex = 0;
            this.RoadMap.TabStop = false;
            // 
            // miniMap
            // 
            this.miniMap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.miniMap.Location = new System.Drawing.Point(7, 27);
            this.miniMap.Name = "miniMap";
            this.miniMap.Size = new System.Drawing.Size(285, 218);
            this.miniMap.TabIndex = 1;
            this.miniMap.TabStop = false;
            // 
            // CurrPicture
            // 
            this.CurrPicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CurrPicture.Location = new System.Drawing.Point(171, 275);
            this.CurrPicture.Name = "CurrPicture";
            this.CurrPicture.Size = new System.Drawing.Size(121, 108);
            this.CurrPicture.TabIndex = 2;
            this.CurrPicture.TabStop = false;
            // 
            // lv
            // 
            this.lv.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lv.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lv.HideSelection = false;
            this.lv.LargeImageList = this.imageList1;
            this.lv.Location = new System.Drawing.Point(7, 429);
            this.lv.Name = "lv";
            this.lv.Size = new System.Drawing.Size(285, 259);
            this.lv.TabIndex = 3;
            this.lv.UseCompatibleStateImageBehavior = false;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "sand.png");
            this.imageList1.Images.SetKeyName(1, "water.png");
            this.imageList1.Images.SetKeyName(2, "coin.png");
            this.imageList1.Images.SetKeyName(3, "wall.png");
            this.imageList1.Images.SetKeyName(4, "11.png");
            this.imageList1.Images.SetKeyName(5, "12.png");
            this.imageList1.Images.SetKeyName(6, "13.png");
            this.imageList1.Images.SetKeyName(7, "14.png");
            this.imageList1.Images.SetKeyName(8, "15.png");
            this.imageList1.Images.SetKeyName(9, "16.png");
            this.imageList1.Images.SetKeyName(10, "17.png");
            this.imageList1.Images.SetKeyName(11, "18.png");
            this.imageList1.Images.SetKeyName(12, "21.png");
            this.imageList1.Images.SetKeyName(13, "22.png");
            this.imageList1.Images.SetKeyName(14, "23.png");
            this.imageList1.Images.SetKeyName(15, "24.png");
            this.imageList1.Images.SetKeyName(16, "25.png");
            this.imageList1.Images.SetKeyName(17, "26.png");
            this.imageList1.Images.SetKeyName(18, "27.png");
            this.imageList1.Images.SetKeyName(19, "28.png");
            this.imageList1.Images.SetKeyName(20, "grass.png");
            // 
            // laText
            // 
            this.laText.AutoSize = true;
            this.laText.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laText.Location = new System.Drawing.Point(3, 248);
            this.laText.Name = "laText";
            this.laText.Size = new System.Drawing.Size(136, 24);
            this.laText.TabIndex = 4;
            this.laText.Text = "Номер ячейки";
            // 
            // buFillArea
            // 
            this.buFillArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buFillArea.Location = new System.Drawing.Point(7, 275);
            this.buFillArea.Name = "buFillArea";
            this.buFillArea.Size = new System.Drawing.Size(158, 32);
            this.buFillArea.TabIndex = 5;
            this.buFillArea.Text = "Заливка";
            this.buFillArea.UseVisualStyleBackColor = true;
            // 
            // buFill
            // 
            this.buFill.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buFill.Location = new System.Drawing.Point(7, 313);
            this.buFill.Name = "buFill";
            this.buFill.Size = new System.Drawing.Size(158, 32);
            this.buFill.TabIndex = 6;
            this.buFill.Text = "Залить";
            this.buFill.UseVisualStyleBackColor = true;
            // 
            // BuPlay
            // 
            this.BuPlay.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BuPlay.Location = new System.Drawing.Point(7, 351);
            this.BuPlay.Name = "BuPlay";
            this.BuPlay.Size = new System.Drawing.Size(158, 32);
            this.BuPlay.TabIndex = 7;
            this.BuPlay.Text = "Игра ВЫКЛ";
            this.BuPlay.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(3, 402);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(209, 24);
            this.label2.TabIndex = 8;
            this.label2.Text = "Доступные элементы:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buClear,
            this.buLoad,
            this.buSave,
            this.BuSettings});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1377, 24);
            this.menuStrip1.TabIndex = 9;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // buClear
            // 
            this.buClear.Name = "buClear";
            this.buClear.Size = new System.Drawing.Size(71, 20);
            this.buClear.Text = "Очистить";
            // 
            // buLoad
            // 
            this.buLoad.Name = "buLoad";
            this.buLoad.Size = new System.Drawing.Size(73, 20);
            this.buLoad.Text = "Загрузить";
            // 
            // buSave
            // 
            this.buSave.Name = "buSave";
            this.buSave.Size = new System.Drawing.Size(78, 20);
            this.buSave.Text = "Сохранить";
            // 
            // BuSettings
            // 
            this.BuSettings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buSize});
            this.BuSettings.Name = "BuSettings";
            this.BuSettings.Size = new System.Drawing.Size(83, 20);
            this.BuSettings.Text = "Параметры";
            // 
            // buSize
            // 
            this.buSize.Name = "buSize";
            this.buSize.Size = new System.Drawing.Size(150, 22);
            this.buSize.Text = "Размер карты";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1377, 691);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.BuPlay);
            this.Controls.Add(this.buFill);
            this.Controls.Add(this.buFillArea);
            this.Controls.Add(this.laText);
            this.Controls.Add(this.lv);
            this.Controls.Add(this.CurrPicture);
            this.Controls.Add(this.miniMap);
            this.Controls.Add(this.RoadMap);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(800, 730);
            this.Name = "fm";
            this.ShowIcon = false;
            this.Text = "ExtRoadEditor";
            ((System.ComponentModel.ISupportInitialize)(this.RoadMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.miniMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrPicture)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox RoadMap;
        private System.Windows.Forms.PictureBox miniMap;
        private System.Windows.Forms.PictureBox CurrPicture;
        private System.Windows.Forms.ListView lv;
        private System.Windows.Forms.Label laText;
        private System.Windows.Forms.Button buFillArea;
        private System.Windows.Forms.Button buFill;
        private System.Windows.Forms.Button BuPlay;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem buClear;
        private System.Windows.Forms.ToolStripMenuItem buLoad;
        private System.Windows.Forms.ToolStripMenuItem buSave;
        private System.Windows.Forms.ToolStripMenuItem BuSettings;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem buSize;
    }
}

