﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExtRoadEditor
{
    public partial class fm : Form
    {
        // Form2
        Label rowCount;
        Label colCount;
        Form form2;
        Button buOk;
        NumericUpDown text1;
        NumericUpDown text2;

        // Объекты классов
        Logics lobj;
        FileWork Fobj;
        public fm()
        {
            InitializeComponent();

            lobj = new Logics(RoadMap.Width, RoadMap.Height, (Bitmap)imageList1.Images[0], new Size(CurrPicture.Width, CurrPicture.Height));
            Fobj = new FileWork();

            SetStyles();
            loadLV();
            lobj.ResizeCells(imageList1, miniMap.Width, miniMap.Height);
            DrawCells();
            refreshMini();
            lobj.DrawCells(imageList1);

            RoadMap.MouseWheel += RoadMap_MouseWheel;
            buClear.Click += BuClear_Click;
            buLoad.Click += BuLoad_Click;
            buSave.Click += BuSave_Click;
            RoadMap.MouseDown += RoadMap_MouseDown;
            RoadMap.MouseMove += RoadMap_MouseMove;
            RoadMap.MouseUp += RoadMap_MouseUp;
            RoadMap.Paint += RoadMap_Paint;
            CurrPicture.Paint += CurrPicture_Paint;
            buSize.Click += BuSize_Click;
            miniMap.Paint += MiniMap_Paint;
            buOk.Click += BuOk_Click;
            buFill.Click += BuFill_Click;
            lv.ItemSelectionChanged += Lv_ItemSelectionChanged;
            BuPlay.Click += BuPlay_Click;
            RoadMap.KeyDown += RoadMap_KeyDown;
            buFillArea.Click += BuFillArea_Click;
            RoadMap.LostFocus += RoadMap_LostFocus;
        }


        private void DrawCells()
        {
            lobj.DrawCells(imageList1);
            RoadMap.Refresh();
        }

        private void loadLV()
        {
            lv.BeginUpdate();
            lv.Items.Clear();
            for (int i = 0; i < imageList1.Images.Count; i++)
            {
                lv.Items.Add(new ListViewItem(new string[] { i.ToString() }, i));
            }
            lv.EndUpdate();
        }


        private void BuOk_Click(object sender, EventArgs e)
        {
            lobj.CELLS_Y = (int)text1.Value;
            lobj.CELLS_X = (int)text2.Value;
            lobj.InitVars(RoadMap.Width, RoadMap.Height, (Bitmap)imageList1.Images[0], new Size(CurrPicture.Width, CurrPicture.Height));
            lobj.ClearMap();
            RoadMap.Refresh();
            refreshMini();
            form2.Close();
        }

        private void RoadMap_LostFocus(object sender, EventArgs e)
        {
            RoadMap.Focus();
        }

        private void BuFillArea_Click(object sender, EventArgs e)
        {
            lobj.paintArea = !lobj.paintArea;
            if (lobj.paintArea)
            {
                buFillArea.Text = "Заливка ВКЛ";
            }
            else
            {
                buFillArea.Text = "Заливка ВЫКЛ";
            }
        }

        private void drawGame()
        {
            lobj.drawGame();
            RoadMap.Refresh();
        }

        private void RoadMap_KeyDown(object sender, KeyEventArgs e) //
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();

            if (lobj.playMode)
            {
                switch (e.KeyCode)
                {
                    case Keys.W:
                        lobj.CheckW();
                        break;
                    case Keys.A:
                        lobj.CheckA();
                        break;
                    case Keys.S:
                        lobj.CheckS();
                        break;
                    case Keys.D:
                        lobj.CheckD();
                        break;
                }

                drawGame();

                if (lobj.gameObj())
                {
                    DrawCells();
                }

                refreshMini();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        private void refreshMini()
        {
            lobj.refreshMini(RoadMap.Width, RoadMap.Height);
            miniMap.Refresh();
        }

        private void denableButtons()
        {
            buFill.Enabled = !buFill.Enabled;
            buFillArea.Enabled = !buFillArea.Enabled;
            buClear.Enabled = !buClear.Enabled;
            BuSettings.Enabled = !BuSettings.Enabled;
            buLoad.Enabled = !buLoad.Enabled;
            buSave.Enabled = !buSave.Enabled;
        }

        private void BuPlay_Click(object sender, EventArgs e)
        {

            if (lobj.playMode)
            {
                BuPlay.Text = "Игра ВЫКЛ";
            }
            else
            {
                BuPlay.Text = "Игра ВКЛ";
            }
            RoadMap.Focus();
            lobj.playEnable();
            denableButtons();
            DrawCells();
            refreshMini();
        }

        private void Lv_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            for (int i = 0; i < imageList1.Images.Count; i++)
            {
                if (i == e.ItemIndex)
                {
                    lobj.activeImage = i;
                    lobj.BitCurrntRoadPicture = new Bitmap(CurrPicture.Width, CurrPicture.Height);
                    var g = Graphics.FromImage(lobj.BitCurrntRoadPicture);
                    g.DrawImage(imageList1.Images[i], new Rectangle(0, 0, CurrPicture.Width, CurrPicture.Height),
                        new Rectangle(0, 0, imageList1.Images[i].Width, imageList1.Images[i].Height), GraphicsUnit.Pixel);
                    g.Dispose();
                    CurrPicture.Refresh();
                }
            }
        }

        private void BuFill_Click(object sender, EventArgs e)
        {
            lobj.fillRect();
            DrawCells();
            refreshMini();
        }

        private void MiniMap_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(lobj.miniBitMap, new Point(0, 0));
        }

        private void BuSize_Click(object sender, EventArgs e)
        {
            form2.ShowDialog();
        }

        private void CurrPicture_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(lobj.BitCurrntRoadPicture, new Point(0, 0));
        }

        private void RoadMap_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImageUnscaled(lobj.bitRoadMap, lobj.CurPointRM);
        }

        private void RoadMap_MouseUp(object sender, MouseEventArgs e)
        {
            if (lobj.mouseUpped())
            {
                DrawCells();
            }
            refreshMini();
        }

        private void RoadMap_MouseMove(object sender, MouseEventArgs e)
        {

            if (lobj.refreshLabel(e.X, e.Y) != "")
            {
                laText.Text = lobj.refreshLabel(e.X, e.Y);
            }
            lobj.mouseRigth(e, imageList1);
            RoadMap.Refresh();
        }

        private void RoadMap_MouseDown(object sender, MouseEventArgs e)
        {
            lobj.mouseDown(e, imageList1);
        }

        private void BuSave_Click(object sender, EventArgs e) // save
        {
            var path = System.IO.Path.GetFullPath(@"config");
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            if (!dirInfo.Exists)
            {
                Directory.CreateDirectory(path);
            }

            if (!File.Exists(@"config\config.txt"))
            {
                StreamWriter sw = File.CreateText(@"config\config.txt");
                sw.Close();
            }

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";

            openFileDialog.InitialDirectory = path;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                Fobj.SaveCells(openFileDialog.FileName, lobj.CELLS_X, lobj.CELLS_Y, lobj.MapCells);
            }
        }

        private void BuLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";

            var path = System.IO.Path.GetFullPath(@"config");
            openFileDialog.InitialDirectory = path;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                (lobj.CELLS_X, lobj.CELLS_Y) = Fobj.LoadData(openFileDialog.FileName);
                text1.Value = lobj.CELLS_X;
                text2.Value = lobj.CELLS_Y;

                lobj.InitVars(RoadMap.Width, RoadMap.Height, (Bitmap)imageList1.Images[0], new Size(CurrPicture.Width, CurrPicture.Height));
                DrawCells();

                lobj.LoadCells(Fobj.LoadArr(openFileDialog.FileName), RoadMap.Width, RoadMap.Height);
                RoadMap.Invalidate();
                refreshMini();
            }
        }

        private void BuClear_Click(object sender, EventArgs e)
        {
            lobj.ClearMap();
            RoadMap.Refresh();
            refreshMini();
        }

        private void RoadMap_MouseWheel(object sender, MouseEventArgs e)
        {
            lobj.mouseWheel(e.Delta);
            DrawCells();
            refreshMini();
        }

        private void SetStyles() // Стили
        {
            RoadMap.BorderStyle = BorderStyle.FixedSingle;
            CurrPicture.BorderStyle = BorderStyle.FixedSingle;

            this.BackColor = Color.FromArgb(37, 6, 79);
            CurrPicture.BackColor = Color.FromArgb(22, 22, 28);
            RoadMap.BackColor = Color.FromArgb(22, 22, 28);
            menuStrip1.BackColor = Color.FromArgb(22, 22, 28);
            menuStrip1.ForeColor = Color.FromArgb(252, 252, 252);
            buSize.BackColor = Color.FromArgb(22, 22, 28);
            buSize.ForeColor = Color.FromArgb(252, 252, 252);

            form2 = new Form();
            form2.BackColor = Color.FromArgb(22, 22, 28);
            form2.ForeColor = Color.FromArgb(252, 252, 252);
            form2.ShowIcon = false;
            form2.Text = "Изменение размера";
            form2.Size = new Size(300, 300);
            form2.MinimumSize = new Size(300, 250);
            form2.MaximumSize = new Size(300, 250);
            form2.StartPosition = FormStartPosition.CenterScreen;

            buOk = new Button();
            buOk.Text = "Подвердить";
            buOk.Dock = DockStyle.Bottom;
            buOk.Font = new Font("Tahoma", 14, FontStyle.Bold);
            buOk.BackColor = Color.FromArgb(37, 6, 79);
            buOk.ForeColor = Color.FromArgb(252, 252, 252);
            buOk.Height = 40;
            form2.Controls.Add(buOk);

            text1 = new NumericUpDown();
            text2 = new NumericUpDown();

            text1.BackColor = Color.FromArgb(22, 22, 28);
            text2.BackColor = Color.FromArgb(22, 22, 28);
            text1.ForeColor = ForeColor = Color.FromArgb(252, 252, 252);
            text2.ForeColor = ForeColor = Color.FromArgb(252, 252, 252);
            text1.Value = lobj.CELLS_X;
            text2.Value = lobj.CELLS_Y;
            text1.Maximum = 50;
            text1.Minimum = 1;
            text2.Maximum = 50;
            text2.Minimum = 1;

            text1.Width = form2.Width - 20;
            text2.Width = form2.Width - 20;
            text1.Font = new Font("Tahoma", 14, FontStyle.Bold);
            text1.ReadOnly = true;
            text2.ReadOnly = true;
            text2.Font = new Font("Tahoma", 14, FontStyle.Bold);
            text1.Height = 50;
            text2.Height = 50;
            text1.Location = new Point(0, 40);
            text2.Location = new Point(0, 120);

            rowCount = new Label();
            colCount = new Label();
            rowCount.Width = form2.Width;
            colCount.Width = form2.Width;
            rowCount.Font = new Font("Tahoma", 14, FontStyle.Bold);
            colCount.Font = new Font("Tahoma", 14, FontStyle.Bold);
            rowCount.Height = 40;
            colCount.Height = 40;

            rowCount.Text = $"Строки:";
            colCount.Text = $"Столбцы:";
            rowCount.Location = new Point(0, 0);
            colCount.Location = new Point(0, 80);
            form2.Controls.Add(rowCount);
            form2.Controls.Add(colCount);
            form2.Controls.Add(text1);
            form2.Controls.Add(text2);

            buFill.BackColor = Color.FromArgb(22, 22, 28);
            buFill.ForeColor = Color.FromArgb(252, 252, 252);

            lv.Font = new Font("Tahoma", 14, FontStyle.Bold);
            lv.ForeColor = Color.FromArgb(252, 252, 252);
            lv.BackColor = Color.FromArgb(22, 22, 28);


            BuPlay.BackColor = Color.FromArgb(22, 22, 28);
            BuPlay.ForeColor = Color.FromArgb(252, 252, 252);

            buFillArea.BackColor = Color.FromArgb(22, 22, 28);
            buFillArea.ForeColor = Color.FromArgb(252, 252, 252);
            buFillArea.Text = "Заливка ВЫКЛ";

            lv.Columns.Add("Имя", 30);
            lv.View = View.Tile;
        }
    }
}
