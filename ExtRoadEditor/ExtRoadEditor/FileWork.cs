﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtRoadEditor
{
    class FileWork
    {

        public FileWork()
        {


        }

        public void SaveCells(string path, int X, int Y, string[,] MapCells)
        {
            using (StreamWriter streamWriter = new StreamWriter(path))
            {
                streamWriter.WriteLine(X);
                streamWriter.WriteLine(Y);
                for (int i = 0; i < X; i++)
                {
                    for (int j = 0; j < Y; j++)
                    {
                        streamWriter.Write(MapCells[i, j]);
                    }
                }
            }
        }

        public (int, int) LoadData(string path)
        {
            using (StreamReader streamReader = new StreamReader(path))
            {
                string x = streamReader.ReadLine();
                string y = streamReader.ReadLine();
                int IX = Convert.ToInt32(x);
                int IY = Convert.ToInt32(y);
                return (IX, IY);
            }
        }
        public string[,] LoadArr(string path)
        {
            using (StreamReader streamReader = new StreamReader(path))
            {
                string x = streamReader.ReadLine();
                string y = streamReader.ReadLine();
                int IX = Convert.ToInt32(x);
                int IY = Convert.ToInt32(y);

                string[,] arr = new string[IX, IY];

                for (int i = 0; i < IX; i++)
                {
                    for (int j = 0; j < IY; j++)
                    {
                        arr[i, j] = Convert.ToChar(streamReader.Read()).ToString();
                    }
                }

                return arr;

            }

        }

    }
}
