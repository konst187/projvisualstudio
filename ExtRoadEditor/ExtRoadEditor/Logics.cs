﻿using ExtRoadEditor.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExtRoadEditor
{
    class Logics
    {
        public Bitmap[] BitPics;
        public Bitmap bitRoadMap;
        public Bitmap BitCurrntRoadPicture;
        public Bitmap miniBitMap;
        public Bitmap tempBitField;
        public Bitmap bitPers = new Bitmap(Resources.chelR);

        public int activeImage;

        public int CurRowRM { get; private set; }
        public int CurColRM { get; private set; }

        public int oldRow { get; private set; }
        public int oldCol { get; private set; }

        public int HeightCellImage = 80;
        public int WidthCellImage = 80;

        public Dictionary<string, Image> dictMap =
   new Dictionary<string, Image>();

        public string[,] MapCells;
        public string[,] MapGame;

        public int DelataZoom = 10;
        public int DelataSpeed = 10;

        public bool paintArea = false;

        public bool RectPressed = false;
        public bool playMode = false;
        public int xGame;
        public int yGame;
        public int CurRowGame;
        public int CurColGame;

        public int MAP_HEIGHT = 0;
        public int MAP_WIDTH = 0;

        public int CELLS_X = 20;
        public int CELLS_Y = 20;

        public Point StartPointRM;
        public Point CurPointRM;

        public Logics(int MapWidth, int MapHeight, Bitmap CurrPic, Size size)
        {
            InitVars(MapWidth, MapHeight, CurrPic, size);
        }

        public void InitVars(int MapWidth, int MapHeight, Bitmap CurrPic, Size size)
        {
            MAP_WIDTH = CELLS_X * WidthCellImage;
            MAP_HEIGHT = CELLS_Y * HeightCellImage;
            MapCells = new string[CELLS_X, CELLS_Y];
            MapGame = new string[CELLS_X, CELLS_Y];
            bitRoadMap = new Bitmap(MAP_WIDTH, MAP_HEIGHT);

            // Заполнение всего массива пустыми клетками
            for (int i = 0; i < CELLS_X; i++)
            {
                for (int j = 0; j < CELLS_Y; j++)
                {
                    MapCells[i, j] = "z";
                }
            }
            // Перемещение в центр карты при запуске приложения
            CurPointRM = new Point((MapWidth - bitRoadMap.Width) / 2, (MapHeight - bitRoadMap.Height) / 2);
            BitCurrntRoadPicture = new Bitmap(CurrPic, new Size(size.Width, size.Height));
        }

        public void ResizeCells(ImageList list, int width, int height) // Добавляет картинки в массив
        {
            char a = 'a';
            miniBitMap = new Bitmap(width, height);
            BitPics = new Bitmap[list.Images.Count];
            for (int i = 0; i < list.Images.Count; i++)
            {
                BitPics[i] = (Bitmap)list.Images[i];
                dictMap.Add(a.ToString(), BitPics[i]);
                a++;
            }
        }

        public void DrawCells(ImageList list)
        {
            var g = Graphics.FromImage(bitRoadMap);

            g.Clear(Color.FromArgb(22, 22, 28));

            for (int i = 0; i < CELLS_X; i++)
            {
                for (int j = 0; j < CELLS_Y; j++)
                {
                    foreach (KeyValuePair<string, Image> kvp in dictMap)
                    {
                        g.DrawRectangle(new Pen(Color.Gray, 1), i * HeightCellImage, j * WidthCellImage, WidthCellImage, HeightCellImage);
                        if (kvp.Key == MapCells[i, j])
                        {
                            g.DrawImage(kvp.Value, new Rectangle(i * HeightCellImage, j * WidthCellImage, WidthCellImage, HeightCellImage),
                            new Rectangle(0, 0, kvp.Value.Width, kvp.Value.Height), GraphicsUnit.Pixel);
                            break;
                        }
                    }
                    if (MapGame[i, j] != null)
                    {
                        g.DrawImage(list.Images[2], new Rectangle(i * HeightCellImage, j * WidthCellImage, WidthCellImage, HeightCellImage),
                             new Rectangle(0, 0, list.Images[2].Width, list.Images[2].Height), GraphicsUnit.Pixel);
                    }
                }
            }

            if (playMode)
            {
                tempBitField = new Bitmap(bitRoadMap);
                g.DrawImage(bitPers, new Rectangle(xGame, yGame, HeightCellImage, HeightCellImage), new Rectangle(0, 0, bitPers.Width, bitPers.Height), GraphicsUnit.Pixel);
            }

            g.DrawRectangle(new Pen(Color.Gray, 3), 0, 0, MAP_WIDTH, MAP_HEIGHT);
            g.Dispose();

        }

        public void refreshMini(int width, int height)
        {
            var gr = Graphics.FromImage(miniBitMap);
            gr.DrawImage(bitRoadMap, 0, 0, miniBitMap.Width, miniBitMap.Height);

            float w = (float)width / (float)WidthCellImage;
            float h = (float)height / (float)HeightCellImage;
            float wcell = (float)miniBitMap.Width / (float)CELLS_X;
            float hcell = (float)miniBitMap.Height / (float)CELLS_Y;

            float xl = (float)bitRoadMap.Width / (float)miniBitMap.Width;
            float yl = (float)bitRoadMap.Height / (float)miniBitMap.Height;

            gr.DrawRectangle(new Pen(Color.Red, 1), -(CurPointRM.X / xl), -(CurPointRM.Y / yl), w * wcell, h * hcell);
            gr.Dispose();

        }

        public void drawGame()
        {
            CurRowGame = ((yGame + WidthCellImage / 2) / WidthCellImage);
            CurColGame = ((xGame + WidthCellImage / 2) / HeightCellImage);
            bitRoadMap = new Bitmap(tempBitField);
            var g = Graphics.FromImage(bitRoadMap);
            g.DrawImage(bitPers, new Rectangle(xGame, yGame, HeightCellImage, HeightCellImage), new Rectangle(0, 0, bitPers.Width, bitPers.Height), GraphicsUnit.Pixel);
            g.DrawRectangle(new Pen(Color.Gray, 3), 0, 0, MAP_WIDTH, MAP_HEIGHT);
        }

        public void CheckW()
        {
            CurRowGame = (yGame / WidthCellImage);
            CurColGame = (xGame + HeightCellImage / 2) / HeightCellImage;
            bitPers = new Bitmap(Resources.chelU);
            if (checkBlock() && yGame > 0)
            {
                yGame -= DelataSpeed;
            }
            else
            {
                yGame += DelataSpeed;
            }
        }
        public void CheckA()
        {
            CurRowGame = (yGame + HeightCellImage / 2) / WidthCellImage;
            CurColGame = (xGame) / HeightCellImage;
            bitPers = new Bitmap(Resources.chelL);
            if (checkBlock() && xGame > 0)
            {
                xGame -= DelataSpeed;
            }
            else
            {
                xGame += DelataSpeed;
            }
        }
        public void CheckS()
        {
            CurRowGame = (yGame + HeightCellImage) / WidthCellImage;
            CurColGame = (xGame + HeightCellImage / 2) / HeightCellImage;
            bitPers = new Bitmap(Resources.chelD);
            if (CurRowGame < CELLS_Y)
                if (checkBlock() && yGame < bitRoadMap.Height)
                {
                    yGame += DelataSpeed;
                }
                else
                {
                    yGame -= DelataSpeed;
                }
        }
        public void CheckD()
        {
            CurRowGame = (yGame + HeightCellImage / 2) / WidthCellImage;
            CurColGame = (xGame + HeightCellImage) / HeightCellImage;
            bitPers = new Bitmap(Resources.chelR);
            if (CurColGame < CELLS_X)
                if (checkBlock() && xGame < bitRoadMap.Width)
                {
                    xGame += DelataSpeed;
                }
                else
                {
                    xGame -= DelataSpeed;
                }
        }

        public bool gameObj()
        {
            if (MapGame[CurColGame, CurRowGame] != null)
            {
                MapGame[CurColGame, CurRowGame] = null;
                return true;
            }
            return false;
        }

        public void playEnable()
        {
            playMode = !playMode;
            tempBitField = new Bitmap(bitRoadMap);
            xGame = 0;
            yGame = 0;
        }

        public bool mouseUpped()
        {
            if (RectPressed == true)
            {
                CheckRect();
                RectPressed = false;
                bitRoadMap = new Bitmap(tempBitField);

                for (int i = oldCol; i <= CurColRM; i++)
                {
                    for (int j = oldRow; j <= CurRowRM; j++)
                    {
                        foreach (KeyValuePair<string, Image> kvp in dictMap)
                        {
                            if (kvp.Value == BitPics[activeImage])
                            {
                                if (kvp.Key != "c")
                                {
                                    MapCells[i, j] = kvp.Key;
                                    MapGame[i, j] = null;
                                }
                            }
                        }
                    }
                }
                return true;
            }
            else
            {
                return false;
            }

        }

        public void fillRect()
        {
            var g = Graphics.FromImage(bitRoadMap);
            g.Clear(Color.FromArgb(22, 22, 28));

            for (int i = 0; i < CELLS_X; i++)
            {
                for (int j = 0; j < CELLS_Y; j++)
                {
                    foreach (KeyValuePair<string, Image> kvp in dictMap)
                    {
                        if (kvp.Value == BitPics[activeImage])
                        {
                            if (kvp.Key != "c")
                            {
                                MapGame[i, j] = null;
                                MapCells[i, j] = kvp.Key;
                                g.DrawImage(kvp.Value, new Rectangle(i * HeightCellImage, j * WidthCellImage, WidthCellImage, HeightCellImage),
                           new Rectangle(0, 0, kvp.Value.Width, kvp.Value.Height), GraphicsUnit.Pixel);
                                break;
                            }
                        }
                    }
                }
            }

            g.DrawRectangle(new Pen(Color.Gray, 3), 0, 0, MAP_WIDTH, MAP_HEIGHT);
            g.Dispose();

        }


        private void CheckRect()
        {
            if (oldCol < 0)
            {
                oldCol = 0;
            }
            if (oldRow < 0)
            {
                oldRow = 0;
            }
            if (CurColRM >= CELLS_X)
            {
                CurColRM = CELLS_X - 1;
            }
            if (CurRowRM >= CELLS_Y)
            {
                CurRowRM = CELLS_Y - 1;
            }
        }

        private bool checkBlock()
        {
            if (MapCells[CurColGame, CurRowGame] == "b" || MapCells[CurColGame, CurRowGame] == "d")
                return false;
            else
                return true;
        }
        

        public void ClearMap()
        {
            var g = Graphics.FromImage(bitRoadMap);

            g.Clear(Color.FromArgb(22, 22, 28));

            for (int i = 0; i < CELLS_X; i++)
            {
                for (int j = 0; j < CELLS_Y; j++)
                {
                    MapGame[i, j] = null;
                    MapCells[i, j] = "z";
                    g.DrawRectangle(new Pen(Color.Gray, 1), i * HeightCellImage, j * WidthCellImage, WidthCellImage, HeightCellImage);
                }
            }

            g.DrawRectangle(new Pen(Color.Gray, 3), 0, 0, MAP_WIDTH, MAP_HEIGHT);
            g.Dispose();

        }

        private bool CheckField() 
        {
            if (CurColRM < 0 || CurRowRM < 0 || CurColRM >= CELLS_X || CurRowRM >= CELLS_Y)
                return false;

            return true;
        }

        public void mouseWheel(int delta)
        {
            int x = CurRowRM * DelataZoom;
            int y = CurColRM * DelataZoom;

            if (delta > 0)
            {
                if (WidthCellImage < 80)
                {
                    WidthCellImage += DelataZoom;
                    HeightCellImage += DelataZoom;
                    CurPointRM = new Point(CurPointRM.X - y, CurPointRM.Y - x);
                }
            }
            else
            {
                if (WidthCellImage > 30)
                {
                    WidthCellImage -= DelataZoom;
                    HeightCellImage -= DelataZoom;
                    CurPointRM = new Point(CurPointRM.X + y, CurPointRM.Y + x);
                }
            }


            xGame = CurColGame * HeightCellImage;
            yGame = CurRowGame * HeightCellImage;


            MAP_WIDTH = CELLS_X * WidthCellImage;
            MAP_HEIGHT = CELLS_Y * WidthCellImage;
            bitRoadMap = new Bitmap(MAP_WIDTH, MAP_HEIGHT);
        }

        public string refreshLabel(int X, int Y)
        {
            CurRowRM = (Y - CurPointRM.Y) / WidthCellImage;
            CurColRM = (X - CurPointRM.X) / HeightCellImage;
            if (CheckField())
            {
                return $"Номер ячейки: {CurColRM}, {CurRowRM}";
            }

            return "";
        }
        public void mouseRigth(MouseEventArgs e, ImageList list)
        {
            CurRowRM = (e.Y - CurPointRM.Y) / WidthCellImage;
            CurColRM = (e.X - CurPointRM.X) / HeightCellImage;
            if (e.Button == MouseButtons.Right)
            {
                CurPointRM.X += e.X - StartPointRM.X;
                CurPointRM.Y += e.Y - StartPointRM.Y;
                StartPointRM = new Point(e.X, e.Y);
            }
            if (e.Button == MouseButtons.Left && RectPressed == true)
            {
                bitRoadMap = new Bitmap(tempBitField);
                Graphics g = Graphics.FromImage(bitRoadMap);
                g.TranslateTransform(-CurPointRM.X, -CurPointRM.Y);
                g.DrawRectangle(new Pen(Color.BlueViolet, 1), StartPointRM.X, StartPointRM.Y, e.Location.X - StartPointRM.X, e.Location.Y - StartPointRM.Y);
            }
            else if ((e.Button == MouseButtons.Left) && (CheckField()) && playMode == false)
            {
                foreach (KeyValuePair<string, Image> kvp in dictMap)
                {
                    if (kvp.Value == BitPics[activeImage])
                    {
                        if (kvp.Key == "c")
                        {
                            MapGame[CurColRM, CurRowRM] = kvp.Key;
                        }
                        else
                        {
                            if (MapGame[CurColRM, CurRowRM] != null)
                            {
                                MapGame[CurColRM, CurRowRM] = null;
                            }
                            MapCells[CurColRM, CurRowRM] = kvp.Key;
                        }
                    }
                }

                var g = Graphics.FromImage(bitRoadMap); // Орисовка выбранного объекта в текущей ячейке на карте
                g.DrawImage(list.Images[activeImage], new Rectangle(CurColRM * WidthCellImage, CurRowRM * HeightCellImage, WidthCellImage, HeightCellImage),
                new Rectangle(0, 0, list.Images[activeImage].Width, list.Images[activeImage].Height), GraphicsUnit.Pixel);
                g.Dispose();

            }
        }


        public void LoadCells(string[,] Arr, int width, int height)
        {          

            for (int i = 0; i < CELLS_X; i++)
            {
                for (int j = 0; j < CELLS_Y; j++)
                {
                    MapCells[i, j] = Arr[i, j];
                }
            }

            var g = Graphics.FromImage(bitRoadMap);
            g.Clear(Color.FromArgb(22, 22, 28));
            for (int i = 0; i < CELLS_X; i++)
            {
                for (int j = 0; j < CELLS_Y; j++)
                {
                    if (MapCells[i, j] == "z")
                        g.DrawRectangle(new Pen(Color.Gray, 1), i * HeightCellImage, j * WidthCellImage, WidthCellImage, HeightCellImage);
                    else
                    {
                        foreach (KeyValuePair<string, Image> kvp in dictMap)
                        {
                            if (kvp.Key == MapCells[i, j])
                            {
                                g.DrawImage(kvp.Value, new Rectangle(i * HeightCellImage, j * WidthCellImage, WidthCellImage, HeightCellImage),
                                new Rectangle(0, 0, kvp.Value.Width, kvp.Value.Height), GraphicsUnit.Pixel);
                                break;
                            }
                        }
                    }
                }
            }

            g.DrawRectangle(new Pen(Color.Gray, 3), 0, 0, MAP_WIDTH, MAP_HEIGHT);
            g.Dispose();


            CurPointRM = new Point((width - bitRoadMap.Width) / 2, (height - bitRoadMap.Height) / 2); 

        }

        public void mouseDown(MouseEventArgs e, ImageList list)
        {
            StartPointRM = new Point(e.X, e.Y);

            if (e.Button == MouseButtons.Left && playMode == true && CheckField())
            {
                xGame = e.X - CurPointRM.X - WidthCellImage / 2;
                yGame = e.Y - CurPointRM.Y - WidthCellImage / 2;
                drawGame();
            }
            else if (e.Button == MouseButtons.Left && paintArea == true)
            {
                tempBitField = new Bitmap(bitRoadMap);
                RectPressed = true;
                oldRow = CurRowRM;
                oldCol = CurColRM;
            }
            else if ((e.Button == MouseButtons.Left) && (CheckField()))
            {
                foreach (KeyValuePair<string, Image> kvp in dictMap)
                {
                    if (kvp.Value == BitPics[activeImage])
                    {
                        if (kvp.Key == "c")
                        {
                            MapGame[CurColRM, CurRowRM] = kvp.Key;
                        }
                        else
                        {
                            if (MapGame[CurColRM, CurRowRM] != null)
                            {
                                MapGame[CurColRM, CurRowRM] = null;
                            }
                            MapCells[CurColRM, CurRowRM] = kvp.Key;
                        }
                    }
                }
                var g = Graphics.FromImage(bitRoadMap); // Орисовка выбранного объекта в текущей ячейке на карте
                g.DrawImage(list.Images[activeImage], new Rectangle(CurColRM * WidthCellImage, CurRowRM * HeightCellImage, WidthCellImage, HeightCellImage),
                new Rectangle(0, 0, list.Images[activeImage].Width, list.Images[activeImage].Height), GraphicsUnit.Pixel);
                g.Dispose();

            }
        }



    }
}
