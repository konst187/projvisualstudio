﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labGraphicsDrawLine
{
    public partial class fm : Form
    {
        private Image b;
        //private Graphics g;
        private Point startPoint;
        private Bitmap tempB;
        private Pen myPen;
        private Pen Erraser;
        private Color color;

        private bool isPressed = false;
        private Point CurPoint = new Point(0, 0);
        private bool rectState = false;
        private bool lineState = false;
        private bool circleState = false;
        private bool brushState = false;
        private bool erraseState;

        public fm()
        {
            InitializeComponent();

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            Graphics g = Graphics.FromImage(b);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            myPen = new Pen(Color.Black, 5);
            Erraser = new Pen(Color.White, 20);
            myPen.StartCap = System.Drawing.Drawing2D.LineCap.Round;
            Erraser.StartCap = System.Drawing.Drawing2D.LineCap.Round;
            _Color.BackColor = myPen.Color;
            brushState = true;


            pxImage.MouseDown += (s, e) => startPoint = e.Location;
            pxImage.MouseMove += PxImage_MouseMove;
            pxImage.Paint += (s, e) => e.Graphics.DrawImage(b, CurPoint);


            trPenWidth.Value = Convert.ToInt32(myPen.Width);
            trPenWidth.ValueChanged += (s, e) => myPen.Width = trPenWidth.Value;

            buClear.Click += BuClear_Click;
            buColor.Click += BuColor_Click;
            pxImage.MouseDown += Fm_MouseDown;
            pxImage.MouseUp += Fm_MouseUp;


            buRect.Click += BuRect_Click;
            buLine.Click += BuLine_Click;
            buCircle.Click += BuCircle_Click;
            buBrush.Click += BuBrush_Click;
            buErrase.Click += BuErrase_Click;
            buSave.Click += BuSave_Click;
            buLoad.Click += BuLoad_Click;

        }

        private void BuLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
            if (dialog.ShowDialog() == DialogResult.OK)
                b = Bitmap.FromFile(dialog.FileName);
            pxImage.Invalidate();
        }

        private void BuSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "Image Files(*.JPG;)|*.JPG;";
            if (dialog.ShowDialog() == DialogResult.OK)
                b.Save(dialog.FileName);
        }

        private void BuErrase_Click(object sender, EventArgs e)
        {
            StateOff();
            erraseState = true;
            buErrase.FlatStyle = FlatStyle.Standard;
        }

        private void BuBrush_Click(object sender, EventArgs e)
        {
            StateOff();
            brushState = true;
            buBrush.FlatStyle = FlatStyle.Standard;
        }

        private void BuCircle_Click(object sender, EventArgs e)
        {
            StateOff();
            circleState = true;
            buCircle.FlatStyle = FlatStyle.Standard;
        }

        private void BuLine_Click(object sender, EventArgs e)
        {
            StateOff();
            lineState = true;
            buLine.FlatStyle = FlatStyle.Standard;
        }

        private void BuRect_Click(object sender, EventArgs e)
        {
            StateOff();
            rectState = true;
            buRect.FlatStyle = FlatStyle.Standard;
        }

        private void BuClear_Click(object sender, EventArgs e)
        {
            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            Graphics g = Graphics.FromImage(b);
            g.Clear(Color.White);
            CurPoint = new Point(0, 0);
            pxImage.Invalidate();
        }

        private void StateOff()
        {
            buBrush.FlatStyle = FlatStyle.Flat;
            buCircle.FlatStyle = FlatStyle.Flat;
            buLine.FlatStyle = FlatStyle.Flat;
            buRect.FlatStyle = FlatStyle.Flat;
            buErrase.FlatStyle = FlatStyle.Flat;
            brushState = false;
            rectState = false;
            lineState = false;
            circleState = false;
            erraseState = false;
        }
        private void Fm_MouseUp(object sender, MouseEventArgs e)
        {
            isPressed = false;
        }

        private void Fm_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isPressed = true;
                startPoint = new Point(e.X, e.Y);
                tempB = new Bitmap(b);
            }

        }

        private void BuColor_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            color = colorDialog1.Color;
            myPen.Color = color;
            _Color.BackColor = color;
        }

        private void PxImage_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                CurPoint.X += e.X - startPoint.X;
                CurPoint.Y += e.Y - startPoint.Y;
                startPoint = e.Location;
                pxImage.Invalidate();
            }

            if (isPressed == true && rectState == true)
            {
                b = new Bitmap(tempB);
                Graphics g = Graphics.FromImage(b);
                g.TranslateTransform(-CurPoint.X, -CurPoint.Y);
                g.DrawRectangle(myPen, startPoint.X, startPoint.Y, e.Location.X - startPoint.X, e.Location.Y - startPoint.Y);
                pxImage.Refresh();

            }
            else if (isPressed == true && lineState == true)
            {
                b = new Bitmap(tempB);
                Graphics g = Graphics.FromImage(b);
                g.TranslateTransform(-CurPoint.X, -CurPoint.Y);
                g.DrawLine(myPen, startPoint.X, startPoint.Y, e.Location.X, e.Location.Y);
                pxImage.Refresh();
            }
            else if (isPressed == true && circleState == true)
            {
                b = new Bitmap(tempB);
                Graphics g = Graphics.FromImage(b);
                g.TranslateTransform(-CurPoint.X, -CurPoint.Y);
                g.DrawEllipse(myPen, startPoint.X, startPoint.Y, e.Location.X - startPoint.X, e.Location.Y - startPoint.Y);
                pxImage.Refresh();

            }
            else if (isPressed == true && brushState == true)
            {
                Graphics g = Graphics.FromImage(b);
                g.TranslateTransform(-CurPoint.X, -CurPoint.Y);
                g.DrawLine(myPen, startPoint, e.Location);
                startPoint = e.Location;
                pxImage.Invalidate();
            }
            else if (isPressed == true && erraseState == true)
            {
                Graphics g = Graphics.FromImage(b);
                g.TranslateTransform(-CurPoint.X, -CurPoint.Y);
                g.DrawLine(Erraser, startPoint, e.Location);
                startPoint = e.Location;
                pxImage.Invalidate();

            }
        }
    }
}
