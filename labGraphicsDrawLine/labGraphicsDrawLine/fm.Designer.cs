﻿namespace labGraphicsDrawLine
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this._Color = new System.Windows.Forms.Button();
            this.buErrase = new System.Windows.Forms.Button();
            this.buLoad = new System.Windows.Forms.Button();
            this.buSave = new System.Windows.Forms.Button();
            this.buBrush = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.buCircle = new System.Windows.Forms.Button();
            this.buLine = new System.Windows.Forms.Button();
            this.buRect = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buColor = new System.Windows.Forms.Button();
            this.buClear = new System.Windows.Forms.Button();
            this.trPenWidth = new System.Windows.Forms.TrackBar();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.pxImage = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trPenWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this._Color);
            this.panel1.Controls.Add(this.buErrase);
            this.panel1.Controls.Add(this.buLoad);
            this.panel1.Controls.Add(this.buSave);
            this.panel1.Controls.Add(this.buBrush);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.buCircle);
            this.panel1.Controls.Add(this.buLine);
            this.panel1.Controls.Add(this.buRect);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.buColor);
            this.panel1.Controls.Add(this.buClear);
            this.panel1.Controls.Add(this.trPenWidth);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 662);
            this.panel1.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(2, 302);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 20);
            this.label5.TabIndex = 19;
            this.label5.Text = "Действия";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(3, 225);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 20);
            this.label4.TabIndex = 18;
            this.label4.Text = "Активный цвет";
            // 
            // _Color
            // 
            this._Color.Enabled = false;
            this._Color.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._Color.Location = new System.Drawing.Point(7, 248);
            this._Color.Name = "_Color";
            this._Color.Size = new System.Drawing.Size(44, 41);
            this._Color.TabIndex = 17;
            this._Color.UseVisualStyleBackColor = true;
            // 
            // buErrase
            // 
            this.buErrase.BackColor = System.Drawing.Color.White;
            this.buErrase.BackgroundImage = global::labGraphicsDrawLine.Properties.Resources._20250;
            this.buErrase.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buErrase.FlatAppearance.BorderSize = 0;
            this.buErrase.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buErrase.Location = new System.Drawing.Point(152, 176);
            this.buErrase.Name = "buErrase";
            this.buErrase.Size = new System.Drawing.Size(30, 30);
            this.buErrase.TabIndex = 16;
            this.buErrase.UseVisualStyleBackColor = false;
            // 
            // buLoad
            // 
            this.buLoad.BackgroundImage = global::labGraphicsDrawLine.Properties.Resources.open;
            this.buLoad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buLoad.FlatAppearance.BorderSize = 0;
            this.buLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buLoad.Location = new System.Drawing.Point(65, 325);
            this.buLoad.Name = "buLoad";
            this.buLoad.Size = new System.Drawing.Size(50, 50);
            this.buLoad.TabIndex = 15;
            this.buLoad.UseVisualStyleBackColor = true;
            // 
            // buSave
            // 
            this.buSave.BackgroundImage = global::labGraphicsDrawLine.Properties.Resources._25398;
            this.buSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buSave.FlatAppearance.BorderSize = 0;
            this.buSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buSave.Location = new System.Drawing.Point(7, 325);
            this.buSave.Name = "buSave";
            this.buSave.Size = new System.Drawing.Size(50, 50);
            this.buSave.TabIndex = 14;
            this.buSave.UseVisualStyleBackColor = true;
            // 
            // buBrush
            // 
            this.buBrush.BackColor = System.Drawing.Color.White;
            this.buBrush.BackgroundImage = global::labGraphicsDrawLine.Properties.Resources._15654;
            this.buBrush.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buBrush.FlatAppearance.BorderSize = 0;
            this.buBrush.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buBrush.Location = new System.Drawing.Point(115, 176);
            this.buBrush.Name = "buBrush";
            this.buBrush.Size = new System.Drawing.Size(30, 30);
            this.buBrush.TabIndex = 13;
            this.buBrush.UseVisualStyleBackColor = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(2, 153);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 20);
            this.label3.TabIndex = 12;
            this.label3.Text = "Инструменты";
            // 
            // buCircle
            // 
            this.buCircle.BackColor = System.Drawing.Color.White;
            this.buCircle.BackgroundImage = global::labGraphicsDrawLine.Properties.Resources.circle;
            this.buCircle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buCircle.FlatAppearance.BorderSize = 0;
            this.buCircle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buCircle.Location = new System.Drawing.Point(79, 176);
            this.buCircle.Name = "buCircle";
            this.buCircle.Size = new System.Drawing.Size(30, 30);
            this.buCircle.TabIndex = 11;
            this.buCircle.UseVisualStyleBackColor = false;
            // 
            // buLine
            // 
            this.buLine.BackColor = System.Drawing.Color.White;
            this.buLine.BackgroundImage = global::labGraphicsDrawLine.Properties.Resources.line;
            this.buLine.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buLine.FlatAppearance.BorderSize = 0;
            this.buLine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buLine.Location = new System.Drawing.Point(43, 176);
            this.buLine.Name = "buLine";
            this.buLine.Size = new System.Drawing.Size(30, 30);
            this.buLine.TabIndex = 10;
            this.buLine.UseVisualStyleBackColor = false;
            // 
            // buRect
            // 
            this.buRect.BackColor = System.Drawing.Color.White;
            this.buRect.BackgroundImage = global::labGraphicsDrawLine.Properties.Resources.rekt;
            this.buRect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buRect.FlatAppearance.BorderSize = 0;
            this.buRect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buRect.Location = new System.Drawing.Point(7, 176);
            this.buRect.Name = "buRect";
            this.buRect.Size = new System.Drawing.Size(30, 30);
            this.buRect.TabIndex = 9;
            this.buRect.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(3, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "Размер кисти";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "Палитра цветов";
            // 
            // buColor
            // 
            this.buColor.BackgroundImage = global::labGraphicsDrawLine.Properties.Resources.gradient;
            this.buColor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buColor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buColor.Location = new System.Drawing.Point(3, 32);
            this.buColor.Name = "buColor";
            this.buColor.Size = new System.Drawing.Size(194, 37);
            this.buColor.TabIndex = 6;
            this.buColor.UseVisualStyleBackColor = true;
            // 
            // buClear
            // 
            this.buClear.BackColor = System.Drawing.Color.White;
            this.buClear.BackgroundImage = global::labGraphicsDrawLine.Properties.Resources.w448h5121380477116trash;
            this.buClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buClear.FlatAppearance.BorderSize = 0;
            this.buClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buClear.Location = new System.Drawing.Point(121, 325);
            this.buClear.Name = "buClear";
            this.buClear.Size = new System.Drawing.Size(50, 50);
            this.buClear.TabIndex = 5;
            this.buClear.UseVisualStyleBackColor = false;
            // 
            // trPenWidth
            // 
            this.trPenWidth.LargeChange = 1;
            this.trPenWidth.Location = new System.Drawing.Point(3, 106);
            this.trPenWidth.Minimum = 1;
            this.trPenWidth.Name = "trPenWidth";
            this.trPenWidth.Size = new System.Drawing.Size(191, 45);
            this.trPenWidth.TabIndex = 4;
            this.trPenWidth.Value = 1;
            // 
            // pxImage
            // 
            this.pxImage.BackColor = System.Drawing.Color.White;
            this.pxImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pxImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pxImage.Location = new System.Drawing.Point(200, 0);
            this.pxImage.Name = "pxImage";
            this.pxImage.Size = new System.Drawing.Size(1059, 662);
            this.pxImage.TabIndex = 1;
            this.pxImage.TabStop = false;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1259, 662);
            this.Controls.Add(this.pxImage);
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(450, 450);
            this.Name = "fm";
            this.ShowIcon = false;
            this.Text = "labGraphicsDrawLine";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trPenWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buClear;
        private System.Windows.Forms.TrackBar trPenWidth;
        private System.Windows.Forms.PictureBox pxImage;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Button buColor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buCircle;
        private System.Windows.Forms.Button buLine;
        private System.Windows.Forms.Button buRect;
        private System.Windows.Forms.Button buBrush;
        private System.Windows.Forms.Button buErrase;
        private System.Windows.Forms.Button buLoad;
        private System.Windows.Forms.Button buSave;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button _Color;
        private System.Windows.Forms.Label label5;
    }
}

