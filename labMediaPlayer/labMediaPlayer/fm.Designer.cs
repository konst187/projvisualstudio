﻿namespace labMediaPlayer
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buPlay = new System.Windows.Forms.Button();
            this.buPause = new System.Windows.Forms.Button();
            this.butStop = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.trVol = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.trVol)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.buPlay.Location = new System.Drawing.Point(176, 52);
            this.buPlay.Name = "button1";
            this.buPlay.Size = new System.Drawing.Size(75, 23);
            this.buPlay.TabIndex = 0;
            this.buPlay.Text = "button1";
            this.buPlay.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.buPause.Location = new System.Drawing.Point(318, 52);
            this.buPause.Name = "button2";
            this.buPause.Size = new System.Drawing.Size(75, 23);
            this.buPause.TabIndex = 1;
            this.buPause.Text = "button2";
            this.buPause.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.butStop.Location = new System.Drawing.Point(453, 52);
            this.butStop.Name = "button3";
            this.butStop.Size = new System.Drawing.Size(75, 23);
            this.butStop.TabIndex = 2;
            this.butStop.Text = "button3";
            this.butStop.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 111);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "label1";
            // 
            // trackBar1
            // 
            this.trVol.Location = new System.Drawing.Point(63, 100);
            this.trVol.Name = "trackBar1";
            this.trVol.Size = new System.Drawing.Size(631, 45);
            this.trVol.TabIndex = 4;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 486);
            this.Controls.Add(this.trVol);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.butStop);
            this.Controls.Add(this.buPause);
            this.Controls.Add(this.buPlay);
            this.Name = "fm";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.trVol)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buPlay;
        private System.Windows.Forms.Button buPause;
        private System.Windows.Forms.Button butStop;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar trVol;
    }
}

