﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;

namespace labMediaPlayer
{
    public partial class fm : Form
    {
        private MediaPlayer mediaPlayer = new MediaPlayer();
        public fm()
        {
            InitializeComponent();

            mediaPlayer.Open(new Uri(@"C:\Users\konst\Music\Музыка\all\Lana Del Rey - Summertime Sadness.mp3"));

            buPlay.Click += (s, e) => mediaPlayer.Play();
            buPlay.Text = "Play";
            buPause.Click += (s, e) => mediaPlayer.Pause();
            buPause.Text = "buPause";
            butStop.Click += (s, e) => mediaPlayer.Stop();
            butStop.Text = "butStop";

            trVol.ValueChanged += TrVol_ValueChanged;
            trVol.Maximum = 100;
            //trVol.Minimum = 0;
            trVol.Value = (int)Math.Round(mediaPlayer.Volume * 100);
        }

        private void TrVol_ValueChanged(object sender, EventArgs e)
        {
            mediaPlayer.Volume = trVol.Value / 100.0;
        }
    }
}
