﻿using gameTetris.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gameTetris
{
    public partial class fm : Form
    {
        private Bitmap b;
        private Bitmap c;

        gameMod gm;

        public delegate void MyDelegate();
        Thread myThread;

        private int size = 20;
        private int Xcells = 10;
        private int Ycells = 18;
        DateTime date1;
        Font fontPixel;
        Font fontPixelBig;
        Font fontPixelMed;

        private int interval = 300;
        private int score = 0;
        PrivateFontCollection fontCollection;
        private bool gameStat = false;
        private bool gameOver = false;

        Label laName;
        Label laScore2;
        TextBox instr;
        Label laTime2;
        Label difLvl;

        Form form2;
        public fm()
        {
            InitializeComponent();

            initTheme();

            b = new Bitmap(picGame.Width, picGame.Height);
            c = new Bitmap(picNext.Width, picNext.Height);
            gm = new gameMod(Xcells, Ycells);
            this.StartPosition = FormStartPosition.CenterScreen;
            picGame.Paint += PicGame_Paint;
            picNext.Paint += PicNext_Paint;
            drawCells();
            newShape();

            laDiff.Text = $"Time interval: {interval}";
            timer1.Interval = interval;
            timer1.Tick += Timer1_Tick;
            timer1.Start();

            this.KeyDown += Fm_KeyDown;
            this.KeyUp += Fm_KeyUp;

            updateNext();

        }

        ~fm()
        {
            this.Close();
            form2.Close();
        }


        private void pauseScreen() // Вызов меню паузы
        {
            initForm2();
            this.Hide();
            form2.Show();
        }

        private void initForm2() // Инициализация стилей для второго окна
        {
            form2 = new Form();
            form2.StartPosition = FormStartPosition.CenterScreen;
            form2.BackColor = Color.FromArgb(22, 22, 28);
            form2.ForeColor = Color.FromArgb(252, 252, 252);
            form2.ShowIcon = false;
            form2.Text = "Menu";
            form2.Size = this.Size;
            form2.MinimumSize = this.MinimumSize = this.Size;
            form2.MaximumSize = this.MaximumSize = this.Size;

            laName = new Label();
            laName.Font = fontPixelBig;
            laName.Width = this.Width / 2;
            laName.Height = 100;
            laName.Text = "TETRIS";
            laName.Location = new Point(this.Width / 3, 0);
            form2.Controls.Add(laName);

            laScore2 = new Label();
            laScore2.Font = fontPixelBig;
            laScore2.Width = this.Width / 2;
            laScore2.Height = 50;
            laScore2.Text = $"Score: {score}";
            laScore2.Location = new Point(0, 100);
            form2.Controls.Add(laScore2);

            laTime2 = new Label();
            laTime2.Font = fontPixelBig;
            laTime2.Width = this.Width / 2;
            laTime2.Height = 50;
            laTime2.Text = $"Time: {date1.ToString("mm:ss")}";
            laTime2.Location = new Point(0, 150);
            form2.Controls.Add(laTime2);

            difLvl = new Label();
            difLvl.Font = fontPixelBig;
            difLvl.Width = this.Width;
            difLvl.Height = 50;
            difLvl.Text = $"Time interval: {interval} ms";
            difLvl.Location = new Point(0, 200);
            form2.Controls.Add(difLvl);

            instr = new TextBox();
            instr.Location = new Point(0, 350);
            instr.Multiline = true;
            instr.Width = this.Width - 20;
            instr.Height = this.Height / 3 + 80;
            instr.Font = fontPixelMed;
            instr.BackColor = Color.FromArgb(22, 22, 28);
            instr.ForeColor = Color.FromArgb(252, 252, 252);
            instr.ReadOnly = true;
            instr.TextAlign = HorizontalAlignment.Center;
            instr.Enabled = false;
            string[] str = new string[] { "Добро пожаловать в игру \"TETRIS\" ",
                "Для того, чтобы продолжить, нажмите Enter",
                "Управление производится стрелочками",
                "Поворот фигуры при помощи Space",
                "Вызов этого меню - Escape",
                "Уровни сложности клавиши 1-5",
            "Новая игра - F1"};
            instr.Lines = str;
            form2.Controls.Add(instr);

            form2.KeyDown += Form2_KeyDown; ;
            form2.FormClosed += Form2_FormClosed;

            form2.LocationChanged += (s, e) => this.Location = form2.Location;
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (gameOver == false)
            {
                this.Show();
                gameStat = false;
                timer1.Start();
            }
            else
            {
                Application.Exit();
            }
        }

        private void Form2_KeyDown(object sender, KeyEventArgs e)  
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (gameOver == true)
                    {
                        newGame();
                        form2.Close();
                        this.Show();
                        gameStat = false;
                    }
                    else
                    {
                        form2.Close();
                        this.Show();
                        gameStat = false;
                        timer1.Start();
                    }
                    break;
                case Keys.F1:
                    newGame();
                    form2.Close();
                    this.Show();
                    gameStat = false;
                    break;
                case Keys.D5:
                    interval = 300;
                    difLvl.Text = $"Time interval: {interval} ms";
                    laDiff.Text = $"Time interval: {interval}";
                    timer1.Interval = interval;
                    break;
                case Keys.D4:
                    interval = 400;
                    difLvl.Text = $"Time interval: {interval} ms";
                    laDiff.Text = $"Time interval: {interval}";
                    timer1.Interval = interval;
                    break;
                case Keys.D3:
                    interval = 500;
                    difLvl.Text = $"Time interval: {interval} ms";
                    laDiff.Text = $"Time interval: {interval}";
                    timer1.Interval = interval;
                    break;
                case Keys.D2:
                    interval = 600;
                    difLvl.Text = $"Time interval: {interval} ms";
                    laDiff.Text = $"Time interval: {interval}";
                    timer1.Interval = interval;
                    break;
                case Keys.D1:
                    interval = 800;
                    difLvl.Text = $"Time interval: {interval} ms";
                    laDiff.Text = $"Time interval: {interval}";
                    timer1.Interval = interval;
                    break;

            }
        }

        private void initTheme() // Инициализация стилей для перовго окна
        {
            size = picGame.Width / 10;
            picGame.Height = size * Ycells;
            picGame.Width = size * Xcells;

            // Я бы прикрепил свой шрифт, не будь это настолько проблематично его сюда встраивать
            // Ведь из ресурсов его невозможно использовать, а сам файл должен лежать в папке дебага,
            // Которая не заливается на гит!!!

            //fontCollection = new PrivateFontCollection();
            //fontCollection.AddFontFile("ARCADECLASSIC.TTF"); // файл шрифта
            //FontFamily family = fontCollection.Families[0];

            fontPixel = new Font(FontFamily.GenericSansSerif, 18);
            fontPixelBig = new Font(FontFamily.GenericSansSerif, 36);
            fontPixelMed = new Font(FontFamily.GenericSansSerif, 24);
            label2.Font = fontPixel;
            label3.Font = fontPixel;
            laScore.Font = fontPixel;
            laTime.Font = fontPixel;

            label2.Text = "Next figure:";
            label3.Text = "Time:";

            label1.Font = fontPixel;

            picNext.Width = size * 4;
            picNext.Height = size * 2;

            this.MaximumSize = this.Size;
            this.MinimumSize = this.Size;
        }

        private void updateTimer() // функция обновления таймера в отдельном потоке
        {
            while (true)
            {
                if (!gameStat)
                {
                    date1 = date1.AddSeconds(1);
                    laTime.Invoke(new Action(() => { laTime.Text = date1.ToString("mm:ss"); }));
                    Thread.Sleep(1000);
                }

            }
        }


        private void PicNext_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(c, new Point(0, 0));
        }

        private void Fm_KeyUp(object sender, KeyEventArgs e)
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            if (gameStat == false)
                switch (e.KeyCode)
                {
                    case Keys.Space:
                        gm.RotateShape();
                        makeMove();
                        break;
                    case Keys.Down:
                        timer1.Interval = 10;
                        if (!gm.moveDown())
                        {
                            makeMove();
                        }
                        else
                        {
                            makeMove();
                            if (gm.Next == false)
                            {
                                newShape();
                            }
                        }
                        break;
                    
                }

        }

        private void makeMove() // Обновить поле после одного хода
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            gm.combine();
            updateField();
            while (gm.checkLines() != 0)
            {
                gm.DeleteLine(gm.checkLines());
                score += 100;
                laScore.Text = $"Score: {score}";

            }

            picGame.Refresh();

        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();

            switch (e.KeyCode)
            {
                case Keys.Left:
                    if (gameStat == false)
                    {
                        gm.moveLeft();
                        makeMove();
                    }
                    break;
                case Keys.Right:
                    if (gameStat == false)
                    {
                        gm.moveRight();
                        makeMove();
                    }
                    break;
                case Keys.Escape:
                    if (gameStat == false)
                    {
                        gameStat = true;
                        timer1.Stop();
                        pauseScreen();
                    }
                    else
                    {
                        gameStat = false;
                        timer1.Start();
                    }
                    break;
            }

        }

        private void newGame() // Новая игра
        {
            gameOver = false;
            score = 0;
            laScore.Text = $"Score: {score}";

            date1 = new DateTime();
            laTime.Text = date1.ToString("mm:ss");

            gm = new gameMod(Xcells, Ycells);
            drawCells();
            newShape();
            timer1.Interval = interval;
            timer1.Start();

        }

        private void newShape() // Создание новой фигуры
        {
            gm.initFigure(3, 0);
            timer1.Interval = interval;

            if (gm.CheckLoose() == true)
            {
                timer1.Stop();
                timer1.Dispose();
                pauseScreen();
                gameOver = true;
            }
            else
            {
                gm.combine();
                updateField();
                picGame.Invalidate();
            }

            updateNext();

        }

        private void Timer1_Tick(object sender, EventArgs e) 
        {
            if (myThread == null)
            {
                myThread = new Thread(new ThreadStart(updateTimer));
                myThread.IsBackground = true;
                myThread.Start(); // запускаем поток
            }

            if (!gm.moveDown())
            {
                makeMove();
            }
            else
            {
                makeMove();
                if (gm.Next == false)
                {
                    newShape();
                }
            }

        }

        private void drawCells() // функция отрисовки ячеек
        {
            var g = Graphics.FromImage(b);
            g.Clear(Color.White);
            for (int i = 0; i < Ycells; i++)
            {
                for (int j = 0; j < Xcells; j++)
                {
                    g.DrawImage(Resources.empty_layer, new RectangleF(j * size, i * size, size, size));
                }
            }
            g.Dispose();
            picGame.Invalidate();
        }

        private void PicGame_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(b, new Point(0, 0));
        }


        private void updateNext() // функция обновления поля след фигуры
        {
            var g = Graphics.FromImage(c);
            g.Clear(Color.White);

            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    g.DrawImage(Resources.empty_layer, new RectangleF(i * size, j * size, size, size));
                }
            }

            for (int i = 0; i < gm.FigXnext; i++)
            {
                for (int j = 0; j < gm.FigYnext; j++)
                {
                    if (gm.nextFigure[i, j] == 1)
                    {
                        g.DrawImage(Resources.full, new RectangleF(i * size, j * size, size, size));
                    }

                }
            }

            g.Dispose();
            picNext.Refresh();

        }

        private void updateField() // функция обновления поля
        {
            var g = Graphics.FromImage(b);
            drawCells();

            for (int i = 0; i < gm.Rows; i++)
            {
                for (int j = 0; j < gm.Cols; j++)
                {
                    if (gm[i, j] == 1)
                    {
                        g.DrawImage(Resources.full, new RectangleF(j * size, i * size, size, size));
                    }
                }
            }

            g.Dispose();
        }
    }
}
