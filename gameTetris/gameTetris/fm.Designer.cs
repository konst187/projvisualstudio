﻿namespace gameTetris
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.picGame = new System.Windows.Forms.PictureBox();
            this.picNext = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.laTime = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.laScore = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.laDiff = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picGame)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picNext)).BeginInit();
            this.SuspendLayout();
            // 
            // picGame
            // 
            this.picGame.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picGame.Location = new System.Drawing.Point(211, 38);
            this.picGame.Name = "picGame";
            this.picGame.Size = new System.Drawing.Size(355, 611);
            this.picGame.TabIndex = 0;
            this.picGame.TabStop = false;
            // 
            // picNext
            // 
            this.picNext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picNext.Location = new System.Drawing.Point(12, 67);
            this.picNext.Name = "picNext";
            this.picNext.Size = new System.Drawing.Size(169, 110);
            this.picNext.TabIndex = 1;
            this.picNext.TabStop = false;
            // 
            // laTime
            // 
            this.laTime.AutoSize = true;
            this.laTime.BackColor = System.Drawing.Color.Transparent;
            this.laTime.Font = new System.Drawing.Font("Segoe UI Black", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.laTime.ForeColor = System.Drawing.Color.White;
            this.laTime.Location = new System.Drawing.Point(87, 180);
            this.laTime.Name = "laTime";
            this.laTime.Size = new System.Drawing.Size(72, 30);
            this.laTime.TabIndex = 2;
            this.laTime.Text = "00:00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Uighur", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(7, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 25);
            this.label2.TabIndex = 3;
            this.label2.Text = "Next figure:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(7, 180);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 26);
            this.label3.TabIndex = 4;
            this.label3.Text = "Time";
            // 
            // laScore
            // 
            this.laScore.AutoSize = true;
            this.laScore.BackColor = System.Drawing.Color.Transparent;
            this.laScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laScore.ForeColor = System.Drawing.Color.White;
            this.laScore.Location = new System.Drawing.Point(7, 206);
            this.laScore.Name = "laScore";
            this.laScore.Size = new System.Drawing.Size(93, 26);
            this.laScore.TabIndex = 5;
            this.laScore.Text = "Score: 0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(7, 661);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 26);
            this.label1.TabIndex = 6;
            this.label1.Text = "Esc - pause";
            // 
            // laDiff
            // 
            this.laDiff.AutoSize = true;
            this.laDiff.BackColor = System.Drawing.Color.Transparent;
            this.laDiff.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laDiff.ForeColor = System.Drawing.Color.White;
            this.laDiff.Location = new System.Drawing.Point(7, 232);
            this.laDiff.Name = "laDiff";
            this.laDiff.Size = new System.Drawing.Size(95, 26);
            this.laDiff.TabIndex = 7;
            this.laDiff.Text = "Difficulty";
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::gameTetris.Properties.Resources.synthwave_retrowave_148087_120;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(579, 696);
            this.Controls.Add(this.laDiff);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.laScore);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.laTime);
            this.Controls.Add(this.picNext);
            this.Controls.Add(this.picGame);
            this.DoubleBuffered = true;
            this.Name = "fm";
            this.ShowIcon = false;
            this.Text = "Tetris";
            ((System.ComponentModel.ISupportInitialize)(this.picGame)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picNext)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picGame;
        private System.Windows.Forms.PictureBox picNext;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label laTime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label laScore;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label laDiff;
    }
}

