﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gameTetris
{
    class gameMod
    {
        public int Rows { get; }
        public int Cols { get; }
        public int FigX;
        public int FigY;
        public int FigXnext;
        public int FigYnext;

        public int[,] gameField;
        public int[,] tempField;
        private int[,] gameFigure;
        public int[,] nextFigure;
        private int x;
        private int y;
        private bool state = false;

        public bool Next;

        public gameMod(int cols, int rows)
        {
            Rows = rows;
            Cols = cols;
            gameField = new int[Rows, Cols];
            initField();
        }

        public void initFigure(int _x, int _y) // Иницализация первой фигуры
        {
            x = _x;
            y = _y;

            if (Next == false)
            {
                nextFigure = randomFigure(nextFigure);
                Next = true;
            }

            if (gameFigure == null)
            {
                gameFigure = new int[3, 2]
               {
                    { 1,0},
                    { 1,0},
                    { 1,1 }
               };

                FigX = 3;
                FigY = 2;
            }

        }

        public int[,] randomFigure(int[,] figure) // Рандом фигуры
        {
            Random rnd = new Random();
            switch (rnd.Next(1, 6))
            {
                case 1:
                    figure = new int[3, 2]
               {
                    { 1,0},
                    { 1,0},
                    { 1,1 }
               };

                    FigXnext = 3;
                    FigYnext = 2;
                    break;
                case 2:
                    figure = new int[3, 2]
               {
                    { 1,0},
                    { 1,1},
                    { 1,0 }
               };

                    FigXnext = 3;
                    FigYnext = 2;
                    break;
                case 3:
                    figure = new int[2, 2]
               {
                    { 1,1},
                    { 1,1 }
               };

                    FigXnext = 2;
                    FigYnext = 2;
                    break;
                case 4:
                    figure = new int[3, 2]
               {
                    { 0,1},
                    { 1,1},
                    { 1,0 }
               };

                    FigXnext = 3;
                    FigYnext = 2;
                    break;
                case 5:
                    figure = new int[4, 1]
               {
                    { 1},
                    { 1},
                    { 1 },
                   { 1 }
               };

                    FigXnext = 4;
                    FigYnext = 1;
                    break;
            }


            return figure;
        }

        public bool CheckLoose() // проверка на проигрыш
        {
            for (int i = x; i < x + FigX; i++)
            {
                for (int j = y; j < y + FigY; j++)
                {
                    if (gameFigure[i - x, j - y] != 0)
                    {
                        if (gameField[j, i] == 1)
                        {
                            return true;
                        }
                    }
                }
            }

            state = false; // test
            return false;
        }

        public bool moveDown() // Движение вниз
        {
            if (y < Rows - FigY)
            {
                y++;
                for (int i = x; i < x + FigX; i++)
                {
                    for (int j = y; j < y + FigY; j++)
                    {
                        if (gameFigure[i - x, j - y] != 0)
                        {
                            if (gameField[j, i] == 1)
                            {
                                if (nextFigure != null && Next == true)
                                {
                                    state = true;
                                    gameFigure = nextFigure;
                                    FigX = FigXnext;
                                    FigY = FigYnext;
                                    x = 3;
                                    y = 0;
                                    nextFigure = null;
                                    Next = false;
                                    sync();
                                    return state;
                                }
                            }
                        }
                    }

                }
                state = false;
            }
            else
            {
                if (nextFigure != null && Next == true)
                {
                    state = true;
                    gameFigure = nextFigure;
                    FigX = FigXnext;
                    FigY = FigYnext;
                    x = 3;
                    y = 0;
                    nextFigure = null;
                    Next = false;
                    sync();
                }

            }

            return state;
        }

        public void moveLeft() // Движение влево
        {
            if (x > 0)
            {
                x--;
                for (int i = x; i < x + FigX; i++)
                {
                    for (int j = y; j < y + FigY; j++)
                    {
                        if (gameFigure[i - x, j - y] != 0)
                        {
                            if (gameField[j, i] == 1)
                            {
                                x++;
                                break;
                            }
                        }
                    }
                }
            }
        }

        public void RotateShape() // Поворот фигуры
        {
           
            if (FigY == 4)
            {
                moveLeft();
            }

            int temp = FigX;
            FigX = FigY;
            FigY = temp;

            int[,] tempMatrix = new int[FigX, FigY];
            for (int i = 0; i < FigX; i++) //3
            {
                for (int j = 0; j < FigY; j++) //2
                {
                    tempMatrix[i, j] = gameFigure[FigY - (j + 1), i];
                }
            }

            bool flag = true;

            if (x < Cols - FigX + 1 && y < Rows - FigY + 1)
            {
                for (int i = x; i < x + FigX; i++)
                {
                    for (int j = y; j < y + FigY; j++)
                    {
                        if (tempMatrix[i - x, j - y] != 0)
                        {
                            if (gameField[j, i] == 1)
                            {
                                flag = false;
                            }
                        }
                    }
                }
            }

            if (flag && x < Cols - FigX + 1 && y < Rows - FigY + 1)
            {
                gameFigure = tempMatrix;
                if (FigY == 4)
                {
                    moveRight();
                }
            }
            else
            {
                temp = FigX;
                FigX = FigY;
                FigY = temp;
               
                if (FigY == 4)
                {
                    moveRight();
                }
            }

        }

        public void moveRight() // Движение вправо
        {
            if (x < Cols - FigX)
            {
                x++;
                for (int i = x; i < x + FigX; i++)
                {
                    for (int j = y; j < y + FigY; j++)
                    {
                        if (gameFigure[i - x, j - y] != 0)
                        {
                            if (gameField[j, i] == 1)
                            {
                                x--;
                                break;
                            }
                        }
                    }
                }
            }
        }


        public bool checkSame(int i, int j) 
        {
            if (gameField[i, j] == tempField[i, j])
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        public int checkLines() // Проверка ряда
        {
            for (int i = 0; i < Rows; i++)
            {
                int k = 0;
                for (int j = 0; j < Cols; j++)
                {
                    if (gameField[i, j] == 1)
                    {
                        k++;
                        if (k == Cols)
                        {
                            k = i;
                            return k;
                        }
                    }
                }
            }

            return 0;
        }

        public void DeleteLine(int k) // Удаление ряда
        {
            if (k != 0)
            {
                for (int i = 0; i < Cols; i++)
                {
                    gameField[k, i] = 0;
                }

                for (int i = k; i > 0; i--) // check later
                {
                    for (int j = 0; j < Cols; j++)
                    {
                        if (gameField[i - 1, j] == 1)
                        {
                            gameField[i - 1, j] = 0;
                            gameField[i, j] = 1;
                        }
                    }
                }
            }

        }

        public void combine() // Сопоставление временного поля с основным
        {
            tempField = new int[Rows, Cols];
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    tempField[i, j] = gameField[i, j];
                }
            }

            for (int i = x; i < x + FigX; i++)
            {
                for (int j = y; j < y + FigY; j++)
                {
                    if (gameFigure[i - x, j - y] != 0)
                    {
                        tempField[j, i] = gameFigure[i - x, j - y];
                    }
                }
            }

            //state = false; // test
        }

        public void sync() // Сопоставление основного поля с временным
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    gameField[i, j] = tempField[i, j];
                }
            }

        }

        public int this[int i, int j] // получение на отрисовку либо временного, либо основного поля
        {
            get
            {
                if (state == true)
                {
                    return gameField[i, j];
                }
                else
                {
                    return tempField[i, j];
                }
            }
        }
        private void initField() // инициализация матрицы
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    gameField[i, j] = 0;
                }
            }
        }


    }
}
