﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labWebBrowser
{
    public partial class fm : MaterialForm
    {
        public fm()
        {
            InitializeComponent();
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;

            materialSkinManager.ColorScheme = new ColorScheme(
                Primary.DeepOrange600, Primary.DeepOrange800,
                Primary.Blue500, Accent.LightBlue200,
                TextShade.WHITE
            );

            buGo.Click += (sender, e) => wb.Navigate(edURL.Text);
            buBack.Click += (sender, e) => wb.GoBack();
            buForward.Click += (sender, e) => wb.GoForward();
            buReload.Click += (sender, e) => wb.Refresh();
            buStop.Click += (sender, e) => wb.Stop();
            wb.DocumentCompleted += (sender, e) => edURL.Text = wb.Url.ToString();

            edURL.KeyDown += EdURL_KeyDown;

        }

        private void EdURL_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                wb.Navigate(edURL.Text);
            }
        }

    }
}

