﻿namespace labWebBrowser
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.wb = new System.Windows.Forms.WebBrowser();
            this.buGo = new MaterialSkin.Controls.MaterialButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.edURL = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.buBack = new MaterialSkin.Controls.MaterialButton();
            this.buForward = new MaterialSkin.Controls.MaterialButton();
            this.buReload = new MaterialSkin.Controls.MaterialButton();
            this.buStop = new MaterialSkin.Controls.MaterialButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // wb
            // 
            this.wb.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.wb.Location = new System.Drawing.Point(0, 106);
            this.wb.MinimumSize = new System.Drawing.Size(400, 200);
            this.wb.Name = "wb";
            this.wb.Size = new System.Drawing.Size(965, 480);
            this.wb.TabIndex = 2;
            // 
            // buGo
            // 
            this.buGo.AutoSize = false;
            this.buGo.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buGo.Depth = 0;
            this.buGo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buGo.DrawShadows = true;
            this.buGo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buGo.HighEmphasis = true;
            this.buGo.Icon = null;
            this.buGo.Location = new System.Drawing.Point(872, 6);
            this.buGo.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buGo.MouseState = MaterialSkin.MouseState.HOVER;
            this.buGo.Name = "buGo";
            this.buGo.Size = new System.Drawing.Size(89, 20);
            this.buGo.TabIndex = 7;
            this.buGo.Text = "Go";
            this.buGo.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.buGo.UseAccentColor = false;
            this.buGo.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Controls.Add(this.edURL, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.buGo, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 68);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(965, 32);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // edURL
            // 
            this.edURL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.edURL.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edURL.Location = new System.Drawing.Point(3, 3);
            this.edURL.Name = "edURL";
            this.edURL.Size = new System.Drawing.Size(862, 24);
            this.edURL.TabIndex = 0;
            this.edURL.Text = "https://ya.ru/";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.buStop, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.buBack, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.buReload, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.buForward, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 592);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(958, 39);
            this.tableLayoutPanel2.TabIndex = 9;
            // 
            // buBack
            // 
            this.buBack.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buBack.Depth = 0;
            this.buBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buBack.DrawShadows = true;
            this.buBack.HighEmphasis = true;
            this.buBack.Icon = null;
            this.buBack.Location = new System.Drawing.Point(4, 6);
            this.buBack.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buBack.MouseState = MaterialSkin.MouseState.HOVER;
            this.buBack.Name = "buBack";
            this.buBack.Size = new System.Drawing.Size(231, 27);
            this.buBack.TabIndex = 10;
            this.buBack.Text = "<< Back";
            this.buBack.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.buBack.UseAccentColor = false;
            this.buBack.UseVisualStyleBackColor = true;
            // 
            // buForward
            // 
            this.buForward.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buForward.Depth = 0;
            this.buForward.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buForward.DrawShadows = true;
            this.buForward.HighEmphasis = true;
            this.buForward.Icon = null;
            this.buForward.Location = new System.Drawing.Point(243, 6);
            this.buForward.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buForward.MouseState = MaterialSkin.MouseState.HOVER;
            this.buForward.Name = "buForward";
            this.buForward.Size = new System.Drawing.Size(231, 27);
            this.buForward.TabIndex = 11;
            this.buForward.Text = "Forward >>";
            this.buForward.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.buForward.UseAccentColor = false;
            this.buForward.UseVisualStyleBackColor = true;
            // 
            // buReload
            // 
            this.buReload.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buReload.Depth = 0;
            this.buReload.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buReload.DrawShadows = true;
            this.buReload.HighEmphasis = true;
            this.buReload.Icon = null;
            this.buReload.Location = new System.Drawing.Point(482, 6);
            this.buReload.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buReload.MouseState = MaterialSkin.MouseState.HOVER;
            this.buReload.Name = "buReload";
            this.buReload.Size = new System.Drawing.Size(231, 27);
            this.buReload.TabIndex = 12;
            this.buReload.Text = "Reload";
            this.buReload.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.buReload.UseAccentColor = false;
            this.buReload.UseVisualStyleBackColor = true;
            // 
            // buStop
            // 
            this.buStop.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buStop.Depth = 0;
            this.buStop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buStop.DrawShadows = true;
            this.buStop.HighEmphasis = true;
            this.buStop.Icon = null;
            this.buStop.Location = new System.Drawing.Point(721, 6);
            this.buStop.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.buStop.MouseState = MaterialSkin.MouseState.HOVER;
            this.buStop.Name = "buStop";
            this.buStop.Size = new System.Drawing.Size(233, 27);
            this.buStop.TabIndex = 13;
            this.buStop.Text = "Stop";
            this.buStop.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.buStop.UseAccentColor = false;
            this.buStop.UseVisualStyleBackColor = true;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(964, 633);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.wb);
            this.MinimumSize = new System.Drawing.Size(400, 200);
            this.Name = "fm";
            this.Text = "labWebBrowser";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.WebBrowser wb;
        private MaterialSkin.Controls.MaterialButton buGo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox edURL;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MaterialSkin.Controls.MaterialButton buStop;
        private MaterialSkin.Controls.MaterialButton buBack;
        private MaterialSkin.Controls.MaterialButton buReload;
        private MaterialSkin.Controls.MaterialButton buForward;
    }
}

