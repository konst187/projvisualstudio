﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labStreamWriter
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"D:\tempStream.txt";

            using (StreamWriter streamWriter = new StreamWriter(path))
            {
                streamWriter.WriteLine("1111");
                streamWriter.WriteLine("2222");
                streamWriter.WriteLine("3333");
                Console.WriteLine("File created!");
            }

            using (StreamWriter streamWriter = new StreamWriter(path, true))
            {

                streamWriter.WriteLine("4444");
                Console.WriteLine("File appended!");
            }

            using (StreamReader streamReader = new StreamReader(path))
            {

                Console.WriteLine("--------------");

                Console.WriteLine(streamReader.ReadToEnd());
                Console.WriteLine("--------------");
                Console.WriteLine("Файл считан целиком");
            }

            using (StreamReader streamReader = new StreamReader(path))
            {

                Console.WriteLine("--------------");
                string line;
                int n = 1;
                while ((line = streamReader.ReadLine()) != null)
                {
                    Console.WriteLine($"Строка номер {n++} = [{line}]");
                }

                Console.WriteLine("--------------");
                Console.WriteLine("Файл считан построчно");
            }

            var a = new int[] { 0, 1, 2, 3, 4, 5, 6 };
            using (StreamWriter streamWriter = new StreamWriter(path, true))
            {
                foreach (var item in a)
                {
                    streamWriter.Write(item);
                }
                
                Console.WriteLine("File Дописан!");
            }

        }
    }
}
