﻿using labImageScrollHorisontal.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageScrollHorisontal
{
    public partial class fm : Form
    {
        private Bitmap imgSky;
        private Bitmap imgRoad;
        private Bitmap imgCar;
        private Bitmap imgWheel;
        private Bitmap b;
        private Graphics g;
        private Point startPoint;
        private int drawX = 0;
        private int drawXDouble = 0;
        private int drawX2;
        private bool state = true;
        private Graphics c;
        private float angle = 0;

        public fm()
        {
            InitializeComponent();

            imgSky = Resources.sky2;
            imgRoad = Resources.Road2;
            imgCar = Resources.car;
            imgWheel = Resources.wheel;
            this.Height = Resources.sky2.Height + Resources.Road2.Height;
            this.Width = Resources.sky2.Width + Resources.Road2.Width;
            this.MinimumSize = new Size(Resources.sky2.Width / 2 + Resources.Road2.Width / 2, Resources.sky2.Height + Resources.Road2.Height);
            this.MaximumSize = new Size(Resources.sky2.Width / 2 + Resources.Road2.Width / 2, Resources.sky2.Height + Resources.Road2.Height);

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);


            this.DoubleBuffered = true;
            this.Paint += Fm_Paint;
            this.MouseDown += Fm_MouseDown;
            this.MouseMove += Fm_MouseMove;
            this.KeyPreview = true;
            this.KeyDown += Fm_KeyDown;

            /*
             * TODO
             * различные фоны
             * добавить персонажа, который будет перемещаться
             */
        }

        private void Fm_MouseDown(object sender, MouseEventArgs e)
        {
            startPoint = e.Location;
        }

        private void Fm_Paint(object sender, PaintEventArgs e)
        {
            UpdateBG();
            e.Graphics.DrawImage(b, 0, 0);
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    UpdateDrawX(-5);
                    UpdateDrawX2(-30);
                    break;
                case Keys.Right:
                    UpdateDrawX(+5);
                    UpdateDrawX2(+30);
                    break;
            }
            this.Invalidate();
        }

        private void Fm_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.Capture)
            {
                UpdateDrawX(startPoint.X - e.X);
                UpdateDrawX2(startPoint.X - e.X);
                startPoint = e.Location;
                this.Invalidate();
            }
        }

        private void UpdateDrawX(int v)
        {
            Text = $"{Application.ProductName}: Разная скорость фона через стрелочки";

            c = Graphics.FromImage(imgWheel);
            c.TranslateTransform(imgWheel.Width / 2, imgWheel.Height / 2);
            c.RotateTransform(angle);
            c.DrawImage(Resources.wheel, -imgWheel.Width / 2, -imgWheel.Height / 2);
            if (state)
            {
                angle += 50;
            }
            else
            {
                angle -= 50;
            }

            if (v > 0 && state != true)
            {
                state = true;
                imgCar.RotateFlip(RotateFlipType.RotateNoneFlipX);

            }
            else if (v < 0 && state == true)
            {
                state = false;
                imgCar.RotateFlip(RotateFlipType.RotateNoneFlipX);
            }

            drawX -= v;

            if (drawX > 0)
            {
                drawX -= imgSky.Width;
            }
            else
            {
                if (drawX < -imgSky.Width)
                {
                    drawX += imgSky.Width;
                }
            }

        }

        private void UpdateDrawX2(int v)
        {
            //Text = $"{Application.ProductName}:{drawX}, {v}";
            drawX2 -= v;
            if (drawX2 > 0)
            {
                drawX2 -= imgRoad.Width;
            }
            else
            {
                if (drawX2 < -imgRoad.Width)
                {
                    drawX2 += imgRoad.Width;
                }
            }

        }

        private void UpdateBG()
        {
            for (int i = 0; i < 2; i++)
            {
                g.DrawImage(imgRoad, new Rectangle(drawX2 + imgRoad.Width * i, this.Height - imgRoad.Height, imgRoad.Width, imgRoad.Height));
                g.DrawImage(imgRoad, (drawX2 + imgRoad.Width * i), this.Height - imgRoad.Height);

            }
          
            g.DrawImage(imgSky, new Rectangle(drawX, 0, imgSky.Width, imgSky.Height));            
            g.DrawImage(imgSky, new Rectangle(((drawX) + imgSky.Width), 0, imgSky.Width, imgSky.Height));
            g.DrawImage(imgCar, imgRoad.Width / 4, this.Height - imgRoad.Height + 20, imgCar.Width / 2, imgCar.Height / 2);
            g.DrawImage(imgWheel, imgRoad.Width / 4 + 33, this.Height - imgRoad.Height + 60, imgWheel.Width / 2, imgWheel.Height / 2);
            g.DrawImage(imgWheel, imgRoad.Width / 3 + 40, this.Height - imgRoad.Height + 60, imgWheel.Width / 2, imgWheel.Height / 2);

        }
    }
}
