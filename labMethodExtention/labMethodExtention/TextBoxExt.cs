﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labMethodExtention
{
    static class TextBoxExt
    {
        public static string TextUp(this TextBox source)
        {
            return source.Text.ToUpper();
        }
    }
}
